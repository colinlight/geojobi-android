package com.geojobi.test;

import com.geojobi.activity.MainActivity;
import com.geojobi.base.IAppDependencies;
import com.geojobi.base.ISettingsDelegate;

import android.app.Application;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;

public class MainTest extends ActivityInstrumentationTestCase2<MainActivity> {
	
	//http://developer.android.com/resources/tutorials/testing/helloandroid_test.html
	private MainActivity mActivity; 
	
	public MainTest()
	{
		super("com.geojobi", MainActivity.class);
	}
	
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
		mActivity = this.getActivity();
	}
	
	public void testAppContext()
	{
		Application app = this.mActivity.getApplication();
		assertNotNull(app);
	}
	
	public void testAppDependenciesSettingsManager()
	{
		ISettingsDelegate settingManager = this.mActivity.getSettingsDelegate();
		assertNotNull( settingManager );
	}

}
