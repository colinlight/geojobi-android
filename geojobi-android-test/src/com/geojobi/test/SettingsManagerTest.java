package com.geojobi.test;

import com.geojobi.activity.MainActivity;
import com.geojobi.model.Settings;
//import com.geojobi.manager.SettingsManager;

import android.test.ActivityInstrumentationTestCase2;

public class SettingsManagerTest extends ActivityInstrumentationTestCase2<MainActivity> 
{
	private MainActivity mActivity;
	
	private static final String TEST_EMAIL = "test.user@test.com";
	
	private static final String TEST_USER_ID = "24"; 

	
	public SettingsManagerTest()
	{
		super("com.geojobi", MainActivity.class);
	}
	
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
		mActivity = this.getActivity();
	}
	
	
	public void testSettingGettingUserEmail()
	{
		mActivity.getSettingsDelegate().setString( Settings.USER_DETAILS_EMAIL, TEST_EMAIL);
		assertEquals( TEST_EMAIL, mActivity.getSettingsDelegate().getString( Settings.USER_DETAILS_EMAIL) );
	}
	
	public void testSettingGettingUserID()
	{
		mActivity.getSettingsDelegate().setString( Settings.CANDIDATE_GUID, TEST_USER_ID);
		assertEquals( TEST_USER_ID, mActivity.getSettingsDelegate().getString( Settings.CANDIDATE_GUID) );
	}
	
	public void testSetGetSettingComplete()
	{
		mActivity.getSettingsDelegate().setState( Settings.SETTINGS_COMPLETE, true);
		assertTrue(mActivity.getSettingsDelegate().getState( Settings.SETTINGS_COMPLETE));
	}
	
	

}
