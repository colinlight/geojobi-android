package com.geojobi.components;

import com.geojobi.R;
import com.geojobi.activity.UserSelectionsActivity;
import com.geojobi.model.AppConstants;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FirstRunDialog extends Dialog {
	
	private static final int STEP_ONE = 0;
	
	private static final int STEP_TWO = 1;
	
	private static final int STEP_THREE = 2;
	
	private static final int STEP_FOUR = 3; 
		
	private LinearLayout mSetFourLayout; 
	
	private Button mNextButton;
	
	private Button mDoneButton;
	
	private TextView mPromptTextView;
	
	private int mCurrentStep;
	
	public FirstRunDialog( Context context )
	{
		super(context);
		this.setUp();
	}
	
	private void setUp()
	{
		this.setContentView(R.layout.firstrun_guide_dialog);
		this.setTitle(R.string.firstRunDialogTitle1);
		
		mSetFourLayout = (LinearLayout)this.findViewById(R.id.firstRunFinalBtns);
		mSetFourLayout.setVisibility(View.GONE);
		
		
		mDoneButton = (Button)this.findViewById(R.id.firstRunDoneBtn);
		mNextButton = (Button)this.findViewById(R.id.firstRunNextBtn);
		mPromptTextView = (TextView)this.findViewById(R.id.firstRunPomptTextView);
		
		mCurrentStep = STEP_ONE;
		
		mNextButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setUpStep();
			}
		});
		
		mDoneButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				cancel();
			}
		});
		
		
		Button startBtn = (Button)this.findViewById(R.id.firstRunStartBtn);
		startBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cancel();
			}
		});
		
		Button helpBtn = (Button)this.findViewById(R.id.firstRunVideoBtn);
		helpBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				closeDialog( true );
			}
		});
		
		
	}
	
	
	private void setUpStep()
	{
		int titleId = 0;
		int prmoptTextId = 0;
		
		switch( mCurrentStep )
		{
			case STEP_ONE:
				mNextButton.setText( R.string.firstRunDialogBtnNext);
				titleId = R.string.firstRunDialogTitle2;
				prmoptTextId = R.string.firstRunDialogPromptTxt2;
				mCurrentStep = STEP_TWO;
				break;
			case STEP_TWO:
				titleId = R.string.firstRunDialogTitle3;
				prmoptTextId = R.string.firstRunDialogPromptTxt3;
				mCurrentStep = STEP_THREE;
				break;
			case STEP_THREE:
				LinearLayout firstButtonLayout = (LinearLayout)this.findViewById(R.id.firstRunStepBtn);
				firstButtonLayout.setVisibility(View.GONE);
				mSetFourLayout.setVisibility(View.VISIBLE);
				titleId = R.string.firstRunDialogTitle4;
				prmoptTextId = R.string.firstRunDialogPromptTxt4;
				mCurrentStep = STEP_FOUR;
				break;
		}
		
		mPromptTextView.setText(prmoptTextId);
		this.setTitle(titleId);
	}
	
	private void closeDialog( boolean showHelp )
	{
		if ( showHelp )
		{
			Intent helpIntent = new Intent( Intent.ACTION_VIEW);
			helpIntent.setData(Uri.parse( AppConstants.HELP_WEBSITE ));
			this.getContext().startActivity(helpIntent);
		}
		else
		{
			this.getContext().startActivity( new Intent( this.getContext(), UserSelectionsActivity.class ));
		}
		this.cancel();
	}

}
