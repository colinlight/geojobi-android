package com.geojobi.components;

import java.io.IOException;
import java.util.List;

import com.geojobi.R;
import com.geojobi.base.IAppDependencies;
import com.geojobi.helper.DropDownListHelper;
import com.geojobi.helper.IOverlayAlertDelegate;
import com.geojobi.helper.JobLocationsOverlay;
import com.geojobi.helper.JobOverlayItem;
import com.geojobi.helper.LocationHelper;
import com.geojobi.model.Settings;
import com.geojobi.model.db.DatabaseTableConstants;
import com.geojobi.model.db.OptionColumns;
import com.google.android.maps.GeoPoint;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

public class JobLocationDialog extends Dialog {
	
	public static final String TAG = "JobLocationDialog";

	private IAppDependencies mContext;

	private IOverlayAlertDelegate mAlertDelegate;

	private GeoPoint mCurrentLocation; 

	private JobLocationsOverlay mJobOverlays;

	private RadioButton mPermitRadioYes;

	private boolean mIsUpdate;

	private JobOverlayItem mSelectedOverlay;

	private GeoPoint mLocation;

	private Spinner mLabelSpinner;

	private Spinner mRadiusSpinner;

	private Spinner mJobTypeSpinner;

	private Spinner mSalarySpinner;

	private Geocoder mGeoCoder;


	public JobLocationDialog( Context context, IOverlayAlertDelegate delegate, 
			JobLocationsOverlay overlays, Address address, 
			GeoPoint currentLocation, GeoPoint point,
			boolean isUpdate, JobOverlayItem overlay )
	{
		super(context, R.style.LocationDialogTheme );
		this.setContentView(R.layout.map_location_dialog);
		this.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);

		mContext = (IAppDependencies)context.getApplicationContext();
		mAlertDelegate = delegate;
		mJobOverlays = overlays;
		mIsUpdate = isUpdate;
		mSelectedOverlay = overlay;
		mCurrentLocation = currentLocation;
		mLocation = point;
		setLocationString(address);

		setUp();

	}

	private void setUp()
	{
		Button deleteBtn = (Button)this.findViewById(R.id.userLocationsRadiusDialogDeleteBtn);
		if (!mIsUpdate)
			deleteBtn.setVisibility(View.GONE);


		int titleId = (!mIsUpdate) ? R.string.userLocationsRadiusDialogTitle
				: R.string.userLocationsRadiusDialogEditTitle;
		this.setTitle(titleId);

		TextView titleTextView = (TextView)findViewById(R.id.userLocationDialogTitle);
		titleTextView.setText(titleId);

		int label = (mSelectedOverlay == null) ? 0 : mSelectedOverlay.getLabel();
		mLabelSpinner = (Spinner)findViewById(R.id.userLocationsLabelsSpinner);
		setSpinnerDataModelFor(mLabelSpinner, DatabaseTableConstants.LOCATIONLABELS, label );

		int radius = (mSelectedOverlay == null) ? 0 : mSelectedOverlay.getRadius();
		mRadiusSpinner = (Spinner)findViewById(R.id.userLocationsRadiusSpinner);
		setSpinnerDataModelFor(mRadiusSpinner, DatabaseTableConstants.DISTANCE, radius );

		int jobTypeIndex = (mSelectedOverlay == null) ? mContext.getSettingsDelegate().getInt(Settings.JOB_TYPE ) : 
			mSelectedOverlay.getJobType();

		mJobTypeSpinner = (Spinner)findViewById(R.id.userLocationsTypeSpinner);
		setSpinnerDataModelFor(mJobTypeSpinner, DatabaseTableConstants.JOBTYPE, jobTypeIndex );

		mSalarySpinner = (Spinner)findViewById(R.id.userLocationsSalarySpinner);
		setSalarySpinner( mSalarySpinner, mSelectedOverlay, -1 );

		mJobTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View v, int position, long id)
			{
				setSalarySpinner( mSalarySpinner, mSelectedOverlay, position );
			}

			public void onNothingSelected(AdapterView<?> arg0){}
		});


		mPermitRadioYes = ( RadioButton )findViewById(R.id.userLocationsPermitRadio2);

		int permit = (mSelectedOverlay == null) ? 0 : mSelectedOverlay.getPermit();
		RadioGroup group = (RadioGroup)findViewById( R.id.userLocationsPermitGroup );

		int resourceId = ( permit == 0 )? R.id.userLocationsPermitRadio1 : 
			R.id.userLocationsPermitRadio2;
		group.check( resourceId );


		Button cancelBtn = (Button)findViewById(R.id.userLocationsRadiusDialogCancelBtn);
		cancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cancel();
			}
		});


		Button saveBtn = (Button)findViewById(R.id.userLocationsRadiusDialogSaveBtn);
		saveBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (mIsUpdate)
					mJobOverlays.removeOverlay(mSelectedOverlay);
				
				JobOverlayItem jobLocation = new JobOverlayItem(mLocation,
						selectedIDFromSpinner( mLabelSpinner ),
						selectedNameFromSpinner( mLabelSpinner ),
						selectedIDFromSpinner( mRadiusSpinner ),
						selectedIDFromSpinner( mSalarySpinner ),
						selectedIDFromSpinner( mJobTypeSpinner ),
						getPermitState(),
						"Job", "");
				
				mJobOverlays.addOverlay(jobLocation);
				saveLocations();
				
				mAlertDelegate.overlayUpdated();

				cancel();
			}
		});


		if (mIsUpdate) {
			deleteBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// write all items so we have index's in order
					mJobOverlays.removeOverlay(mSelectedOverlay);
					cancel();
					mAlertDelegate.overlayUpdated();
					saveLocations();
				}
			});
		}


		Button editLocationBtn = (Button)findViewById(R.id.userLocationsEditLocationBtn);
		editLocationBtn.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showCurrentLocationAlert();
			}
		});

	}


	private void setSpinnerDataModelFor( Spinner spinner, String optionType, int selectedId )
	{
		Cursor cursor = mContext.getOptionsDelegate().getCursorForOptionsOfType(optionType);
		
		DropDownListHelper.setDataModelForDropDownFromOptions( cursor, spinner, this.getContext() );
		
		DropDownListHelper.setSelectedDropDownItem(cursor, spinner, this.getContext(), selectedId );
	}

	private int getPermitState()
	{
		return ( mPermitRadioYes.isChecked() )? 1 : 0;
	}


	private void setSalarySpinner( Spinner salarySpinner, JobOverlayItem selectedOverLay, int typeID )
	{
		typeID = ( typeID <= -1 )?  mContext.getSettingsDelegate().getInt(Settings.JOB_TYPE) : typeID;
		String setting = Settings.MIN_SALARY;
		String dbOptionType = DatabaseTableConstants.SALARYPERMANENT;

		if ( typeID > 1 )
		{
			dbOptionType = DatabaseTableConstants.SALARYCONTRACT;
			setting = Settings.MIN_SALARY_CONTRACT;
		}

		int salaryIndex = (selectedOverLay == null) ?
				mContext.getSettingsDelegate().getInt( setting ) : selectedOverLay.getSalary();

				setSpinnerDataModelFor( salarySpinner, dbOptionType, salaryIndex );
	}


	/**
	 * loop through all locations only ignore/delete the ones that don't have a location
	 * hence the try catch block. This enables the locations to always be in the right
	 * order e.g. 1 -4 active and 5 could be empty
	 */
	private void saveLocations() {

		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences( (Context)mContext);
		SharedPreferences.Editor editor = appPrefs.edit();

		boolean isValid = false;

		for (int i = 0; i < 5; i++) {

			String settingName = Settings.USER_LOCATION_LAT_LONG + i;
			try
			{
				JobOverlayItem overlay = mJobOverlays.getItem(i);
				editor.putString( settingName, 
						mContext.getSettingsDelegate().parseLocationToString(
								overlay.getPoint(), 
								overlay.getRadius(),
								overlay.getSalary(),
								overlay.getJobType(),
								overlay.getPermit(),
								overlay.getLabel(),
								overlay.getLabelName()
								) );
				isValid = true;
				Log.v(TAG, settingName + "VALID");
			}
			catch (Exception e) {
				editor.putString(settingName, "");
				Log.v(TAG, settingName + "EMPTY");
			}
		}
		editor.putBoolean(Settings.USER_LOCATIONS_VALID, isValid);

		editor.commit();
	}
	
	
	/**
	 * Sets the current location string
	 * @param address
	 */
	private void setLocationString( Address address) {

		TextView textView = (TextView)findViewById(R.id.userLocationsPostCodeTextView);
		textView.setText( LocationHelper.getLocationString(address));
	}


	private void showCurrentLocationAlert()
	{	    
		AlertDialog alert = new AlertDialog.Builder(this.getContext())
		.setTitle(R.string.userLocationsCurrentLocDialogTitle)
		.setMessage(R.string.userLocationsCurrentLocDialogMsg)
		.setPositiveButton(R.string.userLocationsCurrentLocDialogYes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				setToCurrentLocation();
			}
		})
		.setNegativeButton(R.string.userLocationsCurrentLocDialogNo, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {}
		})
		.create();
		alert.show();
	}

	private void setToCurrentLocation()
	{
		mLocation = mCurrentLocation; 
		if ( mGeoCoder == null )
			mGeoCoder = new Geocoder(this.getContext());

		try {
			List<Address> locations = mGeoCoder.getFromLocation( mLocation.getLatitudeE6()/1E6, 
					mLocation.getLongitudeE6()/1E6, 1);

			if ( locations.size() <= 0 )
			{
				Toast toastInValid = Toast.makeText(this.getContext(), 
						R.string.userLocationsInvalidLocationToastTxt, Toast.LENGTH_SHORT);

				toastInValid.show();
				return;
			}
			setLocationString( locations.get(0) );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private int selectedIDFromSpinner( Spinner spinner )
	{
		SimpleCursorAdapter adpater = (SimpleCursorAdapter)spinner.getAdapter();
		Cursor cursor = adpater.getCursor();
		return cursor.getInt(cursor.getColumnIndex(OptionColumns.TYPE_ID));
	}
	
	private String selectedNameFromSpinner( Spinner spinner )
	{
		SimpleCursorAdapter adpater = (SimpleCursorAdapter)spinner.getAdapter();
		Cursor cursor = adpater.getCursor();
		return cursor.getString(cursor.getColumnIndex(OptionColumns.DESCRIPTION));
	}
}
