package com.geojobi.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.geojobi.R;
import com.geojobi.helper.DateHelper;
import com.geojobi.model.Job;
import com.geojobi.model.Setting;
import com.geojobi.model.db.VacancyColumns;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class JobListAdapter extends ArrayAdapter<Job> {

	private static final String TAG = "JobListAdapter";
	
	//private SimpleDateFormat mDateFormattter;
	
	private Context mConext;
	
	private LayoutInflater mInflator;

	
	public JobListAdapter( Context context, List<Job> dataModel  )
	{
		super(context, R.layout.job_row, dataModel);
		
		this.mInflator = LayoutInflater.from(context);
		
		//this.mDateFormattter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	}
	
	
	@Override
	public View getView( int position, View convertView, ViewGroup parent )
	{
		final Job job = ( Job ) this.getItem(position);
		
		final ImageView statusImageView;
		TextView titleTextView;
		TextView salaryTextView;
		
		if ( convertView == null )
		{
			convertView = mInflator.inflate(R.layout.job_row, null);
			
			statusImageView = (ImageView) convertView.findViewById(R.id.jobListRowStatusImageView);
			titleTextView = (TextView) convertView.findViewById(R.id.jobListRowTitleTextView);	
			salaryTextView = (TextView) convertView.findViewById(R.id.jobListRowSalaryTextView);	
		}
		else
		{
			//recycle row
			statusImageView = (ImageView) convertView.findViewById(R.id.jobListRowStatusImageView);
			titleTextView = (TextView) convertView.findViewById(R.id.jobListRowTitleTextView);	
			salaryTextView = (TextView) convertView.findViewById(R.id.jobListRowSalaryTextView);	
		}
		

		statusImageView.setImageResource( this.getResourceIDForValidDate(job.getDealine()) );
		titleTextView.setText( job.getTitle() );
		salaryTextView.setText( job.getSalaryPerm() );
		
		convertView.setTag(job);

		return convertView;
	}
	
	
	private int getResourceIDForValidDate( String dateString )
	{
		int resID = android.R.drawable.presence_offline;
		try {
			Date now = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat( DateHelper.DATE_FORMAT );
			Date deadlineDate = dateFormat.parse(dateString);
			resID = ( deadlineDate.after(now) )? android.R.drawable.presence_online :
				android.R.drawable.presence_offline; 
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return resID;
	}

}
