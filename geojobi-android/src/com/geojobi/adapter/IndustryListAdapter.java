package com.geojobi.adapter;

import java.util.List;

import com.geojobi.R;
import com.geojobi.model.IndustryFilter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class IndustryListAdapter extends ArrayAdapter<IndustryFilter> {
	
	public static final String TAG = "IndustryListAdapter";
	
	private LayoutInflater inflater;
	
	private IindustryListAdapaterSelectionDelegate mSelectionDelegate;
	
	private Context mContext; 
	
	
	public IndustryListAdapter( Context context, List<IndustryFilter> industryList, 
			IindustryListAdapaterSelectionDelegate selectionDelegate)
	{
		super(context, R.layout.industry_filter_row, industryList);
		this.inflater = LayoutInflater.from(context);
		this.mSelectionDelegate = selectionDelegate;
		this.mContext = context;
	}
	
	@Override
	public View getView( int position, View convertView, ViewGroup parent )
	{
		final IndustryFilter filter = ( IndustryFilter ) this.getItem(position);
		
		final ImageView imageView;
		TextView titleText;
		TextView yearText;
		
		
		if ( convertView == null )
		{
			convertView = inflater.inflate(R.layout.industry_filter_row, null);
			
			imageView = (ImageView) convertView.findViewById(R.id.industryFilterRowImageView);
			titleText = (TextView) convertView.findViewById(R.id.industryFilterRowTitleTextView);
			yearText = (TextView) convertView.findViewById(R.id.industryFilterRowYearsTextView);
			
		
			convertView.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					IndustryFilter selectedFilter = (IndustryFilter)v.getTag();
					mSelectionDelegate.itemSelected( selectedFilter );
				}
			});
			
		}
		else
		{
			//recycle row
			imageView = (ImageView) convertView.findViewById(R.id.industryFilterRowImageView);
			titleText = (TextView) convertView.findViewById(R.id.industryFilterRowTitleTextView);
			yearText = (TextView) convertView.findViewById(R.id.industryFilterRowYearsTextView);
			
		}
		

		imageView.setImageResource( this.getSelectedImageId(filter) );
		titleText.setText( filter.getIndustry() );
		yearText.setText( filter.getYears() + " " + 
				mContext.getResources().getString(R.string.industryDetailExperienceTxt) );
		
		yearText.setVisibility( this.getYearTextVisible(filter));
		
		convertView.setBackgroundColor( getBackgroundColour( filter ) );
		
		convertView.setTag(filter);
		
		
		return convertView;
	}
	
	private int getSelectedImageId( IndustryFilter filter )
	{
		return ( filter.getSelected() )? android.R.drawable.ic_delete : 
			android.R.drawable.ic_input_add;
	}
	
	private int getBackgroundColour( IndustryFilter filter )
	{
		return ( filter.getSelected() )?  0x99999999 : 0x00000000;
	}
	
	private int getYearTextVisible( IndustryFilter filter )
	{
		return ( filter.getSelected() )?  View.VISIBLE : View.GONE;
	}
	
}
