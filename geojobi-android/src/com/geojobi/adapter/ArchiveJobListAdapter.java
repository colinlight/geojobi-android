package com.geojobi.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.geojobi.R;
import com.geojobi.model.db.DatabaseTableConstants;
import com.geojobi.model.db.VacancyColumns;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ArchiveJobListAdapter extends CursorAdapter {

	private LayoutInflater mInflator;
	
	private SimpleDateFormat mDateFormattter;

	public ArchiveJobListAdapter(Context context, Cursor cursor )
	{
		super(context, cursor);

		this.mInflator = LayoutInflater.from(context);
		
		this.mDateFormattter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {

		TextView titleTextView = (TextView)view.findViewById(R.id.archiveJobListRowTitleTextView);
		titleTextView.setText( cursor.getString(cursor.getColumnIndex(VacancyColumns.JOB_TITLE)) );
		
		int statusId = cursor.getInt(cursor.getColumnIndex(VacancyColumns.STATUS));
		
		TextView statusTextView = (TextView)view.findViewById(R.id.archiveJobListRowStatusTextView);
		statusTextView.setText( getStatusDescription( context, statusId ) );

		ImageView statusImageView = (ImageView)view.findViewById(R.id.archiveJobListRowStatusImageView);	
		String dateString = cursor.getString( cursor.getColumnIndex(VacancyColumns.DEADLINEDATE ));
		statusImageView.setImageResource( this.getResourceIDForValidDate(dateString) );

	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {

		return mInflator.inflate(R.layout.archive_job_row, null);
	}
	
	
	private String getStatusDescription( Context context, int id )
	{
		switch(id)
		{
			case DatabaseTableConstants.STATUS_REJECT:
				return context.getString(R.string.archivedJobStatusReject );
				
			case DatabaseTableConstants.STATUS_INTERESTED:
				return context.getString(R.string.archivedJobStatusInterested );
				
			case DatabaseTableConstants.STATUS_NONE:
				return context.getString(R.string.archivedJobStatusConsider );	
				
		}
		return null;
	}
	
	private int getResourceIDForValidDate( String dateString )
	{
		try {
			Date now = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date deadlineDate = dateFormat.parse(dateString);
			int resID = ( deadlineDate.after(now) )? android.R.drawable.presence_online :
				android.R.drawable.presence_offline; 
			return resID;
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return android.R.drawable.presence_offline;
	}

}
