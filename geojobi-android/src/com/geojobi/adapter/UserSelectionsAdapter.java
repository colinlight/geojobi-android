package com.geojobi.adapter;

import java.util.List;

import com.geojobi.R;
import com.geojobi.model.IndustryFilter;
import com.geojobi.model.Setting;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class UserSelectionsAdapter extends ArrayAdapter<Setting> {
	
	Context mConext;
	
	private LayoutInflater mInflator;
	
	List<Setting> mDataModel;
	
	public UserSelectionsAdapter( Context context, List<Setting> dataModel )
	{
		super(context, R.layout.setting_row, dataModel);
		this.mConext = context;
		this.mDataModel = dataModel;
		
		this.mInflator = LayoutInflater.from(context);
	}
	
	@Override
	public View getView( int position, View convertView, ViewGroup parent )
	{
		final Setting setting = ( Setting ) this.getItem(position);
		
		final ImageView imageView;
		TextView titleText;
		TextView isCompleteText;
		
		
		if ( convertView == null )
		{
			convertView = mInflator.inflate(R.layout.setting_row, null);
			
			imageView = (ImageView) convertView.findViewById(R.id.settingRowIcon);
			titleText = (TextView) convertView.findViewById(R.id.settingRowTitleTextView);	
			isCompleteText = (TextView) convertView.findViewById(R.id.settingRowIsComplete);	
		}
		else
		{
			//recycle row
			imageView = (ImageView) convertView.findViewById(R.id.settingRowIcon);
			titleText = (TextView) convertView.findViewById(R.id.settingRowTitleTextView);
			isCompleteText = (TextView) convertView.findViewById(R.id.settingRowIsComplete);
		}
		

		imageView.setImageResource( setting.icon );
		titleText.setText( setting.title );
		
		if ( setting.shouldValidate )
		{
			isCompleteText.setVisibility(View.VISIBLE);
			
			int resourceID = R.string.userSelectionsComplete;
			int colourID = mConext.getResources().getColor(R.color.complete_green);
			if ( !setting.isComplete )
			{
				resourceID = R.string.userSelectionsInComplete;
				colourID = mConext.getResources().getColor(R.color.incomplete_red);
			}
			
			isCompleteText.setText( resourceID );
			isCompleteText.setTextColor( colourID );
		}
		else
		{
			isCompleteText.setVisibility(View.GONE);
		}
	
		convertView.setTag(setting);

		return convertView;
	}
	

}
