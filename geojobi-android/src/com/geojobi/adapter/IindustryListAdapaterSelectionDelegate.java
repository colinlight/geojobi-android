package com.geojobi.adapter;

import com.geojobi.model.IndustryFilter;

public interface IindustryListAdapaterSelectionDelegate {
	
	public void itemSelected( IndustryFilter filter );
}
