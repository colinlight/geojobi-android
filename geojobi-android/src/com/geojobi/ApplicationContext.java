package com.geojobi;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.geojobi.base.IAppDependencies;
import com.geojobi.base.IAppStateDelegate;
import com.geojobi.base.IJobDataDelegate;
import com.geojobi.base.IServiceDelegate;
import com.geojobi.base.ISettingsDelegate;
import com.geojobi.base.IUserOptionsDelegate;
import com.geojobi.manager.SettingsManager;
import com.geojobi.model.AppStateDelegate;
import com.geojobi.model.Settings;
import com.geojobi.model.db.AppDatabaseHelper;
import com.geojobi.model.parser.VacanciesDataParser;
import com.geojobi.webservice.AsyncLookingService;
import com.geojobi.webservice.AsyncWebService;
import com.geojobi.webservice.IServiceCallDelegate;
import com.geojobi.webservice.ServiceCall;
import com.geojobi.webservice.WebService;
import com.geojobi.webservice.WebServiceConstants;
import com.geojobi.webservice.WebserviceResultReceiver;


public class ApplicationContext extends Application implements IAppDependencies, IServiceDelegate, 
													WebserviceResultReceiver.Reciever
{

	public static final String TAG = "ApplicationContext";

	private static ApplicationContext instance;
	
	private static AppDatabaseHelper mAppDBHelper;
	
	private ISettingsDelegate mSettingsDelegate;
	
	private IAppStateDelegate mAppStateDelegate;
	
	private WebserviceResultReceiver mServiceReceiver;
	
	public ApplicationContext()
	{
		super();
		
		this.mSettingsDelegate = new SettingsManager(this);
		this.mAppStateDelegate = new AppStateDelegate();
	}
	

	@Override
	public ISettingsDelegate getSettingsDelegate() 
	{
		return this.mSettingsDelegate;
	}
	
	@Override
	public IUserOptionsDelegate getOptionsDelegate()
	{
		return mAppDBHelper;
	}
	
	@Override
	public IJobDataDelegate getJobDataDelegate()
	{
		return mAppDBHelper;
	}
	
	@Override
	public SQLiteDatabase getWriteableDatabase()
	{
		return mAppDBHelper.getWritableDatabase();
	}
	
	
	@Override
	public IServiceDelegate getServiceDelegate() 
	{
		return this;
	}
	
	@Override
	public IAppStateDelegate getAppStateDelegate()
	{
		return mAppStateDelegate;
	}


	@Override 
	public void onCreate(){

		super.onCreate();
		mAppDBHelper = new AppDatabaseHelper(this);
		
		this.mServiceReceiver = new WebserviceResultReceiver(new Handler());
	}
	

	@Override 
	public void onLowMemory()
	{
		super.onLowMemory();
	}

	@Override 
	public void onTerminate()
	{
		super.onTerminate();
		mAppDBHelper.close();
		mAppDBHelper = null;
	}

	public static Context getContext() 
	{
		return instance;
	}
	
	/**
	 * Implement IServiceDelegate methods
	 */
	@Override
	public void callServiceForMethod( String method, WebserviceResultReceiver reciever ) 
	{
		final Intent serviceIntent = new Intent(Intent.ACTION_SYNC, null, this, com.geojobi.webservice.WebService.class);
		serviceIntent.putExtra( WebService.RECEIVER, reciever );
		serviceIntent.putExtra( WebService.COMMAND, WebService.QUERY);
		serviceIntent.putExtra( WebServiceConstants.METHOD, method);
		startService(serviceIntent);
	}
	
	@Override
	public void callAsyncServiceWithDelegate( IServiceCallDelegate delegate )
	{
		new AsyncWebService().execute(delegate);
	}
	
	@Override
	public void getLatestJobs()
	{
		IServiceCallDelegate sendUsersProfile = new ServiceCall(this.mServiceReceiver, "", 
				AsyncWebService.POST, null, null, new VacanciesDataParser( mAppDBHelper, this ), null );
		
		sendUsersProfile.setContext( getApplicationContext());
		new AsyncLookingService().execute(sendUsersProfile);
	}
	
	
	public void onReceiveResult( int resultCode, Bundle resultData)
	{
		switch( resultCode )
		{
		case WebServiceConstants.STATUS_RUNNING:
			Log.v(TAG, "running service");
			break;
		case WebServiceConstants.STATUS_FINISHED:
			Log.v(TAG, "date service complete -->");
			
			break;
		case WebServiceConstants.STATUS_FAILED:
			Log.v(TAG, "service failed");
			break;
		}
	}
	

}
