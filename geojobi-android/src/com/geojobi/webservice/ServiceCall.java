package com.geojobi.webservice;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.StringEntity;

import android.app.AlertDialog;
import android.content.Context;
import android.os.ResultReceiver;

import com.geojobi.base.IDataParser;

public class ServiceCall implements IServiceCallDelegate {
	
	
	public ServiceCall( ResultReceiver receiver, String method, int methodType, 
			List<NameValuePair> headers, List<NameValuePair> params, IDataParser resultParser, 
			AlertDialog dialog )
	{
		this.mReceiver = receiver;
		this.mMethod = method;
		this.mMethodType = methodType;
		this.mHeaders = headers;
		this.mParams = params;
		this.mResultParser = resultParser;
		this.mDialog = dialog;
	}

	private ResultReceiver mReceiver;
	@Override
	public void setReceiver(ResultReceiver receiver) {
		this.mReceiver = receiver;
	}
	@Override
	public ResultReceiver getReceiver() {
		return mReceiver;
	}

	
	private IDataParser mResultParser;
	@Override
	public void setResultParser(IDataParser parser) {
		this.mResultParser = parser;
	}
	@Override
	public IDataParser getResultParser() {
		return mResultParser;
	}

	
	private String mMethod;
	@Override
	public void setMethod(String method) {
		this.mMethod = method;
	}
	@Override
	public String getMethod() {
		return mMethod;
	}
	
	
	private List<NameValuePair> mHeaders;
	@Override
	public void setHeader( List<NameValuePair> header)
	{
		this.mHeaders = header;
	}
	@Override
	public List<NameValuePair> getHeaders()
	{
		return this.mHeaders;
	}
	
	
	private int mMethodType;
	@Override
	public void setMethodType(int type) {
		this.mMethodType = type;
	}
	@Override
	public int getMethodType() {
		return mMethodType;
	}
	
	
	
	private List<NameValuePair> mParams;
	@Override
	public void setParams( List<NameValuePair> params)
	{
		this.mParams = params;
	}
	@Override
	public List<NameValuePair> getParams()
	{
		return mParams;
	}
	@Override
	public StringEntity getParamsAsEntity() throws UnsupportedEncodingException
	{
		return new UrlEncodedFormEntity( this.mParams );
	}
	
	
	

	private AlertDialog mDialog;
	@Override
	public void setDialog(AlertDialog dialog) {
		this.mDialog = dialog;
	}
	@Override
	public AlertDialog getDialog() {
		return mDialog;
	}
	
	
	private Context mContext;
	@Override
	public void setContext( Context context)
	{
		this.mContext = context;
	}
	@Override
	public Context getContext()
	{
		return mContext;
	}
	
	
	private String mCandidateGUIDString;
	@Override
	public void setCandidateGUIDString( String guid )
	{
		this.mCandidateGUIDString = guid;
	}
	@Override
	public String getCandidateGUIDString()
	{
		return mCandidateGUIDString;
	}
	
	
}
