package com.geojobi.webservice;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.StringEntity;

import com.geojobi.base.IDataParser;

import android.app.AlertDialog;
import android.content.Context;
import android.os.ResultReceiver;

public interface IServiceCallDelegate {
	
	public void setReceiver( ResultReceiver receiver );
	public ResultReceiver getReceiver();
	
	public void setResultParser( IDataParser parser );
	public IDataParser getResultParser();
	
	public void setMethod( String method );
	public String getMethod();
	
	public void setDialog( AlertDialog dialog );
	public AlertDialog getDialog();
	
	public void setMethodType(int type);
	public int getMethodType();
	
	public void setHeader( List<NameValuePair> header);
	public List<NameValuePair> getHeaders();
	
	public void setParams( List<NameValuePair> params);
	public List<NameValuePair> getParams();
	public StringEntity getParamsAsEntity() throws UnsupportedEncodingException;
	
	public void setContext( Context context);
	public Context getContext();
	
	public void setCandidateGUIDString( String guid);
	public String getCandidateGUIDString();

}
