package com.geojobi.webservice;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.os.ResultReceiver;
import android.util.Log;

import com.geojobi.base.IDataParser;

public class SetJobStatusServiceCall extends ServiceCall {

public static final String TAG = "GetJobServiceCall";
	
	public SetJobStatusServiceCall( ResultReceiver receiver, String method, int methodType, 
			List<NameValuePair> headers, List<NameValuePair> params, IDataParser resultParser, 
			AlertDialog dialog) 
	{
	    super( receiver, method, methodType, headers, params, resultParser, dialog );
	}
	
	private int mStatus;
	public void setStatus( int value )
	{
		mStatus = value;
	}
	
	private int mRejectReasons;
	public void setRejectReasons( int value )
	{
		mRejectReasons = value;
	}
	
	private String mResponseOne, mResponseTwo, mResponseThree, mResponseFour = "";
	public void setResponses( String one, String two, String three, String four )
	{
		mResponseOne = one;
		mResponseTwo = two;
		mResponseThree = three;
		mResponseFour = four;
		
	}
	
	
	@Override
	public StringEntity getParamsAsEntity() throws UnsupportedEncodingException
	{
		return new StringEntity( this.parseCandidateJSON() );
	}
	
	private String parseCandidateJSON()
	{
		JSONObject candidateDetails = new JSONObject();		
		try
		{
			JSONObject jsonDetails = new JSONObject();
			jsonDetails.put("candidateguid",  this.getCandidateGUIDString() );
			jsonDetails.put("candidatedecisionid",  mStatus );
			jsonDetails.put("rejectionreasonid",  mRejectReasons );
			jsonDetails.put("responsetext1",  mResponseOne );
			jsonDetails.put("responsetext2",  mResponseTwo );
			jsonDetails.put("responsetext3",  mResponseThree );
			jsonDetails.put("responsetext4",  mResponseFour );
	
			JSONArray jsonCandidateArray = new JSONArray();
			jsonCandidateArray.put(jsonDetails);
			
			candidateDetails.put("response", jsonCandidateArray);	

		}
		catch (JSONException e) {
			Log.v(TAG, e.toString());
		}
		return candidateDetails.toString();
	}
}
