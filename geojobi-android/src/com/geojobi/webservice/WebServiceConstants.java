package com.geojobi.webservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class WebServiceConstants {
	
	public static final boolean DEBUG = false;

	//METHODS
	public static final String LOGIN = "candidate/login/";
	
	public static final String GET_CURRENCY = "currency";
	
	public static final String GET_ALL_OPTIONS = "lookups?type=all";
	
	
	public static final String UPDATE_PROFILE = "candidate/";
	
	public static final String GET_VACANCIES = "vacancies/";
	
	public static final String GET_VACANCY = "vacancy/";
	
	public static final String RESPOND_TO_VACANCY = "vacancy/response/";
	

	
	public static final String REGISTER_NEW_CANDIDATE = "candidate/register/";
	
	
	public static final String EMAIL_PARAM = "emailaddress";
	
	public static final String PASSWORD_PARAM = "password";
	
	
	
	
	//DATA TYPES
	public static final String METHOD = "method";
	
	public static final String PARSER = "parser";
	
	//RETURN DATA KEY
	public static final String DATA = "data";
	
	//STATUS
	public static final int STATUS_RUNNING = 0;
	
	public static final int STATUS_FINISHED = 1;
	
	public static final int STATUS_FAILED = 2;
	
	public static final int STATUS_FINISHED_WITH_ERROR = 3;
	
	//URL
	public static final String HOST = "http://www.geojobi.com/";
	
	public static final String SERVICE_PATH = "_webservices/";
	
	//AUTH
	public static final String AUTHORISATION_GUID = "65F5EC45-FA73-4100-A422-9A69617D491E";
	
	public static final String JSON_HEADER_TYPE = "application/json";
	
	
	//HEADERS
	public static final String AUTHORISATION_HEADER = "Authorisation";
	
	public static final String ACCEPT_HEADER = "Accept";
	
	public static final String CONTENT_TYPE_HEADER = "Content-type";
	
	
	public static List<NameValuePair> getHeaders()
	{
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add( new BasicNameValuePair( AUTHORISATION_HEADER, AUTHORISATION_GUID) );
		return headers;
	}
	
	public static List<NameValuePair> getHeadersForJson()
	{
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add( new BasicNameValuePair( AUTHORISATION_HEADER, AUTHORISATION_GUID) );
		headers.add( new BasicNameValuePair( ACCEPT_HEADER, JSON_HEADER_TYPE) );
		headers.add( new BasicNameValuePair( CONTENT_TYPE_HEADER, JSON_HEADER_TYPE) );
		return headers;
	}
	
	
	
	
	
	
}
