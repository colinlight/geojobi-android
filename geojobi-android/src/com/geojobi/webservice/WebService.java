package com.geojobi.webservice;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class WebService extends IntentService {

	public static final String TAG = "WebService";
	
	public static final String RECEIVER = "receiver";

	public static final String COMMAND = "command";
	
	public static final String QUERY = "query";
	
	
	public WebService() {
		super("WebService");
	}
	
	
	@Override
	protected void onHandleIntent(Intent intent) {
		final ResultReceiver receiver = intent.getParcelableExtra(RECEIVER);
		String command = intent.getStringExtra(COMMAND);
		String method = intent.getStringExtra(WebServiceConstants.METHOD);
		
		Bundle data = new Bundle();
		
		if ( command.equals(QUERY))
		{
			receiver.send(WebServiceConstants.STATUS_RUNNING, Bundle.EMPTY);
			String url = WebServiceConstants.HOST + WebServiceConstants.SERVICE_PATH + method;
			
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet get = new HttpGet(url);
			try
			{
				get.setHeader("Authorisation", WebServiceConstants.AUTHORISATION_GUID);
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				String responseBody = httpClient.execute(get, responseHandler);
				
				if ( responseBody != null )
				{
					data.putString(WebServiceConstants.DATA, responseBody);
					receiver.send(WebServiceConstants.STATUS_FINISHED, data);
					
				}	
			}
			catch(Exception e )
			{
				Log.v(TAG, e.getLocalizedMessage() + " : " + e.getCause() + " : " + e.getMessage() );
				data.putString(Intent.EXTRA_TEXT, e.toString());
				receiver.send(WebServiceConstants.STATUS_FAILED, data);
			}
		}
		
		this.stopSelf();
	}
	

}
