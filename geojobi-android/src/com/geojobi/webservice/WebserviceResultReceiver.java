package com.geojobi.webservice;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class WebserviceResultReceiver extends ResultReceiver {
	
	private Reciever _receiver;
	
	public WebserviceResultReceiver(Handler handler)
	{
		super(handler);
	}
	
	public void setReceiver(Reciever receiver)
	{
		_receiver = receiver;
	}
	
	public interface Reciever 
	{
		public void onReceiveResult( int resultCode, Bundle resultData);
	}
	
	@Override
	protected void onReceiveResult( int resultCode, Bundle resultData)
	{
		if ( _receiver != null )
		{
			_receiver.onReceiveResult(resultCode, resultData);
		}
	}

}
