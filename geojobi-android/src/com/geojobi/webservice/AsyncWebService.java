package com.geojobi.webservice;


import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;


import com.geojobi.base.IDataParser;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class AsyncWebService extends AsyncTask<IServiceCallDelegate, Integer, String> {

	public static final String TAG = "AsyncWebService";
	
	public static final int GET = 0;
	
	public static final int POST = 1;
	
	private IServiceCallDelegate delegate;

	@Override
	protected String doInBackground(IServiceCallDelegate... params) 
	{
		this.delegate = params[0];
		
		if ( this.delegate.getMethodType() == GET )
			this.getData();
		
		if ( this.delegate.getMethodType() == POST )
			this.postData();
		
		return null;
	}

	
	protected void onProgressUpdate(Integer... progress) {
		//        setProgressPercent(progress[0]);
		sendStatus( WebServiceConstants.STATUS_RUNNING,  Bundle.EMPTY );
	}

	
	protected void onPostExecute(String result) {
	}
	
	
	protected void onPreExecute()
	{
		Log.v(TAG, "START" );
	}
	
	
	protected void getData()
	{
		String url = WebServiceConstants.HOST + WebServiceConstants.SERVICE_PATH + 
				delegate.getMethod();
		
		Bundle data = new Bundle();
		data.putString(WebServiceConstants.METHOD, delegate.getMethod());
		
		HttpClient httpClient = new DefaultHttpClient();
		isDebug( httpClient );
		
		try
		{
			
			if ( this.delegate.getParams() != null )
			{
				List<NameValuePair> paramData = this.delegate.getParams();
				url += "?" + URLEncodedUtils.format(paramData, "utf-8");
			}
			
			HttpGet get = new HttpGet( url );
			
			for( NameValuePair header : this.delegate.getHeaders() )
			{
				get.setHeader(header.getName(), header.getValue() );
			}
				
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = httpClient.execute(get, responseHandler);

			if ( responseBody != null )
			{
				
				if ( delegate.getResultParser() != null )
				{
					IDataParser parser = delegate.getResultParser();
					parser.parseData(responseBody);
					
					if ( parser.hasError() )
					{
						data.putString(WebServiceConstants.DATA, parser.getError() );
						sendStatus( WebServiceConstants.STATUS_FINISHED_WITH_ERROR,  data );
						return;
					}
				}
				
				data.putString(WebServiceConstants.DATA, responseBody);
				sendStatus( WebServiceConstants.STATUS_FINISHED,  data );

			}	
		}
		catch(Exception e )
		{
			if ( delegate.getReceiver() == null )
				return;
			
			Log.v(TAG, e.getLocalizedMessage() + " : " + e.getCause() + " : " + e.getMessage() );
			data.putString(Intent.EXTRA_TEXT, e.toString());
			sendStatus( WebServiceConstants.STATUS_FAILED,  data );
		}
	}
	
	
	
	protected void postData()
	{
		String url = WebServiceConstants.HOST + WebServiceConstants.SERVICE_PATH + 
				delegate.getMethod();
		
		Bundle data = new Bundle();
		data.putString(WebServiceConstants.METHOD, delegate.getMethod());
		
		HttpClient httpClient = new DefaultHttpClient();
		isDebug( httpClient );
		
		HttpPost post = new HttpPost(url);
		try
		{
			
			for( NameValuePair header : this.delegate.getHeaders() )
			{
				post.setHeader(header.getName(), header.getValue() );
			}		
			post.setEntity( this.delegate.getParamsAsEntity() );
			
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = httpClient.execute(post, responseHandler);

			if ( responseBody != null )
			{
				
				if ( delegate.getResultParser() != null )
				{
					IDataParser parser = delegate.getResultParser();
					parser.parseData(responseBody);
					
					if ( parser.hasError() )
					{
						data.putString(WebServiceConstants.DATA, parser.getError() );
						sendStatus( WebServiceConstants.STATUS_FINISHED_WITH_ERROR,  data );
						return;
					}
				}
				
				data.putString(WebServiceConstants.DATA, responseBody);
				sendStatus( WebServiceConstants.STATUS_FINISHED,  data );
			}	
		}
		catch(Exception e )
		{
			if ( delegate.getReceiver() == null )
				return;
			
			Log.v(TAG, e.getLocalizedMessage() + " : " + e.getCause() + " : " + e.getMessage() );
			data.putString(Intent.EXTRA_TEXT, e.toString());
			sendStatus( WebServiceConstants.STATUS_FAILED,  data );
			
		}
	}
	
	
	private void isDebug( HttpClient httpClient )
	{
		if ( !WebServiceConstants.DEBUG )
			return;
		
		HttpHost host = new HttpHost("192.168.1.64", 8888, "http");
		httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, host);
	}
	
	private void sendStatus( int status, Bundle data )
	{
		if ( delegate.getReceiver() != null  )
			delegate.getReceiver().send( status, data);
	}


}
