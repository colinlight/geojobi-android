package com.geojobi.webservice;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.os.ResultReceiver;
import android.util.Log;

import com.geojobi.base.IDataParser;
import com.geojobi.model.db.VacancyColumns;

public class GetJobServiceCall extends ServiceCall {
	
	public static final String TAG = "GetJobServiceCall";
	
	public GetJobServiceCall( ResultReceiver receiver, String method, int methodType, 
			List<NameValuePair> headers, List<NameValuePair> params, IDataParser resultParser, 
			AlertDialog dialog) 
	{
	    super( receiver, method, methodType, headers, params, resultParser, dialog );
	}
	
	@Override
	public StringEntity getParamsAsEntity() throws UnsupportedEncodingException
	{
		return new StringEntity( this.parseCandidateJSON() );
	}
	
	private String parseCandidateJSON()
	{
		JSONObject candidateDetails = new JSONObject();		
		try
		{
			JSONObject jsonDetails = new JSONObject();
			jsonDetails.put( "guid",  this.getCandidateGUIDString() );		
			JSONArray jsonCandidateArray = new JSONArray();
			jsonCandidateArray.put(jsonDetails);
			candidateDetails.put("candidate", jsonCandidateArray);			
		}
		catch (JSONException e) {
			Log.v(TAG, e.toString());
		}
		return candidateDetails.toString();
	}
	
}
