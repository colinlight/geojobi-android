	package com.geojobi.webservice;


import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.geojobi.base.IDataParser;
import com.geojobi.model.Settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

public class AsyncLookingService extends
		AsyncTask<com.geojobi.webservice.IServiceCallDelegate, Integer, String> {
	
	public static final String TAG = "AsyncLookingService";
	
	private IServiceCallDelegate delegate;

	
	@Override
	protected String doInBackground(IServiceCallDelegate... params) 
	{
		this.delegate = params[0];
		this.postData();
		this.getData();
		
		return null;
	}
	
	
	protected void onProgressUpdate(Integer... progress) {
		//        setProgressPercent(progress[0]);
		delegate.getReceiver().send(WebServiceConstants.STATUS_RUNNING, Bundle.EMPTY);
	}

	
	protected void onPostExecute(String result) {
	}
	
	
	protected void onPreExecute()
	{
		Log.v(TAG, "START" );
	}
	
	
	
	private String parseCandidateJSON()
	{
		JSONObject candidateDetails = new JSONObject();
		
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this.delegate.getContext());
		
		try
		{
			//create details object
			JSONObject jsonDetails = new JSONObject();
			jsonDetails.put(Settings.USER_DETAILS_EMAIL,  appPrefs.getString(Settings.USER_DETAILS_EMAIL, "") );
			jsonDetails.put(Settings.USER_DETAILS_NAME,  appPrefs.getString(Settings.USER_DETAILS_NAME, "") );
			jsonDetails.put(Settings.USER_DETAILS_MOBILE,  appPrefs.getString(Settings.USER_DETAILS_MOBILE, "") );
			jsonDetails.put(Settings.MIN_SALARY,  appPrefs.getInt(Settings.MIN_SALARY, -1) );
			jsonDetails.put(Settings.MIN_SALARY_CONTRACT,  appPrefs.getInt(Settings.MIN_SALARY_CONTRACT, -1) );
			jsonDetails.put(Settings.CAREER_EXPERIENCE, appPrefs.getInt(Settings.CAREER_EXPERIENCE, 0) );
			jsonDetails.put(Settings.SOLID_EXPERIENCE, appPrefs.getInt(Settings.SOLID_EXPERIENCE, 0) );
			jsonDetails.put(Settings.OWN_SOLID_LICENSE, appPrefs.getInt(Settings.OWN_SOLID_LICENSE, 0) );
			
			jsonDetails.put(Settings.INDUSTRY_EXPERIENCE + "1" + Settings.ID,  
					appPrefs.getInt(Settings.INDUSTRY_EXPERIENCE + "1" + Settings.ID, 0) );
			jsonDetails.put(Settings.INDUSTRY_EXPERIENCE + "2" + Settings.ID,  
					appPrefs.getInt(Settings.INDUSTRY_EXPERIENCE + "2" + Settings.ID, 0) );
			jsonDetails.put(Settings.INDUSTRY_EXPERIENCE + "3" + Settings.ID,  
					appPrefs.getInt(Settings.INDUSTRY_EXPERIENCE + "3" + Settings.ID, 0) );
			jsonDetails.put(Settings.INDUSTRY_EXPERIENCE + "4" + Settings.ID,  
					appPrefs.getInt(Settings.INDUSTRY_EXPERIENCE + "4" + Settings.ID, 0) );
			jsonDetails.put(Settings.INDUSTRY_EXPERIENCE + "5" + Settings.ID,  
					appPrefs.getInt(Settings.INDUSTRY_EXPERIENCE + "5" + Settings.ID, 0) );
		
			jsonDetails.put(Settings.INDUSTRY_EXPERIENCE_YEARS + "1" + Settings.ID,  
					appPrefs.getInt(Settings.INDUSTRY_EXPERIENCE_YEARS + "1" + Settings.ID, 0) );
			jsonDetails.put(Settings.INDUSTRY_EXPERIENCE_YEARS + "2" + Settings.ID,  
					appPrefs.getInt(Settings.INDUSTRY_EXPERIENCE_YEARS + "2" + Settings.ID, 0) );
			jsonDetails.put(Settings.INDUSTRY_EXPERIENCE_YEARS + "3" + Settings.ID,  
					appPrefs.getInt(Settings.INDUSTRY_EXPERIENCE_YEARS + "3" + Settings.ID, 0) );
			jsonDetails.put(Settings.INDUSTRY_EXPERIENCE_YEARS + "4" + Settings.ID,  
					appPrefs.getInt(Settings.INDUSTRY_EXPERIENCE_YEARS + "4" + Settings.ID, 0) );
			jsonDetails.put(Settings.INDUSTRY_EXPERIENCE_YEARS + "5" + Settings.ID,  
					appPrefs.getInt(Settings.INDUSTRY_EXPERIENCE_YEARS + "5" + Settings.ID, 0) );
		
			jsonDetails.put(Settings.JOB_TYPE,  appPrefs.getInt(Settings.JOB_TYPE, 0) );
			
			parseStringListToJson( appPrefs, Settings.SOLIDWORKS_SKILLS, jsonDetails, null, 0 );
			
			jsonDetails.put(Settings.UNITS, appPrefs.getInt( Settings.UNITS, 0) );
			
			parseStringListToJson( appPrefs, Settings.ROLE_DETAILS_ENJOY, jsonDetails, null, 0 );
			
			jsonDetails.put(Settings.NOTICE_PERIOD,  appPrefs.getInt(Settings.NOTICE_PERIOD, 0) );
			
			jsonDetails.put(Settings.USER_LOCATION_LAT_LONG + "1",  
					appPrefs.getString( Settings.USER_LOCATION_LAT_LONG + "0", "") );
			jsonDetails.put(Settings.USER_LOCATION_LAT_LONG + "2",  
					appPrefs.getString( Settings.USER_LOCATION_LAT_LONG + "1", "") );
			jsonDetails.put(Settings.USER_LOCATION_LAT_LONG + "3",  
					appPrefs.getString( Settings.USER_LOCATION_LAT_LONG + "2", "") );
			jsonDetails.put(Settings.USER_LOCATION_LAT_LONG + "4",  
					appPrefs.getString( Settings.USER_LOCATION_LAT_LONG + "3", "") );
			jsonDetails.put(Settings.USER_LOCATION_LAT_LONG + "5",  
					appPrefs.getString( Settings.USER_LOCATION_LAT_LONG + "4", "") );
			
			parseStringListToJson( appPrefs, Settings.ROLE_DETAILS_ENJOY, jsonDetails, null, 0 );
			
			parseStringListToJson( appPrefs, Settings.ROLE_DETAILS_IMPROVE, jsonDetails, 
					Settings.ROLE_DETAILS_ENJOY, 3  );
			
			//add to array
			JSONArray jsonCandidateArray = new JSONArray();
			jsonCandidateArray.put(jsonDetails);
			
			//set candidate item as array of details
			candidateDetails.put("candidate", jsonCandidateArray);			
		}
		catch (JSONException e) {
			Log.v(TAG, e.toString());
		}
		
		return candidateDetails.toString();
	}
	
	private void parseStringListToJson( SharedPreferences appPrefs, String settingName, JSONObject json, 
			String substituteName, int substituteIndex  )
	{
		String selections = appPrefs.getString( settingName , "");
		String [] listOfSelections = selections.split(",");
		int totalItems = listOfSelections.length;
		
		String name = ( substituteName != null )? substituteName : settingName;
		int startIndex = ( substituteIndex > 0 )? substituteIndex : 0;
		
		for ( int i=0; i < totalItems; i++)
		{
			String itemName = name + ( i + startIndex + 1) + Settings.ID;
			try{
				json.put( itemName,  listOfSelections[i] );
			}
			catch (JSONException e) {
				Log.v(TAG, e.toString());
			}
		}
	}
	
	
	private void postData()
	{
		
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this.delegate.getContext());
		
		String url = WebServiceConstants.HOST + WebServiceConstants.SERVICE_PATH + 
				 WebServiceConstants.UPDATE_PROFILE + appPrefs.getString(Settings.CANDIDATE_GUID, "");
		
		Bundle data = new Bundle();
		data.putString(WebServiceConstants.METHOD, delegate.getMethod());
		
		HttpClient httpClient = new DefaultHttpClient();
		isDebug( httpClient );
		
		HttpPost post = new HttpPost(url);
		try
		{
			post.setHeader("Authorisation", WebServiceConstants.AUTHORISATION_GUID);
			
			//Added
			post.setHeader("Accept", "application/json");
			post.setHeader("Content-type", "application/json");
			//Changed
			post.setEntity( new StringEntity( this.parseCandidateJSON() ));
			
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = httpClient.execute(post, responseHandler);

			if ( responseBody != null )
			{
				//TODO: handle error here
			}	
		}
		catch(Exception e )
		{
			Log.v(TAG, e.getLocalizedMessage() + " : " + e.getCause() + " : " + e.getMessage() );
			data.putString(Intent.EXTRA_TEXT, e.toString());
			delegate.getReceiver().send(WebServiceConstants.STATUS_FAILED, data);
		}
	}
	
	private void getData()
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this.delegate.getContext());
		
		String url = WebServiceConstants.HOST + WebServiceConstants.SERVICE_PATH + 
				 WebServiceConstants.GET_VACANCIES + appPrefs.getString(Settings.CANDIDATE_GUID, "");
		
		Bundle data = new Bundle();
		data.putString(WebServiceConstants.METHOD, delegate.getMethod());
		
		HttpClient httpClient = new DefaultHttpClient();
		isDebug( httpClient );
		
		try
		{
			
			HttpGet get = new HttpGet( url );
			
			get.setHeader("Authorisation", WebServiceConstants.AUTHORISATION_GUID);
				
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = httpClient.execute(get, responseHandler);

			if ( responseBody != null )
			{
				
				if ( delegate.getResultParser() != null )
				{
					IDataParser parser = delegate.getResultParser();
					parser.parseData(responseBody);
					
					if ( parser.hasError() )
					{
						data.putString(WebServiceConstants.DATA, parser.getError() );
						delegate.getReceiver().send(WebServiceConstants.STATUS_FINISHED_WITH_ERROR, data);
						return;
					}
				}
				
				data.putString(WebServiceConstants.DATA, responseBody);
				delegate.getReceiver().send(WebServiceConstants.STATUS_FINISHED, data);

			}	
		}
		catch(Exception e )
		{
			Log.v(TAG, e.getLocalizedMessage() + " : " + e.getCause() + " : " + e.getMessage() );
			data.putString(Intent.EXTRA_TEXT, e.toString());
			delegate.getReceiver().send(WebServiceConstants.STATUS_FAILED, data);
		}
	}
	
	
	private void isDebug( HttpClient httpClient )
	{
		if ( !WebServiceConstants.DEBUG )
			return;
		
		HttpHost host = new HttpHost("192.168.1.64", 8888, "http");
		httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, host);
	}

}
