package com.geojobi.webservice;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;

import android.os.ResultReceiver;
import android.util.Log;

import com.geojobi.base.IDataParser;
import com.geojobi.model.Settings;

public class UpdateEmailServiceCall extends ServiceCall {
	
public static final String TAG = "UpdateEmailServiceCall";
	
	private String mEmail; 
	
	public UpdateEmailServiceCall( ResultReceiver receiver, String method, int methodType, 
			List<NameValuePair> headers, List<NameValuePair> params, IDataParser resultParser, 
			AlertDialog dialog, String email ) 
	{
	    super( receiver, method, methodType, headers, params, resultParser, dialog );
	    
	    mEmail = email;
	}
	
	@Override
	public StringEntity getParamsAsEntity() throws UnsupportedEncodingException
	{
		return new StringEntity( this.parseCandidateJSON() );
	}

	
	private String parseCandidateJSON()
	{
		JSONObject candidateDetails = new JSONObject();		
		try
		{
			JSONObject jsonDetails = new JSONObject();
			jsonDetails.put(Settings.USER_DETAILS_EMAIL,  mEmail );
			JSONArray jsonCandidateArray = new JSONArray();
			jsonCandidateArray.put(jsonDetails);
			candidateDetails.put("candidate", jsonCandidateArray);			
		}
		catch (JSONException e) {
			Log.v(TAG, e.toString());
		}
		return candidateDetails.toString();
	}

}
