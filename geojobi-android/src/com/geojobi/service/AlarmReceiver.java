package com.geojobi.service;

import java.util.Date;

import com.geojobi.ApplicationContext;
import com.geojobi.activity.HomeActivity;
import com.geojobi.model.Settings;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {

	public static final long ALARM_INTERVAL = 5000;
	
	public static final long ALARM_TRIGGER_TIME = SystemClock.elapsedRealtime() + 3000;
	
	public static final long ALARM_MAX = 20000;
	
	//public static final long ALARM_MAX = 144000000 * 14;
			
			
	private static final String TAG = "AlramReceiver";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		ApplicationContext app = (ApplicationContext)context.getApplicationContext();
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		
		//get start date as long from prefs
		long l = appPrefs.getLong(Settings.ALARM_START_DATE, new Date().getTime() );
		//convert this to a start Date
		Date alarmStartDate = new Date(l);
		//add on the number of milliseconds from start date to end date
		l += ALARM_MAX;
		//convert the end data in milliseconds to a stop/end date
		Date alarmStopDate = new Date(l);
		//get the current date
		Date now = new Date();
		
		
		//calculate the number of days remaining e.g. subtract the number of days passed form 
		//the total number of days e.g. 14.
		long timeRemaining = ALARM_MAX - ( now.getDay() - alarmStartDate.getDay() );
		
		Log.v(TAG, "AlarmReceiver : " + now + " : " + alarmStopDate + " : " + Long.toString(timeRemaining) );
		
	
		//if ( app.getSettingsDelegate().getState(Settings.IS_USER_LOOKING) == false )
		if ( now.after(alarmStopDate) )
		{
			Log.v(TAG, "AlarmReceiver : stopped >");
			//stop the alarm
			PendingIntent pendIntent = 
					PendingIntent.getBroadcast(context, 9999, new Intent( context, AlarmReceiver.class ), 0);
			
			AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarm.cancel(pendIntent);
			
			app.getSettingsDelegate().setState(Settings.IS_USER_LOOKING, false);
			
			
			if ( HomeActivity.mHomeActivity != null )
			{
				HomeActivity home = (HomeActivity)HomeActivity.mHomeActivity;
				home.setLookingButtonState();
			}
			return;
		}
		
		
		app.getServiceDelegate().getLatestJobs();
		
	}

}
