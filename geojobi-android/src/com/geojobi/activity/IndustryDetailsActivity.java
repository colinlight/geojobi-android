package com.geojobi.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.geojobi.ApplicationContext;
import com.geojobi.R;
import com.geojobi.base.BaseActivity;
import com.geojobi.base.IOptionSelections;
import com.geojobi.helper.AlertHelper;
import com.geojobi.helper.DropDownListHelper;
import com.geojobi.model.Settings;
import com.geojobi.model.db.DatabaseTableConstants;
import com.geojobi.model.db.OptionColumns;

public class IndustryDetailsActivity extends BaseActivity implements OnItemSelectedListener {

	public static final String TAG = "IndustryDetailsActivity";

	private IOptionSelections mOptionSelections;

	private Spinner mIndustrySpinner;

	@Override
	public void onCreate( Bundle saveInstanceState )
	{
		super.onCreate( saveInstanceState );
		this.setContentView(R.layout.industry_details);

		mIndustrySpinner = (Spinner)findViewById(R.id.industryDetailJobsSpinner);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.chosenIndustries,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mIndustrySpinner.setAdapter(adapter);

		setCurrentSelectionsList( Settings.SOLIDWORKS_SKILLS_ALL_SELECTIONS, 
				R.id.industryDetailSkillSelectionsText );


		String setting = Settings.FILTER_BY_INDUSTRY;
		mIndustrySpinner.setOnItemSelectedListener(this);
		mIndustrySpinner.setTag(setting);
		mIndustrySpinner.setSelection( this.getSettingsDelegate().getInt(setting) );


		setSpinnerDataModelFor( R.id.userDefaultsSWExpSpinner, 
				DatabaseTableConstants.YEARSEXPERIENCE, Settings.SOLID_EXPERIENCE );

		setSpinnerDataModelFor( R.id.userDefaultsSWLicenseSpinner, 
				DatabaseTableConstants.SOLIDWORKSCANDIDATELICENSE, Settings.OWN_SOLID_LICENSE );

		TextView title = (TextView)findViewById(R.id.headerBackNextTitle);
		title.setText(R.string.sectionTitleIndustryDetails);
	}

	public void displayFilterActivity(View view)
	{
		this.startActivity(new Intent( this, IndustryFilterActivity.class));
	}

	public void displaySkills(View view)
	{
		Dialog alert = onCreateDialog();
		alert.show();
	}

	@Override
	public void onResume()
	{
		super.onResume();

		setCurrentSelectionsList( Settings.INDUSTRY_EXPERIENCE_ALL_SELECTIONS, 
				R.id.industryDetailFilterSelectionsText );
	}

	@Override
	public void onPause()
	{
		super.onPause();

		boolean industryValid = true;
		if ( mIndustrySpinner.getSelectedItemPosition() > 0 )
		{
			industryValid = this.getSettingsDelegate().isBooleanSettingsValid( 
					Settings.MANDATORY_INDUSTRY_DETAILS_VALUES); 
		}
		
		this.getSettingsDelegate().setState(Settings.INDUSTRY_DETAILS_VALID, industryValid);
	}


	protected Dialog onCreateDialog() {

		mOptionSelections = this.getOptionsDelegate().getOptionsOfType( 
				DatabaseTableConstants.SOLIDWORKSSKILLS, 
				this.getSettingsDelegate().getString(Settings.SOLIDWORKS_SKILLS ));

		Dialog alert = new AlertDialog.Builder(IndustryDetailsActivity.this)
		.setTitle(R.string.industryDetailSkillsTxt)
		.setMultiChoiceItems(mOptionSelections.getNames(), 
				mOptionSelections.getSelectedItems(),
				new DialogInterface.OnMultiChoiceClickListener() {
			public void onClick(DialogInterface dialog, int whichButton,
					boolean isChecked) {

				final AlertDialog alert = (AlertDialog)dialog;
				final ListView listView = alert.getListView();
				final int total =  listView.getCheckItemIds().length;

				if ( total >= 6 )
				{
					Context context = getApplicationContext();
					Toast toast = Toast.makeText(context, 
							R.string.industryDetailSkillsToastTxt, Toast.LENGTH_SHORT);
					toast.show();
					listView.setItemChecked(whichButton, false);
					isChecked = false;
				}
				mOptionSelections.getSelectedItems()[whichButton] = isChecked;
			}
		})
		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {


			}
		})
		.setPositiveButton("Save", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				final AlertDialog alert = (AlertDialog)dialog;
				final ListView listView = alert.getListView();
				long[] total =  listView.getCheckItemIds();

				ApplicationContext context = (ApplicationContext)getApplicationContext();

				context.getSettingsDelegate().setString(Settings.SOLIDWORKS_SKILLS, 
						mOptionSelections.getSelectedIDs() );

				String selections = "";
				for( int i = 0 ; i < total.length; i++)
				{
					selections+= context.getSettingsDelegate().getListSettingFormat( 
							(String)listView.getItemAtPosition( (int)total[i] ) );
				}
				context.getSettingsDelegate().setString(Settings.SOLIDWORKS_SKILLS_ALL_SELECTIONS, 
						selections);

				setCurrentSelectionsList( Settings.SOLIDWORKS_SKILLS_ALL_SELECTIONS, 
						R.id.industryDetailSkillSelectionsText );

				isValid();
			}
		})
		.create();

		return alert;
	}

	private void isValid()
	{
		String [] skillsSettings = { Settings.SOLIDWORKS_SKILLS };
		boolean valid = this.getSettingsDelegate().isStringSettingsValid( skillsSettings ); 
		this.getSettingsDelegate().setState(Settings.SOLIDWORKS_SKILLS_VALID, valid);
	}


	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) 
	{

		Spinner spinner = (Spinner)parent;

		if ( spinner == mIndustrySpinner)
		{
			this.getSettingsDelegate().setInt( (String)spinner.getTag() , pos );
			return;
		}

		Cursor cursor = ((SimpleCursorAdapter)parent.getAdapter()).getCursor();
		int typeIDIndex = cursor.getColumnIndex(OptionColumns.TYPE_ID);

		this.getSettingsDelegate().setInt( (String)spinner.getTag() , cursor.getInt(typeIDIndex) );
	}

	public void onNothingSelected(AdapterView<?> parent) {

	}

	public void showNext( View v )
	{
		this.finish();
		this.startActivity( new Intent( this, DesignPreferencesActivity.class ));
	}


	public void showSettings( View v )
	{
		this.finish();
	}
	
	public void displayHelp( View v )
    {
    	AlertHelper.createAlertDialog(this, R.string.helpDialogTitle, R.string.industryDetailHelpTxt, 
    			R.string.helpDialogCloseButton);
    }


	private void setCurrentSelectionsList( String setting, int textViewId )
	{
		TextView textView = (TextView)findViewById( textViewId );
		
		String allSelections = this.getSettingsDelegate().getString( setting );
		if ( allSelections.length() <= 0 )
		{
			textView.setVisibility(View.GONE);
			return;
		}
			
		textView.setVisibility(View.VISIBLE);
		textView.setText(allSelections);
	}


	private Spinner setSpinnerDataModelFor( int id, String optionType, String setting )
	{
		Spinner spinner = (Spinner)findViewById(id);

		Cursor cursor = this.getOptionsDelegate().getCursorForOptionsOfType(optionType);
		startManagingCursor(cursor);

		DropDownListHelper.setDataModelForDropDownFromOptions( cursor, spinner, this );

		DropDownListHelper.setSelectedDropDownItem(cursor, spinner, this, 
				this.getSettingsDelegate().getInt(setting) );

		spinner.setOnItemSelectedListener(this);

		spinner.setTag(setting);

		return spinner;
	}

}
