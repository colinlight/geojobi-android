package com.geojobi.activity;


import java.util.ArrayList;
import java.util.List;

import com.geojobi.R;
import com.geojobi.adapter.UserSelectionsAdapter;
import com.geojobi.base.BaseActivity;
import com.geojobi.base.BaseListActivity;
import com.geojobi.helper.AlertHelper;
import com.geojobi.model.Setting;
import com.geojobi.model.Settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListView;
import android.widget.TextView;


public class UserSelectionsActivity extends BaseActivity {

	public static final String TAG = "UserSelectionsActivity";

	private ListView mSettingsList;
	
	private Activity mSettingActivity; 

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.settings);
		this.mSettingActivity = this;
	}


	@Override
	public void onResume()
	{
		super.onResume();
		
		mSettingsList = (ListView)findViewById(R.id.settingsList);

		UserSelectionsAdapter adapter = new UserSelectionsAdapter(this, this.getSettingModel());

		mSettingsList.setAdapter(adapter);

		mSettingsList.setItemsCanFocus(false);
		mSettingsList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		PaintDrawable div = new PaintDrawable(android.R.drawable.divider_horizontal_dark);
		mSettingsList.setDivider(div);
		mSettingsList.setDividerHeight(4);


		mSettingsList.setOnItemClickListener( new AdapterView.OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> adapater, View arg1, int position,
					long arg3) {
				
				Setting setting = (Setting)adapater.getAdapter().getItem(position);
				startActivity(new Intent( mSettingActivity, setting.activity ));
				
			}} );
	}

	private List<Setting> getSettingModel()
	{
		List<Setting> settingData = new ArrayList<Setting>();

		settingData.add( new Setting( android.R.drawable.ic_menu_preferences, 
				this.getString(R.string.userSelectionsDetails), 
				this.getSettingsDelegate().getState(Settings.USER_DETAILS_VALID),
				true, UserDetailActivity.class) );

		settingData.add( new Setting( android.R.drawable.ic_menu_preferences, 
				this.getString(R.string.userSelectionsIndustry), 
				this.getSettingsDelegate().getState(Settings.INDUSTRY_DETAILS_VALID),
				true, IndustryDetailsActivity.class) );

		settingData.add( new Setting( android.R.drawable.ic_menu_preferences, 
				this.getString(R.string.userSelectionsDesign), 
				this.getSettingsDelegate().getState(Settings.DESIGN_PREFERENCE_VALID), 
				true, DesignPreferencesActivity.class) );

		settingData.add( new Setting( android.R.drawable.ic_menu_preferences, 
				this.getString(R.string.userSelectionsRole),
				this.getSettingsDelegate().getState(Settings.ROLE_DETAILS_VALID),
				true, RoleDetailsActivity.class) );

		settingData.add( new Setting( android.R.drawable.ic_menu_preferences, 
				this.getString(R.string.userSelectionsLetter), true, 
				false, CoverLetterActivity.class) );

		settingData.add( new Setting( android.R.drawable.ic_menu_preferences, 
				this.getString(R.string.userSelectionsLocations), 
				this.getSettingsDelegate().getState(Settings.USER_LOCATIONS_VALID), 
				true, UserLocationsActivity.class) 
				);

		return settingData;
	}
	
	
	public void showHome( View v )
	{
		this.finish();
	}
	
	public void showNext( View v )
	{
		this.startActivity(new Intent( this, UserDetailActivity.class ));
	}
	
	public void displayHelp( View v )
    {
		AlertDialog alert = new AlertDialog.Builder(this)
		.setTitle(R.string.helpDialogTitle)
		.setMessage(R.string.userSelectionsHelpTxt)
		.setPositiveButton(R.string.helpDialogCloseButton, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		})
		.setNegativeButton(R.string.userSelectionsHelpDialogTutorBtn, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				startActivity(new Intent(getApplicationContext(), VideoActivity.class));
			}
		})
		.create();
		alert.show();
    }
}
