package com.geojobi.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.geojobi.R;
import com.geojobi.base.IAppDependencies;
import com.geojobi.components.JobLocationDialog;
import com.geojobi.helper.AlertHelper;
import com.geojobi.helper.IOverlayAlertDelegate;
import com.geojobi.helper.JobLocationsOverlay;
import com.geojobi.helper.JobLocationsOverlayHelper;
import com.geojobi.helper.JobOverlayItem;
import com.geojobi.helper.LocationHelper;
import com.geojobi.model.Settings;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;

public class UserLocationsActivity extends MapActivity implements IOverlayAlertDelegate {

	public static final String TAG = "UserLocationsActivity";

	private MapView map;

	private MapController controller;

	private JobLocationsOverlay mJobOverlays;

	private IAppDependencies mContext;

	private Geocoder mGeoCoder;

	private LocationManager mLoctionManager;

	private String mProvider;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.user_locations);

		mContext = (IAppDependencies)this.getApplicationContext();

		map = (MapView) findViewById(R.id.map);
		controller = map.getController();

		map.setBuiltInZoomControls(true);

		Drawable pin = this.getResources().getDrawable(R.drawable.geojobi_icon);
		ArrayList<JobOverlayItem> jobOverlays = 
				JobLocationsOverlayHelper.getPreviousLocations( mContext.getSettingsDelegate() );

		mJobOverlays = new JobLocationsOverlay(this, pin,
				this.getApplicationContext(), jobOverlays);
		map.getOverlays().add(mJobOverlays);

		// initialise current location
		final MyLocationOverlay overlay = new MyLocationOverlay(this, map);
		// not this does not work in the emulator
		overlay.enableMyLocation();
		overlay.runOnFirstFix(new Runnable() {
			public void run() {
				// Zoom in to current location
				controller.setZoom(8);
				controller.animateTo(overlay.getMyLocation());
			}
		});
		
		mLoctionManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
	    mProvider = mLoctionManager.getBestProvider(criteria, false);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}


	public void showHelp( View v )
	{
		AlertHelper.createAlertDialog(this, R.string.helpDialogTitle, R.string.userLocationsHelpDialogPrompt, 
				R.string.helpDialogCloseButton);
	}

	public void showSettings( View v )
	{
		this.finish();
	}

	public void showJobList( View v )
	{
		this.startActivity(new Intent( this, UserLocationsListActivity.class));
		this.finish();
	}

	public void findLocation( View v )
	{
		EditText editText = (EditText)findViewById(R.id.userLocationsSearchTxt);
		String searchText = editText.getText().toString();

		if ( searchText.length() <= 0 )
		{
			Toast invalidToast = Toast.makeText(this, R.string.userLocationsInvalidSearchToast, Toast.LENGTH_SHORT);
			invalidToast.show();
			return;
		}

		if ( mGeoCoder == null )
			mGeoCoder = new Geocoder(this);

		try {
			List<Address> locations = mGeoCoder.getFromLocationName(searchText, 1);
			if ( locations.size() <= 0 )
			{
				Toast toastInValid = Toast.makeText(this, 
						R.string.userLocationsInvalidLocationToastTxt, Toast.LENGTH_SHORT);

				toastInValid.show();
				return;
			}

			Address address = locations.get(0);
			GeoPoint point = new GeoPoint( (int)( address.getLatitude() * 1E6 ), (int)( address.getLongitude() * 1E6 ) );
			displayDialog(point, locations);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * IOverlayAlertDelegate methods
	 */
	public void displayDialog(GeoPoint point, List<Address> addressList) {

		JobLocationDialog dialog = new JobLocationDialog(this, this, mJobOverlays, 
				addressList.get(0),  LocationHelper.getGeoPointForLocation(mLoctionManager, mProvider), 
				point, false, null);
		dialog.show();
	}

	public void displayDialog(JobOverlayItem overlay, List<Address> addressList) {

		JobLocationDialog dialog = new JobLocationDialog(this, this, mJobOverlays, 
				addressList.get(0),  LocationHelper.getGeoPointForLocation(mLoctionManager, mProvider), 
				overlay.getPoint(), true, overlay);
		dialog.show();
	}

	public void overlayUpdated()
	{
		map.invalidate();
	}
	
	


}
