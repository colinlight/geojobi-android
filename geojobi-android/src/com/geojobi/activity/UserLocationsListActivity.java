package com.geojobi.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PaintDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.geojobi.R;
import com.geojobi.base.BaseActivity;
import com.geojobi.components.JobLocationDialog;
import com.geojobi.helper.AlertHelper;
import com.geojobi.helper.IOverlayAlertDelegate;
import com.geojobi.helper.JobLocationsOverlay;
import com.geojobi.helper.JobLocationsOverlayHelper;
import com.geojobi.helper.JobOverlayItem;
import com.geojobi.helper.LocationHelper;
import com.google.android.maps.GeoPoint;

public class UserLocationsListActivity extends BaseActivity implements OnItemClickListener, IOverlayAlertDelegate {

	private Geocoder mGeoCoder;

	private JobLocationsOverlay mJobOverlays;
	
	private LocationManager mLoctionManager;
	
	private String mProvider;
	
	private ListView mListView;
	
	private ArrayList<JobOverlayItem> mOverlayList;
	
	private ArrayAdapter<JobOverlayItem> mAadapter;

	@Override
	public void onCreate( Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.user_locations_list);
		
		mListView = (ListView)findViewById(R.id.userLocationsList);

		mOverlayList = JobLocationsOverlayHelper.getPreviousLocations( this.getSettingsDelegate() );

		mAadapter = new ArrayAdapter<JobOverlayItem>(
				this, android.R.layout.simple_list_item_1,
				android.R.id.text1, mOverlayList
				);
		

		mListView.setAdapter(mAadapter);

		PaintDrawable div = new PaintDrawable(android.R.drawable.divider_horizontal_dark);
		mListView.setDivider(div);
		mListView.setDividerHeight(4);

		mListView.setOnItemClickListener(this);

		Drawable pin = this.getResources().getDrawable(R.drawable.geojobi_icon);

		mJobOverlays = new JobLocationsOverlay(this, pin,
				this.getApplicationContext(), mOverlayList);	
		
		mLoctionManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
	    mProvider = mLoctionManager.getBestProvider(criteria, false);
	}

	public void showMap( View v )
	{
		this.startActivity(new Intent( this, UserLocationsActivity.class));
		this.finish();
	}

	public void showSettings( View v )
	{
		this.finish();
	}

	public void showHelp( View v )
	{	
		AlertHelper.createAlertDialog(this, R.string.helpDialogTitle, R.string.userLocationsListHelpDialogPrompt, 
				R.string.helpDialogCloseButton);
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View arg1, int position, long arg3) {

		JobOverlayItem overlay = (JobOverlayItem)adapter.getAdapter().getItem(position);
		List<Address> address = this.getAddressFromPoint(overlay.getPoint());
		
		JobLocationDialog dialog = new JobLocationDialog(this, this, mJobOverlays, 
				address.get(0), LocationHelper.getGeoPointForLocation(mLoctionManager, mProvider), 
				overlay.getPoint(), true, overlay);
		dialog.show();
	}

	private List<Address> getAddressFromPoint( GeoPoint point )
	{
		if ( mGeoCoder == null )
			mGeoCoder = new Geocoder(this);

		try {
			List<Address> locations = mGeoCoder.getFromLocation( point.getLatitudeE6()/1E6, 
					point.getLongitudeE6()/1E6, 1);

			if ( locations.size() <= 0 )
			{
				Toast toastInValid = Toast.makeText(this, 
						R.string.userLocationsInvalidLocationToastTxt, Toast.LENGTH_SHORT);

				toastInValid.show();
				return null;
			}
			return locations;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * IOverlayAlertDelegate methods
	 */
	public void displayDialog(GeoPoint point, List<Address> addressList) {}

	public void displayDialog(JobOverlayItem overlay, List<Address> addressList) {}

	public void overlayUpdated()
	{
		mAadapter.notifyDataSetChanged();		
	}

}
