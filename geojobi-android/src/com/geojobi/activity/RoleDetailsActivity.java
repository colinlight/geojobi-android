package com.geojobi.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.geojobi.ApplicationContext;
import com.geojobi.R;
import com.geojobi.base.BaseActivity;
import com.geojobi.base.IOptionSelections;
import com.geojobi.helper.AlertHelper;
import com.geojobi.model.Settings;
import com.geojobi.model.db.DatabaseTableConstants;

public class RoleDetailsActivity extends BaseActivity {

	private IOptionSelections mOptionSelections;

	private String mSetting;

	private String mSettingAllSelection;

	private int mAllSelectionTextView;

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.role_details);

		setCurrentSelectionsList( Settings.ROLE_DETAILS_ALL_ENJOY_SELECTIONS, 
				R.id.roleDetailEnjoySelectionsText );

		setCurrentSelectionsList( Settings.ROLE_DETAILS_ALL_IMPROVE_SELECTIONS, 
				R.id.roleDetailsImproveSelectionsText );
		
		TextView title = (TextView)findViewById(R.id.headerBackNextTitle);
		title.setText(R.string.sectionTitleRoleDetails);
	}

	public void displayEnjoyList( View view)
	{	
		Dialog alert = onCreateDialog( R.string.roleDetailsImproveButtonTxt, 
				DatabaseTableConstants.ENJOYALTERPOINTS, Settings.ROLE_DETAILS_ENJOY, 
				Settings.ROLE_DETAILS_ALL_ENJOY_SELECTIONS, R.id.roleDetailEnjoySelectionsText, 
				R.string.roleDetailsEnjoyTitleTxt );

		alert.show();
	}

	public void displayImproveList( View view )
	{		
		Dialog alert = onCreateDialog( R.string.roleDetailsImproveButtonTxt, 
				DatabaseTableConstants.ENJOYALTERPOINTS, Settings.ROLE_DETAILS_IMPROVE, 
				Settings.ROLE_DETAILS_ALL_IMPROVE_SELECTIONS, R.id.roleDetailsImproveSelectionsText, 
				R.string.roleDetailsImproveTitleTxt );

		alert.show();
	}

	public void showNext( View view )
	{
		this.finish();
		this.startActivity( new Intent( this, CoverLetterActivity.class ));
	}

	public void showSettings( View v )
	{
		this.finish();
	}
	
	public void displayHelp( View v )
    {
    	AlertHelper.createAlertDialog(this, R.string.helpDialogTitle, R.string.roleDetailsHelpTxt, 
    			R.string.helpDialogCloseButton);
    }

	protected Dialog onCreateDialog( int roledetailsenjoybuttontxt, String type, String setting, 
			String allSettingSelection, int textViewId,  int titleId ) {

		mOptionSelections = this.getOptionsDelegate().getOptionsOfType( type, 
				this.getSettingsDelegate().getString( setting ));

		mSetting = setting;

		mSettingAllSelection = allSettingSelection;

		mAllSelectionTextView = textViewId;

		Dialog alert = new AlertDialog.Builder(RoleDetailsActivity.this)
		.setTitle( titleId )
		.setMultiChoiceItems(mOptionSelections.getNames(), 
				mOptionSelections.getSelectedItems(),
				new DialogInterface.OnMultiChoiceClickListener() {
			public void onClick(DialogInterface dialog, int whichButton,
					boolean isChecked) {

				final AlertDialog alert = (AlertDialog)dialog;
				final ListView listView = alert.getListView();
				final int total =  listView.getCheckItemIds().length;

				if ( total >= 4 )
				{
					Context context = getApplicationContext();
					Toast toast = Toast.makeText(context, 
							R.string.roleDetailsToastTxt, Toast.LENGTH_SHORT);
					toast.show();
					listView.setItemChecked(whichButton, false);
					isChecked = false;
				}
				mOptionSelections.getSelectedItems()[whichButton] = isChecked;
			}
		})
		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {


			}
		})
		.setPositiveButton("Save", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				final AlertDialog alert = (AlertDialog)dialog;
				final ListView listView = alert.getListView();
				long[] total =  listView.getCheckItemIds();

				ApplicationContext context = (ApplicationContext)getApplicationContext();				
				context.getSettingsDelegate().setString( mSetting, mOptionSelections.getSelectedIDs() );

				isValid();

				String selections = "";
				for( int i = 0 ; i < total.length; i++)
				{
					selections+= context.getSettingsDelegate().getListSettingFormat( 
							(String)listView.getItemAtPosition( (int)total[i] ) );
				}
				context.getSettingsDelegate().setString( mSettingAllSelection, selections);

				setCurrentSelectionsList( mSettingAllSelection, mAllSelectionTextView );
			}
		})
		.create();

		return alert;
	}

	private void isValid()
	{
		boolean valid = this.getSettingsDelegate().isStringSettingsValid(Settings.MANDATORY_ROLE_DETAIL_VALUES); 
		this.getSettingsDelegate().setState(Settings.ROLE_DETAILS_VALID, valid);
	}

	private void setCurrentSelectionsList( String setting, int textViewId )
	{
		String allSelections = this.getSettingsDelegate().getString( setting );
		if ( allSelections.length() <= 0 )
			return;

		TextView textView = (TextView)findViewById( textViewId );
		textView.setVisibility(View.VISIBLE);
		textView.setText(allSelections);
	}

}
