package com.geojobi.activity;

import java.util.ArrayList;

import android.R.bool;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geojobi.R;
import com.geojobi.adapter.JobListAdapter;
import com.geojobi.base.BaseActivity;
import com.geojobi.base.IApplyForJobDelegate;
import com.geojobi.base.IDataParser;
import com.geojobi.helper.AlertHelper;
import com.geojobi.model.Job;
import com.geojobi.model.Settings;
import com.geojobi.model.db.DatabaseTableConstants;
import com.geojobi.model.db.VacancyColumns;
import com.geojobi.model.parser.JobStatusDataParser;
import com.geojobi.webservice.AsyncWebService;
import com.geojobi.webservice.IServiceCallDelegate;
import com.geojobi.webservice.SetJobStatusServiceCall;
import com.geojobi.webservice.WebServiceConstants;
import com.geojobi.webservice.WebserviceResultReceiver;

public class CoverLetterActivity extends BaseActivity implements WebserviceResultReceiver.Reciever {

	private static final String TAG = "CoverLetterActivity";

	public static final String SHOULD_SHOW_APPLY_BUTTON = "shouldShowApplyButton";
	
	public WebserviceResultReceiver serviceReceiver;

	private EditText mCoverLetter1;

	private EditText mCoverLetter2;

	private EditText mCoverLetter3;

	private EditText mCoverLetter4;

	private EditText mCoverLetter5;

	private EditText mCoverLetter6;

	//private IApplyForJobDelegate applyDelegate;

	private String mJobGuid;

	private boolean mApplied;
	
	private boolean mSavedEnabled = true;
	
	private boolean mApplyView = false;
	
	private Dialog mLoadingAlert;

	private ArrayList<CheckBox> mSelectedItems;


	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.cover_letter);

		mCoverLetter1 = (EditText)findViewById(R.id.coverLetterTextinput1);

		mCoverLetter2 = (EditText)findViewById(R.id.coverLetterTextinput2);

		mCoverLetter3 = (EditText)findViewById(R.id.coverLetterTextinput3);

		mCoverLetter4 = (EditText)findViewById(R.id.coverLetterTextinput4);

		mCoverLetter5 = (EditText)findViewById(R.id.coverLetterTextinput5);

		mCoverLetter6 = (EditText)findViewById(R.id.coverLetterTextinput6);
		
		TextView title = (TextView)findViewById(R.id.headerBackNextTitle);
		title.setText(R.string.sectionTitleCoverLetter);

		try
		{
			Bundle bundle = this.getIntent().getExtras();
			if ( bundle.getBoolean(SHOULD_SHOW_APPLY_BUTTON) )
			{
				showApply();
				mJobGuid = bundle.getString(VacancyColumns.GUID);
				mSavedEnabled = false;
				mApplyView = true;
				this.serviceReceiver = new WebserviceResultReceiver(new Handler());
			}
		}
		catch (Exception e) {
			LinearLayout headerBar = (LinearLayout)findViewById(R.id.coverLetterHeaderBar);
			headerBar.setVisibility( View.VISIBLE );
		}
	}

	private void showApply() 
	{
		LinearLayout headerBar = (LinearLayout)findViewById(R.id.coverLetterApplyHeaderBar);
		headerBar.setVisibility( View.VISIBLE );
		
		TextView promtTextView = (TextView)findViewById(R.id.coverLetterPromotTxt);
		promtTextView.setText(R.string.coverLetterPromotTxt);
		
		Button applyButton = (Button)findViewById(R.id.coverLetterApplyBtn);
		applyButton.setVisibility( View.VISIBLE );

		CheckBox checkOne = (CheckBox)findViewById(R.id.coverLettercheckBox1);
		checkOne.setTag(mCoverLetter1);
		checkOne.setVisibility(View.VISIBLE);

		CheckBox checkTwo = (CheckBox)findViewById(R.id.coverLettercheckBox2);
		checkTwo.setTag(mCoverLetter2);
		checkTwo.setVisibility(View.VISIBLE);

		CheckBox checkThree= (CheckBox)findViewById(R.id.coverLettercheckBox3);
		checkThree.setTag(mCoverLetter3);
		checkThree.setVisibility(View.VISIBLE);

		CheckBox checkFour = (CheckBox)findViewById(R.id.coverLettercheckBox4);
		checkFour.setTag(mCoverLetter4);
		checkFour.setVisibility(View.VISIBLE);

		CheckBox checkFive = (CheckBox)findViewById(R.id.coverLettercheckBox5);
		checkFive.setTag(mCoverLetter5);
		checkFive.setVisibility(View.VISIBLE);

		CheckBox checkSix = (CheckBox)findViewById(R.id.coverLettercheckBox6);
		checkSix.setTag(mCoverLetter6);
		checkSix.setVisibility(View.VISIBLE);

		mApplied = false;

		mSelectedItems = new ArrayList<CheckBox>();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		
		if ( !mSavedEnabled )
			return;

		SharedPreferences appPrefs = PreferenceManager
				.getDefaultSharedPreferences(this.getApplicationContext());

		SharedPreferences.Editor editor = appPrefs.edit();
		editor.putString(Settings.COVER_LETTER_1, mCoverLetter1.getText().toString() );
		editor.putString(Settings.COVER_LETTER_2, mCoverLetter2.getText().toString() );
		editor.putString(Settings.COVER_LETTER_3, mCoverLetter3.getText().toString() );
		editor.putString(Settings.COVER_LETTER_4, mCoverLetter4.getText().toString() );
		editor.putString(Settings.COVER_LETTER_5, mCoverLetter5.getText().toString() );
		editor.putString(Settings.COVER_LETTER_6, mCoverLetter6.getText().toString() );
		editor.commit();
		
		if ( mApplyView &&  ( this.serviceReceiver != null ))
			serviceReceiver.setReceiver(null);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		mCoverLetter1.setText( this.getSettingsDelegate().getString( Settings.COVER_LETTER_1 ));
		mCoverLetter2.setText( this.getSettingsDelegate().getString( Settings.COVER_LETTER_2 ));
		mCoverLetter3.setText( this.getSettingsDelegate().getString( Settings.COVER_LETTER_3 ));
		mCoverLetter4.setText( this.getSettingsDelegate().getString( Settings.COVER_LETTER_4 ));
		mCoverLetter5.setText( this.getSettingsDelegate().getString( Settings.COVER_LETTER_5 ));
		mCoverLetter6.setText( this.getSettingsDelegate().getString( Settings.COVER_LETTER_6 ));
		
		if ( mApplyView )
			this.serviceReceiver.setReceiver(this);
	}


//	public void setApplyDelegate( IApplyForJobDelegate delegate )
//	{
//		this.applyDelegate = delegate;
//	}

	public void applyForVacancy( View v )
	{
		if ( mApplied )
			return;

		mApplied = true;

		this.applyForJob(
				mJobGuid,
				getCoverLetterTextFromCheckBox( 0 ), 
				getCoverLetterTextFromCheckBox( 1 ),
				getCoverLetterTextFromCheckBox( 2 ), 
				getCoverLetterTextFromCheckBox( 3 ) 
				);
	}


	public void showNext( View v )
	{
		this.finish();
		this.startActivity( new Intent( this, UserLocationsActivity.class ));
	}

	public void showSettings( View v )
	{
		this.finish();
	}
	
	public void displayHelp( View v )
    {
    	AlertHelper.createAlertDialog(this, R.string.helpDialogTitle, R.string.coverLetterHelpTxt, 
    			R.string.helpDialogCloseButton);
    }
	
	public void showJobDetails( View v )
	{
		Intent jobDetailIntent = new Intent(this, JobDetailActivity.class);
		jobDetailIntent.putExtra(VacancyColumns.GUID, mJobGuid );

		startActivity( jobDetailIntent );
		finish();
	}

	public void checkBoxSelected( View v )
	{
		CheckBox selectedCheckBox = (CheckBox)v;		

		if ( !selectedCheckBox.isChecked() )
		{
			mSelectedItems.remove(selectedCheckBox);
			return;
		}


		if ( mSelectedItems.size() >= 4 )
		{
			Toast limitToast = Toast.makeText(this, R.string.coverLetterSelectionLimit, Toast.LENGTH_SHORT);
			limitToast.show();
			selectedCheckBox.setChecked(false);
			return;
		}

		mSelectedItems.add(selectedCheckBox);

	}

	private String getCoverLetterTextFromCheckBox( int index )
	{
		CheckBox checkbox = mSelectedItems.get(index);
		EditText editText = (EditText)checkbox.getTag();
		return editText.getText().toString();
	}
	
	
	
	public void onReceiveResult(int resultCode, Bundle resultData) {
		switch (resultCode) {
		case WebServiceConstants.STATUS_RUNNING:
			Log.v(TAG, "running service");
			break;
		case WebServiceConstants.STATUS_FINISHED:
			Log.v(TAG, "date service complete -->");
			mLoadingAlert.cancel();
			finish();
			break;
		case WebServiceConstants.STATUS_FAILED:
			Log.v(TAG, "service failed");
			mLoadingAlert.cancel();
			break;
		}
	}
	
	
	
	public void applyForJob( String jobGuid, String responseOne, String responseTwo, 
			String responseThree, String responseFour )
	{
		
		IDataParser parser = new JobStatusDataParser(getJobDataDelegate(), jobGuid, 
				DatabaseTableConstants.STATUS_INTERESTED );
		
		SetJobStatusServiceCall jobStatusCall = new SetJobStatusServiceCall( null,
				WebServiceConstants.RESPOND_TO_VACANCY + jobGuid, AsyncWebService.POST, 
				WebServiceConstants.getHeadersForJson(), null,
				parser, null);
		
		jobStatusCall.setContext(this);
		jobStatusCall.setCandidateGUIDString( 
				this.getSettingsDelegate().getString(Settings.CANDIDATE_GUID ));
		jobStatusCall.setStatus( DatabaseTableConstants.STATUS_INTERESTED );
		jobStatusCall.setResponses(responseOne, responseTwo, responseThree, responseFour);
		
		IServiceCallDelegate delegate = (IServiceCallDelegate)jobStatusCall;

		this.getServiceDelegate().callAsyncServiceWithDelegate(delegate);
		
//		mLoadingAlert = AlertHelper.createProgressAlert(this, R.string.applyTabApplyAlert);
//		mLoadingAlert.show();
		
	}

}
