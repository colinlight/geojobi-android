package com.geojobi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.geojobi.R;
import com.geojobi.base.BaseTabActivity;

public class JobsTabActivity extends BaseTabActivity {
	
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		
		final TabHost tabHost = getTabHost();

		TabSpec jobMapSpec = tabHost.newTabSpec("JobMap");
		jobMapSpec.setIndicator( this.getString(R.string.jobTabsMapTitle ) );
		Intent jobMapIntent = new Intent(this, JobMapActivity.class);
		jobMapIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		jobMapSpec.setContent(jobMapIntent);
		
		TabSpec jobListSpec = tabHost.newTabSpec("JobList");
		jobListSpec.setIndicator( this.getString(R.string.jobTabsJobListTitle ) );
		Intent jobListIntent = new Intent(this, JobListActivity.class);
		jobListSpec.setContent(jobListIntent);
		
		TabSpec jobArchiveListSpec = tabHost.newTabSpec("ArchivedJobList");
		jobArchiveListSpec.setIndicator( this.getString(R.string.jobTabsArchivedJobTitle ) );
		Intent jobArchiveListIntent = new Intent(this, JobArchiveActivity.class);
		jobArchiveListSpec.setContent(jobArchiveListIntent);
		
		
		tabHost.addTab(jobMapSpec);
		tabHost.addTab(jobListSpec);
		tabHost.addTab(jobArchiveListSpec);
	
	}

}
