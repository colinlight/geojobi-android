package com.geojobi.activity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.geojobi.R;
import com.geojobi.base.BaseActivity;
import com.geojobi.model.AppConstants;

public class VideoActivity extends BaseActivity {


	public void onCreate(Bundle savedInstanceState) {
	       
		   super.onCreate(savedInstanceState);
	       setContentView(R.layout.video_view);
	       
	       VideoView videoPlayer = (VideoView)findViewById(R.id.myvideoview); 
	       videoPlayer.setVideoURI(Uri.parse( AppConstants.HELP_VIDEO ));
	       videoPlayer.setMediaController(new MediaController(this));
	       videoPlayer.requestFocus();
	       videoPlayer.start();
	   }
}
