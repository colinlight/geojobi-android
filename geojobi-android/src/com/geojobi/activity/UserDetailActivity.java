package com.geojobi.activity;

import com.geojobi.R;
import com.geojobi.base.BaseActivity;
import com.geojobi.base.IDataParser;
import com.geojobi.helper.AlertHelper;
import com.geojobi.helper.DropDownListHelper;
import com.geojobi.helper.Network;
import com.geojobi.helper.ValidationHelper;
import com.geojobi.model.Settings;
import com.geojobi.model.db.DatabaseTableConstants;
import com.geojobi.model.db.OptionColumns;
import com.geojobi.model.parser.LoginDataParser;
import com.geojobi.webservice.AsyncWebService;
import com.geojobi.webservice.UpdateEmailServiceCall;
import com.geojobi.webservice.WebServiceConstants;
import com.geojobi.webservice.WebserviceResultReceiver;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class UserDetailActivity extends BaseActivity implements OnItemSelectedListener, WebserviceResultReceiver.Reciever {

	public static final String TAG = "UserDetailActivity";
	
	public WebserviceResultReceiver serviceReceiver;
	
	private TextView mFullnameTextView;
	
	private TextView mMobileTextView;
	
	private TextView mEmailTextView;
	
	private TextView mWebsiteTextView;
	
	private TextView mEducationTextView;
	
	private Dialog mAlertDialog;
	
	private String mUpdatedEmail;
	
	private Spinner mJobTypeSpinner;
	
	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.user_details);
		
		mFullnameTextView = (TextView)findViewById(R.id.userDefaultsName);
		
		mMobileTextView = (TextView)findViewById(R.id.userDefaultsMobile);
		
		mEmailTextView = (TextView)findViewById(R.id.userDefaultsEmail);
		
		mWebsiteTextView = (TextView)findViewById(R.id.userDefaultsWebsite);
		
		mEducationTextView = (TextView)findViewById(R.id.userDefaultsEducation);
		
		mJobTypeSpinner = setSpinnerDataModelFor( R.id.userDefaultsJobTypeSpinner, 
				DatabaseTableConstants.JOBTYPE, Settings.JOB_TYPE, 
				DatabaseTableConstants.JOBTYPE_DEFAULT );
		
		setSalarySpinner();
				
		setSpinnerDataModelFor( R.id.userDefaultsNoticeSpinner, 
				DatabaseTableConstants.NOTICEPERIOD, Settings.NOTICE_PERIOD, 
				DatabaseTableConstants.NOTICEPERIOD_DEFAULT );
		
		setSpinnerDataModelFor( R.id.userDefaultsCareerExpSpinner, 
				DatabaseTableConstants.YEARSEXPERIENCE, Settings.CAREER_EXPERIENCE, 0 );
				
		setSpinnerDataModelFor( R.id.userDefaultsUnitsSpinner, 
				DatabaseTableConstants.UNITS, Settings.UNITS, 0 );
		
		this.serviceReceiver = new WebserviceResultReceiver(new Handler());
		
		TextView title = (TextView)findViewById(R.id.headerBackNextTitle);
		title.setText(R.string.sectionTitleUserDetail);
	
	}
	
	
	
	@Override
	public void onPause()
	{
		super.onPause();
		
		SharedPreferences appPrefs = PreferenceManager
				.getDefaultSharedPreferences(this.getApplicationContext());
		
		SharedPreferences.Editor editor = appPrefs.edit();
		editor.putString(Settings.USER_DETAILS_NAME, mFullnameTextView.getText().toString() );
		editor.putString(Settings.USER_DETAILS_MOBILE, mMobileTextView.getText().toString() );
		editor.putString(Settings.USER_DETAILS_EMAIL, mEmailTextView.getText().toString() );
		editor.putString(Settings.USER_DETAILS_WEBSITE, mWebsiteTextView.getText().toString() );
		editor.putString(Settings.USER_DETAILS_EDUCATION, mEducationTextView.getText().toString() );
		editor.commit();
		
		boolean isValid = this.getSettingsDelegate().isStringSettingsValid(Settings.MANDATORY_USER_DETAIL_STRINGS);
		if ( isValid )
			isValid = this.getSettingsDelegate().isIntegerSettingsValid(Settings.MANDATORY_USER_DETAIL_VALUES);
		
		this.getSettingsDelegate().setState(Settings.USER_DETAILS_VALID, isValid);
		
		
		if ( this.serviceReceiver != null )
			serviceReceiver.setReceiver(null);
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		mFullnameTextView.setText( this.getSettingsDelegate().getString(Settings.USER_DETAILS_NAME));
		mMobileTextView.setText( this.getSettingsDelegate().getString(Settings.USER_DETAILS_MOBILE));
		mEmailTextView.setText( this.getSettingsDelegate().getString(Settings.USER_DETAILS_EMAIL));
		mWebsiteTextView.setText( this.getSettingsDelegate().getString(Settings.USER_DETAILS_WEBSITE));
		mEducationTextView.setText( this.getSettingsDelegate().getString(Settings.USER_DETAILS_EDUCATION));	
		
		this.serviceReceiver.setReceiver(this);
	}
	
	
	public void onReceiveResult( int resultCode, Bundle resultData)
	{
		switch( resultCode )
		{
		case WebServiceConstants.STATUS_RUNNING:
			break;
		case WebServiceConstants.STATUS_FINISHED:
			Log.v(TAG, "date service complete -->");
			mAlertDialog.cancel();
			mEmailTextView.setText( mUpdatedEmail );
			this.getSettingsDelegate().setString(Settings.USER_DETAILS_EMAIL, mUpdatedEmail);
			break;
		case WebServiceConstants.STATUS_FAILED:
		case WebServiceConstants.STATUS_FINISHED_WITH_ERROR:
			mAlertDialog.cancel();
			AlertHelper.createAlertDialog( this, R.string.userDefaultsUpdateAlertFailed, 
					resultData.getString( WebServiceConstants.DATA ));
			break;
		}
	}
	
	public void updateEmail( View v )
	{
		if ( !Network.checkNetWorkStatus(this, R.string.alertNoInternet ) )
    		return;
    	
        LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView = factory.inflate(R.layout.update_email_dialog, null);
        
        AlertDialog dialog =  new AlertDialog.Builder(UserDetailActivity.this)
            .setIcon(android.R.drawable.ic_dialog_email)
            .setTitle(R.string.changeEmailDialogTitle)
            .setView(textEntryView)
            .setPositiveButton(R.string.changeEmailDialogSave, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                	AlertDialog alert = (AlertDialog)dialog;
                	
                	EditText emailEditText = (EditText)alert.findViewById(R.id.changeEmailDialogEditText);
                	
                	
                	if ( ValidationHelper.isEmailValid( emailEditText.getText().toString() ) )
	               	{ 
                		saveNewEmail( emailEditText.getText().toString() );
	               	}
	               	
                }
            })
            .setNegativeButton(R.string.changeEmailDialogCancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {}
            })
            .create();
        
        dialog.show();
	}
	
	
	private void setSalarySpinner()
	{
		int typeID = this.getSettingsDelegate().getInt(Settings.JOB_TYPE);
		String setting = Settings.MIN_SALARY;
		String dbOptionType = DatabaseTableConstants.SALARYPERMANENT;
		int defaultIndex = DatabaseTableConstants.SALARYPERMANENT_DEFAULT;
		
		if ( typeID > 1 )
		{
			setting =  Settings.MIN_SALARY_CONTRACT;
			dbOptionType = DatabaseTableConstants.SALARYCONTRACT;
			defaultIndex = 0;
		}
		
		setSpinnerDataModelFor( R.id.userDefaultsMinSalarySpinner, dbOptionType, setting, defaultIndex );
	}
	
	
	private void saveNewEmail( String email )
	{
		mUpdatedEmail = email;
		
		String method = WebServiceConstants.UPDATE_PROFILE + 
				this.getSettingsDelegate().getString(Settings.CANDIDATE_GUID);
		
		IDataParser resultParser = new LoginDataParser(this.getSettingsDelegate(), email);
		
		UpdateEmailServiceCall serviceCall = new UpdateEmailServiceCall(serviceReceiver, method, 
				AsyncWebService.POST, WebServiceConstants.getHeadersForJson(), null, resultParser, null, email);
		
		this.getServiceDelegate().callAsyncServiceWithDelegate(serviceCall);
	
		mAlertDialog = AlertHelper.createProgressAlert(this, R.string.userDefaultsUpdateAlert);
		mAlertDialog.show();
	}

	
	private Spinner setSpinnerDataModelFor( int id, String optionType, String setting, int defaultIndex )
	{
		Spinner spinner = (Spinner)findViewById(id);
		
		Cursor cursor = this.getOptionsDelegate().getCursorForOptionsOfType(optionType);
		startManagingCursor(cursor);
		
		DropDownListHelper.setDataModelForDropDownFromOptions( cursor, spinner, this );
		
		DropDownListHelper.setSelectedDropDownItem(cursor, spinner, this, 
		this.getSettingsDelegate().getInt(setting), defaultIndex );
		
		spinner.setOnItemSelectedListener(this);
		
		spinner.setTag(setting);
			
		return spinner;
	}
	
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) 
	{
		Spinner spinner = (Spinner)parent;
		
		Cursor cursor = ((SimpleCursorAdapter)parent.getAdapter()).getCursor();
		int typeIDIndex = cursor.getColumnIndex(OptionColumns.TYPE_ID);
		
		this.getSettingsDelegate().setInt( (String)spinner.getTag() , cursor.getInt(typeIDIndex) );
		
		if ( mJobTypeSpinner == spinner )
			setSalarySpinner();
    }

    public void onNothingSelected(AdapterView<?> parent) {
       
    }
    
    
    public void showNext( View v )
    {
    	this.finish();
    	this.startActivity( new Intent( this, IndustryDetailsActivity.class ));
    }
    
    public void showSettings( View v )
    {
    	this.finish();
    }
    
    
    public void displayHelp( View v )
    {
    	AlertHelper.createAlertDialog(this, R.string.helpDialogTitle, R.string.userDefaultsHelpTxt, 
    			R.string.helpDialogCloseButton);
    }
    

}
