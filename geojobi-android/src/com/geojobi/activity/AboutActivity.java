package com.geojobi.activity;

import com.geojobi.R;
import com.geojobi.base.BaseActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AboutActivity extends BaseActivity {

	public static final String TAG = "AboutActivity";

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);

		TextView title = (TextView)findViewById(R.id.headerHomeTitle);
		title.setText(R.string.sectionTitleAbout);

	}


	public void showHome( View v )
	{
		this.finish();
	}

}
