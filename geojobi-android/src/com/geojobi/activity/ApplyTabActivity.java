package com.geojobi.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;


import com.geojobi.R;
import com.geojobi.base.BaseTabActivity;
import com.geojobi.base.IApplyForJobDelegate;
import com.geojobi.base.IDataParser;
import com.geojobi.helper.AlertHelper;
import com.geojobi.model.Job;
import com.geojobi.model.Settings;
import com.geojobi.model.db.DatabaseTableConstants;
import com.geojobi.model.db.VacancyColumns;
import com.geojobi.model.parser.JobStatusDataParser;
import com.geojobi.webservice.AsyncWebService;
import com.geojobi.webservice.IServiceCallDelegate;
import com.geojobi.webservice.SetJobStatusServiceCall;
import com.geojobi.webservice.WebServiceConstants;
import com.geojobi.webservice.WebserviceResultReceiver;


public class ApplyTabActivity extends BaseTabActivity implements IApplyForJobDelegate, 
WebserviceResultReceiver.Reciever {
	
	public WebserviceResultReceiver serviceReceiver;
	
	private static final String TAG = "ApplyTabActivity";
	
	private IApplyForJobDelegate mApplyDelegate;
	
	private Dialog mLoadingAlert;
	
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		
		Bundle bundle = this.getIntent().getExtras();
		String guid = bundle.getString(VacancyColumns.GUID);
		
		mApplyDelegate = this;
		
		
		this.serviceReceiver = new WebserviceResultReceiver(new Handler());
		
	
		final TabHost tabHost = getTabHost();

		TabSpec coverLetterSpec = tabHost.newTabSpec("CoverLetter");
		coverLetterSpec.setIndicator( this.getString(R.string.applyTabCoverLetter ) );
		Intent coverLetterIntent = new Intent(this, CoverLetterActivity.class);
		coverLetterIntent.putExtra(CoverLetterActivity.SHOULD_SHOW_APPLY_BUTTON, true);
		coverLetterIntent.putExtra(VacancyColumns.GUID, guid);
		coverLetterIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		coverLetterSpec.setContent(coverLetterIntent);
		
		TabSpec jobDetailsSpec = tabHost.newTabSpec("JobDetails");
		jobDetailsSpec.setIndicator( this.getString(R.string.applyTabCoverDetail ) );
		Intent jobDetailsIntent = new Intent(this, JobDetailActivity.class);
		jobDetailsIntent.putExtra(VacancyColumns.GUID, guid);
		jobDetailsIntent.putExtra(JobDetailActivity.SHOULD_HIDE_BUTTONS, true);
		jobDetailsSpec.setContent(jobDetailsIntent);
		
		
		tabHost.addTab(coverLetterSpec);
		tabHost.addTab(jobDetailsSpec);
		
		tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {
				checkCurrentActivity();
			}
		});
	}
	
	
	@Override
	public void onResume()
	{
		super.onResume();
		this.checkCurrentActivity();
	}
	
	private void checkCurrentActivity()
	{		
		Activity selectedActivity = getCurrentActivity();
		if ( selectedActivity instanceof CoverLetterActivity )
		{
			CoverLetterActivity coverLetterActivity = (CoverLetterActivity)selectedActivity;
			//coverLetterActivity.setApplyDelegate(mApplyDelegate);
		}
	}
	
	
	public void onReceiveResult(int resultCode, Bundle resultData) {
		switch (resultCode) {
		case WebServiceConstants.STATUS_RUNNING:
			Log.v(TAG, "running service");
			break;
		case WebServiceConstants.STATUS_FINISHED:
			Log.v(TAG, "date service complete -->");
			mLoadingAlert.cancel();
			finish();
			break;
		case WebServiceConstants.STATUS_FAILED:
			Log.v(TAG, "service failed");
			mLoadingAlert.cancel();
			break;
		}
	}
	
	
	public void applyForJob( String jobGuid, String responseOne, String responseTwo, 
			String responseThree, String responseFour )
	{
		
		IDataParser parser = new JobStatusDataParser(getJobDataDelegate(), jobGuid, 
				DatabaseTableConstants.STATUS_INTERESTED );
		
		SetJobStatusServiceCall jobStatusCall = new SetJobStatusServiceCall( null,
				WebServiceConstants.RESPOND_TO_VACANCY + jobGuid, AsyncWebService.POST, 
				WebServiceConstants.getHeadersForJson(), null,
				parser, null);
		
		jobStatusCall.setContext(this);
		jobStatusCall.setCandidateGUIDString( 
				this.getSettingsDelegate().getString(Settings.CANDIDATE_GUID ));
		jobStatusCall.setStatus( DatabaseTableConstants.STATUS_INTERESTED );
		jobStatusCall.setResponses(responseOne, responseTwo, responseThree, responseFour);
		
		IServiceCallDelegate delegate = (IServiceCallDelegate)jobStatusCall;

		this.getServiceDelegate().callAsyncServiceWithDelegate(delegate);
		
//		mLoadingAlert = AlertHelper.createProgressAlert(this, R.string.applyTabApplyAlert);
//		mLoadingAlert.show();
		
	}
	
	

}
