package com.geojobi.activity;


import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;


import com.geojobi.R;
import com.geojobi.helper.AlertHelper;
import com.geojobi.helper.Network;
import com.geojobi.helper.ValidationHelper;
import com.geojobi.model.parser.CreateAccountDataParser;
import com.geojobi.model.parser.LoginDataParser;
import com.geojobi.activity.HomeActivity;
import com.geojobi.base.BaseActivity;
import com.geojobi.webservice.AsyncWebService;
import com.geojobi.webservice.IServiceCallDelegate;
import com.geojobi.webservice.ServiceCall;
import com.geojobi.webservice.WebServiceConstants;
import com.geojobi.webservice.WebserviceResultReceiver;


public class LoginActivity extends BaseActivity implements WebserviceResultReceiver.Reciever  {
	
	public static final String TAG = "LoginActivity";
	
	public WebserviceResultReceiver serviceReceiver;
	
	private Dialog mAlertDialog;
	
	private ScrollView mLoginScrollView;
	
	 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        
        this.serviceReceiver = new WebserviceResultReceiver(new Handler());
        

        EditText emailText = (EditText)findViewById(R.id.loginEmail);
        mLoginScrollView = (ScrollView)findViewById(R.id.loginScrollView);
        
        emailText.setOnFocusChangeListener( new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				
				//Log.v(TAG, "has focu 1 --> " + mLoginScrollView + " : " + mLoginScrollView.getScrollY() );
				mLoginScrollView.scrollTo(0, 216);
				//Log.v(TAG, "has focu 2--> " + mLoginScrollView + " : " + mLoginScrollView.getScrollY() );
			}
		});
       
    }
    
 
    
    public void login(View v)
    {
    	if ( !Network.checkNetWorkStatus(this, R.string.alertNoInternet ) )
    		return;

    	EditText emailText = (EditText)findViewById(R.id.loginEmail);
    	String email = emailText.getText().toString();
    	EditText passwordText = (EditText)findViewById(R.id.loginPassword);

    	if ( ValidationHelper.isEmailValid( email ) && 
    			ValidationHelper.isStringLengthValid( passwordText.getText().toString() ) )
    	{ 

    		List<NameValuePair> params = new ArrayList<NameValuePair>(2);

    		params.add(new BasicNameValuePair( WebServiceConstants.EMAIL_PARAM, email));
    		
    		params.add(new BasicNameValuePair( WebServiceConstants.PASSWORD_PARAM, 
    				passwordText.getText().toString()));
    		
    		IServiceCallDelegate serviceCall = new ServiceCall(this.serviceReceiver, 
    				WebServiceConstants.LOGIN, AsyncWebService.GET, WebServiceConstants.getHeaders(), 
    				params, new LoginDataParser( this.getSettingsDelegate(), email ), null );

    		this.getServiceDelegate().callAsyncServiceWithDelegate(serviceCall);
    		
    		mAlertDialog = AlertHelper.createProgressAlert(this, R.string.loginProcessDialogTitle);
    		mAlertDialog.show();

    		return;
    	}

    	//display alert
    }
    
    public void createAccount(View v)
    {
    	if ( !Network.checkNetWorkStatus(this, R.string.alertNoInternet ) )
    		return;
    	
        LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView = factory.inflate(R.layout.create_account_dialog, null);
        AlertDialog dialog =  new AlertDialog.Builder(LoginActivity.this)
            .setIcon(R.drawable.geojobi_icon)
            .setTitle(R.string.createAccountDialogTitle)
            .setView(textEntryView)
            .setPositiveButton(R.string.createAccountDialogCreate, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                	AlertDialog alert = (AlertDialog)dialog;
                	EditText emailEditText = (EditText)alert.findViewById(R.id.createAccountEmail);
                	EditText passwordEditText = (EditText)alert.findViewById(R.id.createAccountPassword);
                	
                	if ( ValidationHelper.isEmailValid( emailEditText.getText().toString() ) && 
               			 ValidationHelper.isStringLengthValid( passwordEditText.getText().toString() ) )
	               	 { 
                		
                		validateNewAccount( emailEditText.getText().toString(), 
                				passwordEditText.getText().toString() );
	               		 return;
	               	 }
                	
                	//display alert
	                	
                	
                }
            })
            .setNegativeButton(R.string.createAccountDialogCancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {}
            })
            .create();
        
        dialog.show();
    }
    
    public void onPause()
    {
    	super.onPause();
    	
    	if ( this.serviceReceiver != null )
    		serviceReceiver.setReceiver(null);
    }
    
    public void onResume()
    {
    	super.onResume();
    	this.serviceReceiver.setReceiver(this);
    }
    
    public void onReceiveResult( int resultCode, Bundle resultData)
    {
    	String method = resultData.getString(WebServiceConstants.METHOD);
    	if ( method == WebServiceConstants.REGISTER_NEW_CANDIDATE )
    	{
    		createAccountResult( resultCode, resultData );
    		return;
    	}
    	
    	switch( resultCode )
    	{
    		case WebServiceConstants.STATUS_RUNNING:
    			break;
    		case WebServiceConstants.STATUS_FINISHED:
    			mAlertDialog.cancel();
    			Intent homeIntent = new Intent( this, HomeActivity.class);
    			homeIntent.putExtra(com.geojobi.activity.HomeActivity.IS_FIRST_RUN, true);
    			this.startActivity( homeIntent );
    			break;
    		case WebServiceConstants.STATUS_FAILED:
    			mAlertDialog.cancel();
    			Log.v(TAG, "service failed");
    			break;
    		case WebServiceConstants.STATUS_FINISHED_WITH_ERROR:
    			mAlertDialog.cancel();
    			AlertHelper.createAlertDialog( this, R.string.loginFailDialogTitle, 
    					resultData.getString( WebServiceConstants.DATA ));
    			break;
    	}
    }
    
    /**
     * Handle results for create account service call
     * 
     * @param resultCode
     * @param resultData
     */
    private void createAccountResult( int resultCode, Bundle resultData )
    {
    	switch( resultCode )
    	{
    		case WebServiceConstants.STATUS_RUNNING:
    			break;
    		case WebServiceConstants.STATUS_FINISHED:
    			mAlertDialog.cancel();
    			
    			String msg = this.getString(R.string.createAccountSuccessDialogPrompt);
    			AlertHelper.createAlertDialog( this, R.string.createAccountSuccessDialogTitle, msg);
    			break;
    		case WebServiceConstants.STATUS_FAILED:
    			mAlertDialog.cancel();
    			Log.v(TAG, "service failed");
    			break;
    		case WebServiceConstants.STATUS_FINISHED_WITH_ERROR:
    			mAlertDialog.cancel();
    			AlertHelper.createAlertDialog( this, R.string.createAccountFailDialogTitle, 
    					resultData.getString( WebServiceConstants.DATA ));
    			break;
    	}
    }
    
    
    
    /**
     * Validate new account settings on server
     * 
     * @param email
     * @param password
     */
    private void validateNewAccount( String email, String password )
    {
    	if ( !Network.checkNetWorkStatus(this, R.string.alertNoInternet ) )
    		return;
    	
    	List<NameValuePair> params = new ArrayList<NameValuePair>(2);
    	
    	params.add(new BasicNameValuePair( WebServiceConstants.EMAIL_PARAM, email));
        params.add(new BasicNameValuePair( WebServiceConstants.PASSWORD_PARAM, password));
		
    	IServiceCallDelegate serviceCall = new ServiceCall(this.serviceReceiver, 
				WebServiceConstants.REGISTER_NEW_CANDIDATE, AsyncWebService.POST, 
				WebServiceConstants.getHeaders(), params, 
				new CreateAccountDataParser( this.getSettingsDelegate() ), null );
    	
		this.getServiceDelegate().callAsyncServiceWithDelegate(serviceCall);
		
		mAlertDialog = AlertHelper.createProgressAlert(this, R.string.createAccountProcessDialogTitle);
		mAlertDialog.show();
    }
       
   
}
