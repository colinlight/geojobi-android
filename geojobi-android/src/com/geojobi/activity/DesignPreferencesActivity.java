package com.geojobi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.geojobi.R;
import com.geojobi.base.BaseActivity;
import com.geojobi.helper.AlertHelper;
import com.geojobi.model.Settings;

public class DesignPreferencesActivity extends BaseActivity {


	private static final int[] PREF_TYPE = {
		R.id.designTypeRadio1,
		R.id.designTypeRadio2,
		R.id.designTypeRadio3,
		R.id.designTypeRadio4,
		R.id.designTypeRadio5,
		R.id.designTypeRadio6
	};

	private static final int[] PREF_PREF = {
		R.id.designPrefRadio1,
		R.id.designPrefRadio2,
		R.id.designPrefRadio3,
		R.id.designPrefRadio4,
		R.id.designPrefRadio5,
		R.id.designPrefRadio6
	};


	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.design_preferences);

		setUpRadio( R.id.designPrefsTyperatingGroup, Settings.DESIGN_PREFERENCE_TYPE, PREF_TYPE );

		setUpRadio( R.id.designPrefsPrefRatingGroup, Settings.DESIGN_PREFERENCE_PREF, PREF_PREF );
		
		TextView title = (TextView)findViewById(R.id.headerBackNextTitle);
		title.setText(R.string.sectionTitleDesignPreferences);

	}

	public void setSelectedDesignType( View v)
	{
		RadioButton button = (RadioButton)v;

		int index = getButtonIndex(PREF_TYPE, button.getId() );

		this.getSettingsDelegate().setInt(Settings.DESIGN_PREFERENCE_TYPE, index);

		this.isValid();
	}

	public void setSelectedDesignPref( View v)
	{
		RadioButton button = (RadioButton)v;

		int index = getButtonIndex(PREF_PREF, button.getId() );

		this.getSettingsDelegate().setInt(Settings.DESIGN_PREFERENCE_PREF, index);

		this.isValid();
	}


	public void showNext( View v )
	{
		this.finish();
		this.startActivity( new Intent( this, RoleDetailsActivity.class ));
	}

	public void showSettings( View v )
	{
		this.finish();
	}
	
	public void displayHelp( View v )
    {
    	AlertHelper.createAlertDialog(this, R.string.designPrefsHelpTitle, R.string.designPrefsHelpTxt, 
    			R.string.helpDialogCloseButton);
    }


	private void isValid()
	{
		boolean valid = this.getSettingsDelegate().isIntegerSettingsValid( 
				Settings.MANDATORY_DESIGN_PREFERENCE_VALUES, true );

		this.getSettingsDelegate().setState(Settings.DESIGN_PREFERENCE_VALID, valid);
	}


	private int getButtonIndex( int[] buttonList, int id )
	{
		for( int i = 0; i < buttonList.length; ++i)
		{
			if (buttonList[i] == id )
				return i;
		}
		return -1;
	}


	private void setUpRadio( int groupId, String setting, int[] buttonList )
	{
		RadioGroup group = (RadioGroup)this.findViewById( groupId );

		if ( this.getSettingsDelegate().getInt( setting ) == -1 )
		{
			group.clearCheck();
			return;
		}

		int prefType = this.getSettingsDelegate().getInt( setting );
		int buttonId = 0;
		for( int i = 0; i < buttonList.length; ++i)
		{
			if ( prefType == i )
			{
				buttonId = buttonList[i];
				break;
			}
		}

		group.check( buttonId );
	}

}
