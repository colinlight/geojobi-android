package com.geojobi.activity;



import com.geojobi.R;
import com.geojobi.adapter.ArchiveJobListAdapter;
import com.geojobi.base.BaseActivity;
import com.geojobi.helper.AlertHelper;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class JobArchiveActivity extends BaseActivity {
	
	private ListView mJobList;

	private Activity mJobActivity; 
	
	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		this.setContentView(R.layout.job_archive);
		this.mJobActivity = this;
	}
	
	@Override 
	public void onResume()
	{
		
		super.onResume();

		mJobList = (ListView)findViewById(R.id.JobList);

		
		Cursor cursor = this.getJobDataDelegate().getCursorForArchivedJobs();
		this.startManagingCursor(cursor);
		
		ArchiveJobListAdapter adapter = new ArchiveJobListAdapter(this, cursor);
		mJobList.setAdapter(adapter);
		this.stopManagingCursor(cursor);

		mJobList.setDividerHeight(1);

		mJobList.setOnItemClickListener( new AdapterView.OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> adapater, View arg1, int position,
					long arg3) {
				
//				JobListAdapter jobAdapater = (JobListAdapter)adapater.getAdapter();
//				Job job = jobAdapater.getItem(position);
//
//				Intent jobDetailIntent = new Intent(mJobActivity, JobDetailActivity.class);
//				jobDetailIntent.putExtra(VacancyColumns.GUID, job.getGUID() );
//
//				startActivity( jobDetailIntent );

			}} );
	}
	
	
	
	public void showJobsList( View v )
	{
		this.finish();
		this.startActivity(new Intent( this, JobListActivity.class));
	}
	
	public void displayHelp( View v )
    {
    	AlertHelper.createAlertDialog(this, R.string.helpDialogTitle, R.string.jobTabsArchiveHelpTxt, 
    			R.string.helpDialogCloseButton);
    }
	

}
