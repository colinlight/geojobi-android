package com.geojobi.activity;

import com.geojobi.R;
import com.geojobi.model.Settings;
import com.geojobi.activity.LoginActivity;
import com.geojobi.base.BaseActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

public class MainActivity extends BaseActivity {

	private static final String TAG = "MainActivity";

	private Handler mWaitHandler = new Handler();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		initialiseApp();
	}

	private void initialiseApp() {

		//TODO: remove only for testing
		//this.getJobDataDelegate().resetJobData();

		if (this.getSettingsDelegate().getString(Settings.CANDIDATE_GUID).length() == 0) {
			mWaitHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					showLogin();
				}
			}, 3000);
			return;
		}

		Intent homeIntent = new Intent( this, HomeActivity.class);
		homeIntent.putExtra(com.geojobi.activity.HomeActivity.IS_FIRST_RUN, false);
		this.startActivity( homeIntent );

		this.finish();
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.v(TAG, "Pause-->");
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.v(TAG, "Resume-->");
	}

	private void showLogin()
	{
		this.startActivity(new Intent(this, LoginActivity.class));
		this.finish();
	}
}