package com.geojobi.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.Contacts.People;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.geojobi.R;
import com.geojobi.adapter.JobListAdapter;
import com.geojobi.base.BaseActivity;
import com.geojobi.base.BaseListActivity;
import com.geojobi.helper.AlertHelper;
import com.geojobi.model.Job;
import com.geojobi.model.Setting;
import com.geojobi.model.db.VacancyColumns;

public class JobListActivity extends BaseActivity {

	private ListView mJobList;

	private Activity mJobActivity; 

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.job_list);
		this.mJobActivity = this;
	}


	@Override
	public void onResume()
	{
		super.onResume();

		mJobList = (ListView)findViewById(R.id.JobList);

		ArrayList<Job> jobList = this.getJobDataDelegate().getAllJobs();
		JobListAdapter adapter = new JobListAdapter(this, jobList);
		mJobList.setAdapter(adapter);

		mJobList.setDividerHeight(1);

		mJobList.setOnItemClickListener( new AdapterView.OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> adapater, View arg1, int position,
					long arg3) {

				JobListAdapter jobAdapater = (JobListAdapter)adapater.getAdapter();
				Job job = jobAdapater.getItem(position);

				Intent jobDetailIntent = new Intent(mJobActivity, JobDetailActivity.class);
				jobDetailIntent.putExtra(VacancyColumns.GUID, job.getGUID() );

				startActivity( jobDetailIntent );
				finish();

			}} );
	}
	
	@Override 
	public void onBackPressed() 
	{ 
	    finish();
	}
	
	public void showHome( View v )
	{
		this.finish();
	}
	
	public void showArchive( View v )
	{
		this.finish();
		this.startActivity(new Intent( this, JobArchiveActivity.class));
	}
	
	public void showJobMap( View v )
	{
		this.finish();
		this.startActivity(new Intent( this, JobMapActivity.class));
	}
	
	public void displayHelp( View v )
    {
    	AlertHelper.createAlertDialog(this, R.string.helpDialogTitle, R.string.jobTabsListHelpTxt, 
    			R.string.helpDialogCloseButton);
    }

}