package com.geojobi.activity;


import java.util.Date;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.geojobi.R;
import com.geojobi.base.BaseActivity;
import com.geojobi.base.IDataParser;
import com.geojobi.components.FirstRunDialog;
import com.geojobi.helper.Network;
import com.geojobi.model.IndustryFilter;
import com.geojobi.model.OptionSelections;
import com.geojobi.model.Settings;
import com.geojobi.model.db.AppDatabaseHelper;
import com.geojobi.model.db.DatabaseTableConstants;
import com.geojobi.model.db.OptionColumns;
import com.geojobi.model.parser.OptionsDataParser;
import com.geojobi.service.AlarmReceiver;
import com.geojobi.webservice.AsyncWebService;
import com.geojobi.webservice.IServiceCallDelegate;
import com.geojobi.webservice.ServiceCall;
import com.geojobi.webservice.WebServiceConstants;
import com.geojobi.webservice.WebserviceResultReceiver;

public class HomeActivity extends BaseActivity implements WebserviceResultReceiver.Reciever
{
	public static final String TAG = "HomeActivity";

	public static final String IS_FIRST_RUN = "isFirstRun";

	public static BaseActivity mHomeActivity = null;

	public WebserviceResultReceiver serviceReceiver;

	private static final int FIRST_RUN_DIALOG = 0;

	private static final int UPDATE_ALERT = 1;

	private static final int HOME_ALERT = 2;

	private static final int UPDATE_FAILED_ALERT = 3;

	private static final int INCOMPLETE_PROFILE_ALERT = 4;

	private Dialog mLoadingAlert;

	//@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		this.serviceReceiver = new WebserviceResultReceiver(new Handler());

		isOptionsDownloaded();
	}


	private void getContent()
	{
		if ( !this.getAppStateDelegate().getIsUpdating() )
		{
			this.getAppStateDelegate().setIsUpdating(true);

			this.getSettingsDelegate().setState(Settings.OPTIONS_COMPLETE, false);

			IDataParser parser = new OptionsDataParser( (AppDatabaseHelper)this.getOptionsDelegate() );

			IServiceCallDelegate serviceCall = new ServiceCall(this.serviceReceiver, 
					WebServiceConstants.GET_ALL_OPTIONS, AsyncWebService.GET,  
					WebServiceConstants.getHeaders(),  null, parser, null);

			this.getServiceDelegate().callAsyncServiceWithDelegate(serviceCall);
		}

		mLoadingAlert = createDialogue( UPDATE_ALERT);
		mLoadingAlert.show();
	}

	public void displayAbout(View v)
	{
		if ( !isOptionsDownloaded() )
			return;

		this.startActivity(new Intent(this, AboutActivity.class));
	}

	public void displaySettings(View v)
	{
		if ( !isOptionsDownloaded() )
			return;

		this.startActivity(new Intent(this, UserSelectionsActivity.class));
	}

	public void displayJobs(View v)
	{
		if ( !isOptionsDownloaded() )
			return;

		this.startActivity(new Intent(this, JobMapActivity.class));
	}

	public void showHelp( View v)
	{		
		this.startActivity(new Intent(this, VideoActivity.class));
	}

	public void showTerms( View v )
	{
		this.startActivity(new Intent(this,TermsActivity.class));
	}

	public void startLooking( View v)
	{
		if ( !isOptionsDownloaded() || this.getSettingsDelegate().getState(Settings.IS_USER_LOOKING) )
			return;

		if ( !this.getSettingsDelegate().isBooleanSettingsValid(Settings.VALID_SETTINGS_VALUES) )
		{
			Dialog inCompleteAlert = this.createDialogue(INCOMPLETE_PROFILE_ALERT);
			inCompleteAlert.show();
			return;
		}

		Dialog lookingAlert = this.createDialogue(HOME_ALERT);
		lookingAlert.show();
	}


	/**
	 * Check all options have been down loaded and exist in the database
	 * @return
	 */
	private boolean isOptionsDownloaded()
	{
		boolean state = this.getSettingsDelegate().getState(Settings.OPTIONS_COMPLETE);
		if ( state == false )
		{
			this.getContent();
		}
		return state;
	}


	private Dialog createDialogue( int id )
	{
		switch( id )
		{
		case FIRST_RUN_DIALOG:

			final FirstRunDialog dialog = new FirstRunDialog( this );
			return dialog;

		case UPDATE_ALERT:

			ProgressDialog progressDialog = new ProgressDialog(HomeActivity.this);
			progressDialog.setTitle(R.string.updateAlertTitleTxt);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			return progressDialog;

		case HOME_ALERT:

			return new AlertDialog.Builder(HomeActivity.this)
			.setTitle(R.string.homeAlertTitleTxt)
			.setMessage( R.string.homeAlertInfoTxt )
			.setPositiveButton(R.string.homeAlertConfirmButtonTxt, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					startJobAlarm();
				}
			})
			.setNeutralButton(R.string.homeAlertChangeButtonTxt, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					startActivity(new Intent(HomeActivity.this, UserSelectionsActivity.class));
				}
			})
//			.setNegativeButton(R.string.homeAlertCancelButtonTxt, new DialogInterface.OnClickListener() {
//				public void onClick(DialogInterface dialog, int whichButton) {}
//			})
			.create();
		case UPDATE_FAILED_ALERT:

			return new AlertDialog.Builder(HomeActivity.this)
			.setTitle(R.string.updateFailAlertTitleTxt)
			.setMessage(R.string.updateFailAlertTxt)
			.setNeutralButton(R.string.updateFailAlertBtnTxt, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {

				}
			})
			.create();
		case INCOMPLETE_PROFILE_ALERT:
			return new AlertDialog.Builder(HomeActivity.this)
			.setTitle(R.string.profileIncompleteAlertTitle)
			.setMessage(R.string.profileIncompleteAlertPrompt)
			.setNeutralButton(R.string.profileIncompleteAlertViewBtn, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					startActivity(new Intent(HomeActivity.this, UserSelectionsActivity.class));
					dialog.cancel();
				}
			})
			.setNegativeButton(R.string.profileIncompleteAlertCancelBtn, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			})
			.create();
		}

		return null;

	}

	@Override
	public void onPause()
	{
		super.onPause();
		mHomeActivity = null;

		if ( this.serviceReceiver != null )
			serviceReceiver.setReceiver(null);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		mHomeActivity = this;
		this.serviceReceiver.setReceiver(this);
		this.setLookingButtonState();
	}

	public void onReceiveResult( int resultCode, Bundle resultData)
	{
		switch( resultCode )
		{
		case WebServiceConstants.STATUS_RUNNING:
			Log.v(TAG, "running service");
			break;
		case WebServiceConstants.STATUS_FINISHED:
			Log.v(TAG, "date service complete -->");
			mLoadingAlert.cancel();
			Dialog firstRunDialog = this.createDialogue(FIRST_RUN_DIALOG);
			firstRunDialog.show();
			this.getSettingsDelegate().setState(Settings.OPTIONS_COMPLETE, true);
			this.getAppStateDelegate().setIsUpdating(false);
			break;
		case WebServiceConstants.STATUS_FAILED:
			Log.v(TAG, "service failed");
			mLoadingAlert.cancel();
			Dialog failAlert = this.createDialogue(UPDATE_FAILED_ALERT);
			failAlert.show();
			break;
		}
	}

	private void startJobAlarm()
	{
		if ( this.getSettingsDelegate().getState(Settings.IS_USER_LOOKING) || 
				!Network.checkNetWorkStatus(this, R.string.alertTitle, 
						this.getResources().getString(R.string.alertNoInternet) ) )
			return;

		this.getSettingsDelegate().setState(Settings.IS_USER_LOOKING, true);
		this.getSettingsDelegate().setLong(Settings.ALARM_START_DATE, new Date().getTime());

		Context context = getApplicationContext();

		AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		PendingIntent pendIntent = 
				PendingIntent.getBroadcast(context, 9999, new Intent( context, AlarmReceiver.class ), 0);

		alarm.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 
				AlarmReceiver.ALARM_TRIGGER_TIME, AlarmReceiver.ALARM_INTERVAL, pendIntent);

		setLookingButtonState();
	}

	public void setLookingButtonState()
	{
		Button lookingButton = (Button)findViewById(R.id.lookingButton);

		Resources resources = getResources();

		if ( this.getSettingsDelegate().getState(Settings.IS_USER_LOOKING) )
		{
			lookingButton.setText(R.string.lookingActiveButtonTxt );

			lookingButton.setTextColor(R.color.white); 

			lookingButton.getBackground().setColorFilter( 
					resources.getColor(R.color.looking_active), PorterDuff.Mode.MULTIPLY);
			return;
		}
		lookingButton.setText(R.string.lookingButtonTxt );
		lookingButton.setTextColor(R.color.black);
		lookingButton.getBackground().setColorFilter( 
				resources.getColor(R.color.looking_grey), PorterDuff.Mode.MULTIPLY);

	}

}
