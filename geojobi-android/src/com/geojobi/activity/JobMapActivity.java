package com.geojobi.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.geojobi.R;
import com.geojobi.adapter.JobListAdapter;
import com.geojobi.base.IAppDependencies;
import com.geojobi.helper.AlertHelper;
import com.geojobi.helper.IVacancyOverlayDelegate;
import com.geojobi.helper.VacancyLocationsOverlay;
import com.geojobi.model.IndustryFilter;
import com.geojobi.model.Job;
import com.geojobi.model.db.OptionColumns;
import com.geojobi.model.db.VacancyColumns;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.OverlayItem;

public class JobMapActivity extends MapActivity implements IVacancyOverlayDelegate {

	private MapView map;
    private MapController controller;
    
	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.job_map);
		
        map = (MapView) findViewById(R.id.jobMap);
        controller = map.getController();
        
        
        Drawable pin = this.getResources().getDrawable(R.drawable.geojobi_icon);  
        ArrayList<OverlayItem> vacancyOverlays = getVancancyLocations();
        VacancyLocationsOverlay mJobOverlays = 
        		new VacancyLocationsOverlay( this, pin, this.getApplicationContext(), vacancyOverlays );
        map.getOverlays().add(mJobOverlays);
        
        
        //initialise current location
        final MyLocationOverlay overlay = new MyLocationOverlay(this, map);
        // not this does not work in the emulator
        overlay.enableMyLocation();
        overlay.runOnFirstFix(new Runnable() {
                public void run() {
                        // Zoom in to current location
                        controller.setZoom(8);
                        controller.animateTo(overlay.getMyLocation());
                }
        });
        map.getOverlays().add(overlay);
	}
	
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	private ArrayList<OverlayItem> getVancancyLocations()
	{
		ArrayList<OverlayItem> vancancies = new ArrayList<OverlayItem>();
		
		try 
		{			
			IAppDependencies mAppDepends = (IAppDependencies)this.getApplicationContext();
			ArrayList<Job> jobList = mAppDepends.getJobDataDelegate().getAllJobs();
			int totalNum = jobList.size();
			
			for( int i = 0; i < totalNum; i++ )
			{
				Job currentJob = jobList.get(i);
				OverlayItem overlay = new OverlayItem(currentJob.getGeoLocation(), "", 
						currentJob.getGUID());	
				
				vancancies.add(overlay);
			}
		}
		catch (Exception e) {}

		return vancancies;
	}
	
	public void showHome( View v )
	{
		this.finish();
	}
	
	@Override 
	public void onBackPressed() 
	{ 
	    finish();
	}
	
	public void showArchive( View v )
	{
		this.finish();
		this.startActivity(new Intent( this, JobArchiveActivity.class));
	}
	
	public void showJobList( View v )
	{
		this.finish();
		this.startActivity(new Intent( this, JobListActivity.class));
	}
	
	
	public void displayHelp( View v )
    {
    	AlertHelper.createAlertDialog(this, R.string.jobTabsMapHelpTitle, R.string.jobTabsMapHelpTxt, 
    			R.string.helpDialogCloseButton);
    }
	
	/**
	 * Implement IVacancyOverlayDelegate methods
	 */
	public void showJobVancancyForGuid( String guid )
	{
		Log.v("JobMapActivity", "show job details: " + guid);

		Intent jobDetailIntent = new Intent(this, JobDetailActivity.class);
		jobDetailIntent.putExtra(VacancyColumns.GUID, guid);
		
		this.startActivity( jobDetailIntent );
	}
	

}
