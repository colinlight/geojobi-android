package com.geojobi.activity;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.geojobi.R;
import com.geojobi.adapter.IindustryListAdapaterSelectionDelegate;
import com.geojobi.adapter.IndustryListAdapter;
import com.geojobi.base.BaseListActivity;
import com.geojobi.model.IndustryFilter;
import com.geojobi.model.Settings;
import com.geojobi.model.db.DatabaseTableConstants;

public class IndustryFilterActivity extends BaseListActivity implements
		IindustryListAdapaterSelectionDelegate {

	public static final String TAG = "IndustryFilterActivity";

	private IndustryFilter mSelectedFilter;

	private ListView mListView;

	private ArrayList<IndustryFilter> mSelectedFilters = new ArrayList<IndustryFilter>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		ArrayList<IndustryFilter> filters = this.getOptionsDelegate()
				.getIndustryFilters();
		
		this.setPreviousFilters(filters);

		IndustryListAdapter adapter = new IndustryListAdapter(this, filters,
				this);

		this.mListView = this.getListView();
		mListView.setDividerHeight(2);

		mListView.setItemsCanFocus(false);

		this.setListAdapter(adapter);
	}

	public void itemSelected(IndustryFilter filter) {
		mSelectedFilter = filter;

		if (mSelectedFilter.getSelected()) {
			mSelectedFilter.setYears("");
			mSelectedFilter.setSelected(false);
			mSelectedFilter.setYearsID(-1);
			mSelectedFilters.remove(filter);
			mListView.invalidateViews();
			saveSelections();
			return;
		}

		if (mSelectedFilters.size() >= 5) {
			Context context = getApplicationContext();
			Toast toast = Toast.makeText(context,
					R.string.industryDetailSkillsToastTxt, Toast.LENGTH_SHORT);
			toast.show();
			return;
		}

		Dialog yearsDialog = this.onCreateDialog();
		yearsDialog.show();
	}

	protected Dialog onCreateDialog() {

		final CharSequence[] years = this.getOptionsDelegate()
				.getNamesForOptionsOfType(
						DatabaseTableConstants.YEARSEXPERIENCE);
		
		String title = this.getString(R.string.industryFilterYearDialogTitle) + " " + mSelectedFilter.getIndustry();

		Dialog alert = new AlertDialog.Builder(IndustryFilterActivity.this)
				.setTitle( title )
				.setSingleChoiceItems(years, -1,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								CharSequence yr = years[which];
								mSelectedFilter.setYears(yr.toString());
								mSelectedFilter.setSelected(true);
								mSelectedFilter.setYearsID(which);
								mListView.invalidateViews();
								mSelectedFilters.add(mSelectedFilter);

								saveSelections();

								dialog.cancel();
							}
						}).create();

		return alert;
	}

	private void saveSelections() {
		
		SharedPreferences appPrefs = PreferenceManager
				.getDefaultSharedPreferences(this.getApplicationContext());

		SharedPreferences.Editor editor = appPrefs.edit();

		boolean isValid = false;
		String selections = "";
		for (int i = 0; i < 5; i++) {

			String industryName = Settings.INDUSTRY_EXPERIENCE + i + Settings.ID;
			String yearsIdName = Settings.INDUSTRY_EXPERIENCE_YEARS + i + Settings.ID;
			String yearsName = Settings.INDUSTRY_EXPERIENCE_YEARS + i;
			
			if  ( i < mSelectedFilters.size() )
			{
				IndustryFilter filter = mSelectedFilters.get(i);
				editor.putInt(industryName, filter.getIndustryId());
				editor.putInt(yearsIdName, filter.getYearsID());
				editor.putString(yearsName, filter.getYears());
				
				selections+= this.getSettingsDelegate().getListSettingFormat( 
								filter.getIndustry() + ", " 
								+ filter.getYears() + " " 
								+ this.getString(R.string.industryDetailExperienceTxt) 
								);
				isValid = true;
			}
			else
			{
				editor.putInt(industryName, -1);
				editor.putInt(yearsIdName, -1);
				editor.putString(yearsName, "");
			}
		}
		
		editor.putString(Settings.INDUSTRY_EXPERIENCE_ALL_SELECTIONS, selections);
		editor.putBoolean(Settings.INDUSTRY_EXPERIENCE_VALID, isValid);

		editor.commit();
	}

	private void setPreviousFilters(ArrayList<IndustryFilter> filters) {
	
		for (int i = 0; i < 5; i++) {
			
			int industryValue = 
					this.getSettingsDelegate().getInt( Settings.INDUSTRY_EXPERIENCE + i + Settings.ID );
			int yearsValue = 
					this.getSettingsDelegate().getInt( Settings.INDUSTRY_EXPERIENCE_YEARS + i + Settings.ID );
			
			String yearsDescription = this.getSettingsDelegate().getString( Settings.INDUSTRY_EXPERIENCE_YEARS + i );

			for (IndustryFilter filter : filters) {
				if (filter.getIndustryId() == industryValue) {
					filter.setSelected(true);
					filter.setYearsID( yearsValue );
					filter.setYears( yearsDescription);
					mSelectedFilters.add(filter);
					break;
				}
			}
		}

	}

}
