package com.geojobi.activity;

import android.os.Bundle;

import com.geojobi.R;
import com.geojobi.base.BaseActivity;

public class RejectActivity extends BaseActivity {
	
	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.reject_job);
	}

}
