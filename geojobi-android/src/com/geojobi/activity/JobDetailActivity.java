package com.geojobi.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.geojobi.R;
import com.geojobi.base.BaseActivity;
import com.geojobi.base.IDataParser;
import com.geojobi.helper.AlertHelper;
import com.geojobi.helper.DropDownListHelper;
import com.geojobi.helper.Network;
import com.geojobi.helper.ValidationHelper;
import com.geojobi.model.Job;
import com.geojobi.model.Settings;
import com.geojobi.model.db.AppDatabaseHelper;
import com.geojobi.model.db.DatabaseTableConstants;
import com.geojobi.model.db.OptionColumns;
import com.geojobi.model.db.VacancyColumns;
import com.geojobi.model.parser.JobDetailsDataParser;
import com.geojobi.model.parser.JobStatusDataParser;
import com.geojobi.webservice.AsyncWebService;
import com.geojobi.webservice.GetJobServiceCall;
import com.geojobi.webservice.IServiceCallDelegate;
import com.geojobi.webservice.SetJobStatusServiceCall;
import com.geojobi.webservice.WebServiceConstants;
import com.geojobi.webservice.WebserviceResultReceiver;

public class JobDetailActivity extends BaseActivity implements
WebserviceResultReceiver.Reciever {

	public static final String TAG = "JobDetailActivity";

	public static final String SHOULD_HIDE_BUTTONS = "shouldHideButtons";

	public WebserviceResultReceiver serviceReceiver;

	private static final int CONSIDER_DIALOG = 0;

	private static final int REJECT_DIALOG = 1;

	private static final int EMAIL_FRIEND_DIALOG = 2;

	private Dialog mLoadingAlert;

	private String mGUID;

	private String mJobDetails;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.job_details);

		Bundle bundle = this.getIntent().getExtras();
		mGUID = bundle.getString(VacancyColumns.GUID);

		if (bundle.getBoolean(SHOULD_HIDE_BUTTONS))
			hideButtons();

		this.serviceReceiver = new WebserviceResultReceiver(new Handler());

		Job job = this.getJobDataDelegate().getJobFromGUID(mGUID);

		if (job == null) {
			this.loadJobDetails(mGUID);
			return;
		}

		this.setJobDetailsText(job);

	}
	
	public void showJobList( View v )
	{
		this.startActivity(new Intent( this, JobListActivity.class ));
		this.finish();
	}
	
	public void showMap( View v )
	{
		this.startActivity(new Intent( this, JobMapActivity.class ));
		this.finish();
	}
	
	public void displayHelp( View v )
    {
    	AlertHelper.createAlertDialog(this, R.string.helpDialogTitle, R.string.jobDetailHelpTxt, 
    			R.string.helpDialogCloseButton);
    }

	private void hideButtons() {
		LinearLayout buttonContainer = (LinearLayout) this
				.findViewById(R.id.jobDetailButtonContainer);
		buttonContainer.setVisibility( View.GONE );
	}

	private void loadJobDetails(String guid) {
		
		if ( !Network.checkNetWorkStatus(this, R.string.alertNoInternet ) )
			return;
		
		IDataParser parser = new JobDetailsDataParser(
				(AppDatabaseHelper) this.getOptionsDelegate(), guid);

		IServiceCallDelegate delegate = new GetJobServiceCall(serviceReceiver,
				WebServiceConstants.GET_VACANCY + guid, AsyncWebService.POST, 
				WebServiceConstants.getHeadersForJson(), null,
				parser, null);
		delegate.setContext(this);
		delegate.setCandidateGUIDString( 
				this.getSettingsDelegate().getString(Settings.CANDIDATE_GUID ));

		this.getServiceDelegate().callAsyncServiceWithDelegate(delegate);

		mLoadingAlert = AlertHelper.createProgressAlert(this,
				R.string.jobDetailLoadingAlertTitle);
		mLoadingAlert.show();
	}

	public void onPause() {
		super.onPause();

		if (this.serviceReceiver != null)
			serviceReceiver.setReceiver(null);
	}

	public void onResume() {
		super.onResume();
		this.serviceReceiver.setReceiver(this);
	}

	public void rejectJob(View v) {
		
		if ( !Network.checkNetWorkStatus(this, R.string.alertNoInternet ) )
			return;
		
		this.createDialogue(REJECT_DIALOG).show();
	}

	public void setInterested(View v) {
		
		if ( !Network.checkNetWorkStatus(this, R.string.alertNoInternet ) )
			return;
		
		setJobStatus( DatabaseTableConstants.STATUS_INTERESTED );

		//Intent applyTabIntent = new Intent(this, ApplyTabActivity.class);
		//Intent applyIntent = new Intent(this, CoverLetterActivity.class)
		//applyIntent.putExtra(VacancyColumns.GUID, mGUID);
		
		Intent coverLetterIntent = new Intent(this, CoverLetterActivity.class);
		coverLetterIntent.putExtra(CoverLetterActivity.SHOULD_SHOW_APPLY_BUTTON, true);
		coverLetterIntent.putExtra(VacancyColumns.GUID, mGUID);
		
		this.startActivity(coverLetterIntent);
		this.finish();
	}

	public void setConsider(View v) {
		
		if ( !Network.checkNetWorkStatus(this, R.string.alertNoInternet ) )
			return;
		
		this.getJobDataDelegate().setJobStatus(this.mGUID, DatabaseTableConstants.STATUS_NONE);
		AlertDialog alert = this.createDialogue(CONSIDER_DIALOG);
		alert.show();
	}

	private AlertDialog createDialogue(int dialogId) {
		LayoutInflater factory = LayoutInflater.from(this);

		switch (dialogId) {
		
		case CONSIDER_DIALOG:
			return new AlertDialog.Builder(JobDetailActivity.this)
			.setTitle(R.string.jobDetailConsiderDialogTitle)
			.setMessage(R.string.jobDetailConsiderDialogTxt)
			.setPositiveButton(R.string.jobDetailConsiderBtnTxt,
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,
						int whichButton) {

				}
			})
			.setNeutralButton(R.string.jobDetailApplyBtnTxt,
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,
						int whichButton) {
					// startActivity(new
					// Intent(HomeActivity.this,
					// UserSelectionsActivity.class));
				}
			}).create();
			
		case REJECT_DIALOG:
			final View rejectReasonView = factory.inflate(
					R.layout.reject_job_dialog, null);

			ListView list = (ListView) rejectReasonView.findViewById(R.id.jobRejectListView);
			list.setChoiceMode(2);
			list.setDividerHeight(2);

			Cursor cursor = this.getOptionsDelegate().getCursorForOptionsOfType( DatabaseTableConstants.REJECTIONREASONS);
			startManagingCursor(cursor);

			SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, 
					android.R.layout.simple_list_item_multiple_choice, cursor,
					new String[] { OptionColumns.DESCRIPTION }, new int[] { android.R.id.text1 });

			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			list.setAdapter(adapter);

			return new AlertDialog.Builder(JobDetailActivity.this)
			.setTitle(R.string.jobDetailRejectDialogTitle)
			.setView(rejectReasonView)
			.setPositiveButton(R.string.jobDetailRejectDialogFinishBtn,
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.cancel();
					setJobStatus( DatabaseTableConstants.STATUS_REJECT );
					finish();
				}
			})
			.setNeutralButton(R.string.jobDetailRejectDialogSendBtn,
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.cancel();
					emailToFriend();
				}
			}).create();
			
		case EMAIL_FRIEND_DIALOG:
			final View emailFirendView = factory.inflate(
					R.layout.send_to_friend_dialog, null);
			return new AlertDialog.Builder(JobDetailActivity.this)
			.setIcon(android.R.drawable.ic_menu_send)
			.setTitle(R.string.jobDetailEmailDialogTitle)
			.setView(emailFirendView)
			.setPositiveButton(R.string.jobDetailEmailDialogSendBtn, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					
					AlertDialog alert = (AlertDialog) dialog;
					
					EditText nameEditText = (EditText) alert.findViewById(R.id.jobDetailEmailNameEditText);
					EditText emailEditText = (EditText) alert.findViewById(R.id.jobDetailEmailEditText);

					if (ValidationHelper.isEmailValid(emailEditText.getText().toString())
									&& ValidationHelper.isStringLengthValid(nameEditText.getText().toString())) {
						
						alert.cancel();
						Context content = alert.getContext();

						Intent emailIntent = new Intent(
								Intent.ACTION_SEND);
						emailIntent.setType("message/rfc822");
						emailIntent.putExtra( Intent.EXTRA_EMAIL, new String[] { emailEditText
										.getText()
										.toString() });
						
						emailIntent.putExtra( Intent.EXTRA_SUBJECT,
								content.getString(R.string.jobDetailEmailDialogSubject));

						String msg = content.getString(R.string.jobDetailEmailDialogContent1)
								+ nameEditText.getText().toString()
								+ content.getString(R.string.jobDetailEmailDialogContent2)
								+ "\n\n" + mJobDetails;

						emailIntent.putExtra(Intent.EXTRA_TEXT, msg);
						
						try {
							startActivity(Intent.createChooser( emailIntent, "Send mail.."));
						} catch (ActivityNotFoundException e) {
							Toast.makeText( JobDetailActivity.this, "", Toast.LENGTH_SHORT).show();
						}
						finish();
						return;
					}

					// display alert

				}
			})
			.setNegativeButton(R.string.jobDetailEmailDialogCancelBtn,
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,
						int whichButton) {
					finish();
				}
			}).create();

		}

		return null;
	}

	public void onReceiveResult(int resultCode, Bundle resultData) {
		switch (resultCode) {
		case WebServiceConstants.STATUS_RUNNING:
			Log.v(TAG, "running service");
			break;
		case WebServiceConstants.STATUS_FINISHED:
			Log.v(TAG, "date service complete -->");
			mLoadingAlert.cancel();
			Job job = this.getJobDataDelegate().getJobFromGUID(mGUID);
			this.setJobDetailsText(job);
			break;
		case WebServiceConstants.STATUS_FAILED:
			Log.v(TAG, "service failed");
			mLoadingAlert.cancel();
			break;
		}
	}

	private void setJobDetailsText(Job job) {
		TextView detailsTextView = (TextView) this
				.findViewById(R.id.jobDetailsTextView);
		mJobDetails = job.getJobDetails();
		detailsTextView.setText(mJobDetails);
	}

	private void emailToFriend() {
		this.createDialogue(EMAIL_FRIEND_DIALOG).show();
		setJobStatus( DatabaseTableConstants.STATUS_REJECT );
	}
	
	
	private void setJobStatus( int status )
	{

		IDataParser parser = new JobStatusDataParser(getJobDataDelegate(), mGUID, status);
		
		SetJobStatusServiceCall jobStatusCall = new SetJobStatusServiceCall(serviceReceiver,
				WebServiceConstants.RESPOND_TO_VACANCY + mGUID, AsyncWebService.POST, 
				WebServiceConstants.getHeadersForJson(), null,
				parser, null);
		
		jobStatusCall.setContext(this);
		jobStatusCall.setCandidateGUIDString( 
				this.getSettingsDelegate().getString(Settings.CANDIDATE_GUID ));
		jobStatusCall.setStatus(status);
		
		IServiceCallDelegate delegate = (IServiceCallDelegate)jobStatusCall;

		this.getServiceDelegate().callAsyncServiceWithDelegate(delegate);

	}

}
