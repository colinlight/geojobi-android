package com.geojobi.base;


public interface IDataParser  {
	
	public void parseData(String data);
	
	public void destroy();
	
	public boolean hasError();
	
	public String getError();
}
