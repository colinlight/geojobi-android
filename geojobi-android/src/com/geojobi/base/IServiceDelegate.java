package com.geojobi.base;

import com.geojobi.webservice.IServiceCallDelegate;
import com.geojobi.webservice.WebserviceResultReceiver;

public interface IServiceDelegate {
	
	public void callServiceForMethod(String method, WebserviceResultReceiver reciever );
	
	public void callAsyncServiceWithDelegate( IServiceCallDelegate delegate );
	
	public void getLatestJobs();
	
}
