package com.geojobi.base;

import java.util.ArrayList;

import com.geojobi.model.Job;
import android.database.Cursor;

public interface IJobDataDelegate {

	public void resetJobData();
	
	public ArrayList<Job> getAllJobs();
	
	public Cursor getCursorForAllJobs();
	
	public Job getJobFromGUID( String guid );
	
	public void setJobStatus( String jobGUID, int status );
	
	public Cursor getCursorForArchivedJobs();
}
