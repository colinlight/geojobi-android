package com.geojobi.base;


import android.database.sqlite.SQLiteDatabase;

public interface IAppDependencies 
{
	IServiceDelegate getServiceDelegate();
	ISettingsDelegate getSettingsDelegate();
	
	IUserOptionsDelegate getOptionsDelegate();
	SQLiteDatabase getWriteableDatabase();
	
	IJobDataDelegate getJobDataDelegate();
	
	IAppStateDelegate getAppStateDelegate();
}
