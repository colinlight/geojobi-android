package com.geojobi.base;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;

public class BaseActivity extends Activity implements IAppDependencies {

	

	@Override
	public ISettingsDelegate getSettingsDelegate() 
	{
		return this.getAppDependencies().getSettingsDelegate();
	}

	@Override
	public IUserOptionsDelegate getOptionsDelegate()
	{
		return this.getAppDependencies().getOptionsDelegate();
	}
	
	@Override
	public IJobDataDelegate getJobDataDelegate()
	{
		return this.getAppDependencies().getJobDataDelegate();
	}
	
	
	@Override
	public SQLiteDatabase getWriteableDatabase()
	{
		return this.getAppDependencies().getWriteableDatabase();
	}

	@Override
	public IServiceDelegate getServiceDelegate() 
	{
		return this.getAppDependencies().getServiceDelegate();
	}
	
	@Override
	public IAppStateDelegate getAppStateDelegate()
	{
		return this.getAppDependencies().getAppStateDelegate();
	}
	
	
	/**
	 * @private
	 * 
	 * returns the app context as IAppDependencies
	 * 
	 * @return
	 */
	private IAppDependencies getAppDependencies()
	{
		return ( IAppDependencies )this.getApplication();
	}

}
