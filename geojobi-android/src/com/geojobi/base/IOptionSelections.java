package com.geojobi.base;

/**
 * IOptionsSelections
 * 
 * @author colinlight
 * 
 * Implement this interface in a class to map database options to
 * user selections in a multichoice list
 */
public interface IOptionSelections {

	public void setNames( CharSequence[] names );
	public CharSequence[] getNames();
	
	public void setSelectedItems( boolean[] items );
	public boolean[] getSelectedItems();
	
	public String getSelectedIDs();
}
