package com.geojobi.base;


import java.util.ArrayList;

import com.geojobi.model.IndustryFilter;

import android.database.Cursor;

public interface IUserOptionsDelegate {
	
	public Cursor getCursorForOptionsOfType( String type );
	
	public CharSequence[] getNamesForOptionsOfType( String type );
	
	public ArrayList<IndustryFilter> getIndustryFilters();
	
	public IOptionSelections getOptionsOfType( String type, String selections );
	
	public String getOptionDescription( String optionType, int typeId );

}
