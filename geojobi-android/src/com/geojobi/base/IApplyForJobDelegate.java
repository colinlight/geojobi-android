package com.geojobi.base;

public interface IApplyForJobDelegate {
	
	public void applyForJob( String jobGuid, String responseOne, String responseTwo, 
			String responseThree, String responseFour );
}
