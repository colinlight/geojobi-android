package com.geojobi.base;

public interface IAppStateDelegate {
	
	boolean getIsUpdating();
	void setIsUpdating( boolean state );

}
