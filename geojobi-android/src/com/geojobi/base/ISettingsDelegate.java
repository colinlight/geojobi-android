package com.geojobi.base;

import com.google.android.maps.GeoPoint;

import android.content.Context;

public interface ISettingsDelegate 
{
	void setContext(Context context);

	String getString( String name);
	void setString( String name, String value );
	
	Boolean getState( String name );
	void setState( String name, Boolean value );
	
	int getInt( String name);
	void setInt(String name, int value);
	
	Long getLong( String name);
	void setLong(String name, Long value);
	
	public String parseSettingsListToString( boolean[] items );
	
	public String parseSettingsListToString( long[] items, String delimeter );
	
	public String parseLocationToString( GeoPoint point, int radius, int salary, int jobType, int permit, 
			int label, String labelName );
	
	public boolean isStringSettingsValid( String[] settings );
	
	public boolean isIntegerSettingsValid( String[] settings );
	
	public boolean isIntegerSettingsValid( String[] settings, boolean allowZero );
	
	public boolean isBooleanSettingsValid( String[] settings );
	
	public String getListSettingFormat( String value );
	
}
