package com.geojobi.model;

import com.geojobi.base.IAppStateDelegate;

public class AppStateDelegate implements IAppStateDelegate {

	
	private boolean mIsUpdating = false;
	@Override
	public boolean getIsUpdating() {
		return mIsUpdating;
	}
	@Override
	public void setIsUpdating(boolean state) {
		this.mIsUpdating = state;
	}

}
