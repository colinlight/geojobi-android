package com.geojobi.model;

public class Settings {
	
	
	public static final String CANDIDATE_GUID = "guid";
	
	public static final String OPTIONS_COMPLETE = "optionsComplete";
	
	public static final String SETTINGS_COMPLETE = "settingComplete";
	
	public static final String IS_USER_LOOKING = "isUserLooking";
	
	public static final String ALARM_START_DATE = "alarmStartDate";
	
	
	
	public static final String USER_DETAILS_VALID = "user_details_valid";
	
	public static final String USER_DETAILS_NAME = "name";
	
	public static final String USER_DETAILS_MOBILE = "telephonemobile";
	
	public static final String USER_DETAILS_EMAIL = "emailaddress";
	
	public static final String USER_DETAILS_WEBSITE = "website";
	
	public static final String USER_DETAILS_EDUCATION = "education";
	
	public static final String JOB_TYPE = "jobtypeid";
	
	public static final String MIN_SALARY = "minimumsalarypermanentid";
	
	public static final String MIN_SALARY_CONTRACT = "minimumsalarycontractid";
	
	public static final String NOTICE_PERIOD = "noticeperiodid";
	
	public static final String CAREER_EXPERIENCE = "careerexperienceyearsid";
	
	
	public static final String UNITS = "unitsid";
	
	public static final String [] MANDATORY_USER_DETAIL_STRINGS = { USER_DETAILS_NAME, USER_DETAILS_MOBILE, 
		USER_DETAILS_EMAIL };
	
	public static final String [] MANDATORY_USER_DETAIL_VALUES = { JOB_TYPE, MIN_SALARY };
	
	

	public static final String FILTER_BY_INDUSTRY = "filterByIndustry";
	
	
	public static final String INDUSTRY_EXPERIENCE_ALL_SELECTIONS = "industryExperienceAllAelections";
	
	public static final String INDUSTRY_EXPERIENCE_VALID = "industry_experience_valid";
	
	public static final String INDUSTRY_EXPERIENCE = "industryexperience";
	
	public static final String INDUSTRY_EXPERIENCE_YEARS = "industryexperienceyearsexperience"; 
	
	public static final String ID = "id";
	
	
	public static final String SOLIDWORKS_SKILLS = "solidworksskills";
	
	public static final String SOLIDWORKS_SKILLS_ALL_SELECTIONS = "solidworksSkillsAllSelections";
	
	public static final String SOLIDWORKS_SKILLS_VALID = "solid_worksskills_valid";
	
	public static final String SOLID_EXPERIENCE = "solidworksexperienceyearsid";
	
	public static final String OWN_SOLID_LICENSE = "solidworkscandidatelicenceid";
	
	
	public static final String [] MANDATORY_INDUSTRY_DETAILS_VALUES = { INDUSTRY_EXPERIENCE_VALID };
	
	public static final String INDUSTRY_DETAILS_VALID = "industry_details_valid";
	
	
	
	
	
	public static final String DESIGN_PREFERENCE_VALID = "design_preference_valid";
	
	public static final String DESIGN_PREFERENCE_TYPE = "designPreferenceType";
	
	public static final String DESIGN_PREFERENCE_PREF = "designPreferencePref";
	
	public static final String [] MANDATORY_DESIGN_PREFERENCE_VALUES = { DESIGN_PREFERENCE_TYPE, 
		DESIGN_PREFERENCE_PREF };
	
	
	
	public static final String ROLE_DETAILS_VALID = "role_details_valid";
	
	public static final String ROLE_DETAILS_ENJOY = "enjoyalter";
	
	public static final String ROLE_DETAILS_ALL_ENJOY_SELECTIONS = "roleDetailsAllEnjoySelections";
	
	public static final String ROLE_DETAILS_IMPROVE = "roleDetailsImprove";
	
	public static final String ROLE_DETAILS_ALL_IMPROVE_SELECTIONS = "roleDetailsAllImproveSelections";
	
	public static final String [] MANDATORY_ROLE_DETAIL_VALUES = { ROLE_DETAILS_ENJOY, ROLE_DETAILS_IMPROVE };
	
	

	public static final String COVER_LETTER_1 = "cover_letter_1";
	
	public static final String COVER_LETTER_2 = "cover_letter_2";
	
	public static final String COVER_LETTER_3 = "cover_letter_3";
	
	public static final String COVER_LETTER_4 = "cover_letter_4";
	
	public static final String COVER_LETTER_5 = "cover_letter_5";
	
	public static final String COVER_LETTER_6 = "cover_letter_6";
	
	public static final String COVER_LETTER_SELECTIONS = "cover_letter_selections";
	

	
	
	public static final String USER_LOCATION_LAT_LONG = "locationtowork";
	
	public static final String USER_LOCATION_DETAILS = "user_location_details";
	
	public static final String USER_LOCATIONS_VALID = "user_locations_valid";
	
	
	
	
	public static final String [] VALID_SETTINGS_VALUES = { USER_DETAILS_VALID, INDUSTRY_DETAILS_VALID, 
		DESIGN_PREFERENCE_VALID, ROLE_DETAILS_VALID, USER_LOCATIONS_VALID };
	

}
