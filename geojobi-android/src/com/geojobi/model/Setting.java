package com.geojobi.model;


public class Setting {

	public int icon;
	public String title;
	public boolean isComplete;
	public boolean shouldValidate;
	public Class<?> activity;
	
	public Setting()
	{
		super();
	}
	
	public Setting( int icon, String title, boolean isComplete, boolean shouldValidate, Class<?> activity )
	{
		super();
		this.icon = icon;
		this.title = title;
		this.isComplete = isComplete;
		this.shouldValidate = shouldValidate;
		this.activity = activity;
	}
}
