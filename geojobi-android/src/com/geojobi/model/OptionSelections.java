package com.geojobi.model;

import com.geojobi.base.IOptionSelections;

public class OptionSelections implements IOptionSelections {

	private Integer[] mIDs;
	
	public OptionSelections( CharSequence[] names, boolean[] selectedItems, Integer[] ids )
	{
		this.mNames = names;
		this.mItems = selectedItems;
		this.mIDs = ids;
	}
	
	private CharSequence[] mNames;
	@Override
	public void setNames(CharSequence[] names) 
	{
		this.mNames = names;
	}

	@Override
	public CharSequence[] getNames() {
		return this.mNames;
	}

	
	private boolean[] mItems;
	@Override
	public void setSelectedItems(boolean[] selectedItems) 
	{
		this.mItems = selectedItems;
	}

	@Override
	public boolean[] getSelectedItems() {
		return this.mItems;
	}
	
	
	@Override
	public String getSelectedIDs()
	{
		StringBuffer selectedItems = new StringBuffer();
		int totalItems = this.mItems.length;
		for( int i=0; i<totalItems; i++)
		{
			if ( mItems[i] )
			{
				selectedItems.append( mIDs[i] );
				selectedItems.append(",");				
			}
		}
		return selectedItems.toString();
	}

}
