package com.geojobi.model;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.widget.Toast;

import com.geojobi.R;
import com.geojobi.helper.LocationHelper;
import com.geojobi.model.db.VacancyColumns;
import com.google.android.maps.GeoPoint;

public class Job {

	private Context mContext;
	private String mDisplayTitle;
	private String mDisplayType;
	private String mCurrency;
	private String mDisplaySalary;
	private String mDisplayIndustry;
	private String mDisplayLocation;
	private String mDisplaySolidWorksPercent;

	public Job( Context context )
	{
		this.init(context);
	}


	public Job( Context context, String guid, String description )
	{
		this.mGUID = guid;
		this.mDescription = description;
		this.init(context);
	}

	private void init( Context context )
	{
		mContext = context;
		mDisplayTitle = context.getString(R.string.jobDetailDisplayTitle);
		mDisplayType = context.getString(R.string.jobDetailDisplayType);
		mCurrency = context.getString(R.string.globalCurrency);
		mDisplaySalary = context.getString(R.string.jobDetailDisplaySalary);
		mDisplayIndustry = context.getString(R.string.jobDetailDisplayIndustry);
		mDisplayLocation = context.getString(R.string.jobDetailDisplayLocation);
		mDisplaySolidWorksPercent = context.getString(R.string.jobDetailDisplaySolidWorksPercent);
	}


	private String mJobLocationDescription;

	public String getJobDetails()
	{
		if ( mJobLocationDescription== null )
			getLocationDescription();

		return  mDisplayTitle + mTitle + "\n\n" 
		+ mDisplayType + mType + "\n\n"
		+ mDisplaySalary + mSalaryPerm + "\n\n"
		+ mDisplayIndustry + mIndustry + "\n\n"
		+ mDisplayLocation + mJobLocationDescription + "\n\n"
		+ mDisplaySolidWorksPercent + mPercentSolidWorks + "\n\n"
		+ mDescription;
	}

	private void getLocationDescription()
	{
		Geocoder geoCoder = new Geocoder( mContext );
		try {
			List<Address> locations = geoCoder.getFromLocation( 
					this.getGeoLocation().getLatitudeE6()/1E6, 
					this.getGeoLocation().getLongitudeE6()/1E6, 1);

			if ( locations.size() <= 0 )
				return;

			mJobLocationDescription = LocationHelper.getLocationString(locations.get(0));

		} 
		catch (IOException e) 
		{ 
			e.printStackTrace(); 
		}
	}



	private String mGUID = "";

	public void setGUID( String value )
	{
		this.mGUID = value;
	}
	public String getGUID()
	{
		return this.mGUID;
	}



	private String mTitle = "";

	public void setTitle( String value )
	{
		this.mTitle = value;
	}
	public String getTitle()
	{
		return this.mTitle;
	}



	private String mType = "";

	public void setType( String value )
	{
		this.mType = value;
	}
	public String getType()
	{
		return this.mType;
	}



	private String mSalaryPerm = "";

	public void setSalaryPerm( String value )
	{
		this.mSalaryPerm = value;
	}
	public String getSalaryPerm()
	{
		return mCurrency + this.mSalaryPerm;
	}



	private String mDescription;

	public void setDescription( String value )
	{
		this.mDescription = value;
	}
	public String getDescription()
	{
		return this.mDescription;
	}



	private String mIndustry;

	public void setIndustry( String value )
	{
		this.mIndustry = value;
	}
	public String getIndustry()
	{
		return this.mIndustry;
	}


	private String mLocation;

	public void setLocation( String value )
	{
		this.mLocation = value;
		Log.v("JOB", "setLoc: " + value + this.mGUID );
	}
	public String getLocation()
	{
		return this.mLocation;
	}



	private String mPercentSolidWorks;

	public void setPercentSolidWorks( String value )
	{
		this.mPercentSolidWorks = value;
	}
	public String getPercentSolidWorks()
	{
		return this.mPercentSolidWorks;
	}



	private String mDeadline;

	public void setDealine( String value )
	{
		this.mDeadline = value;
	}
	public String getDealine()
	{
		return this.mDeadline;
	}


	private GeoPoint mGeoLocation;

	public GeoPoint getGeoLocation()
	{
		if ( mLocation == null )
			return null;

		if ( mGeoLocation != null )
			return mGeoLocation;

		String[] latLongRadius = mLocation.split(",");

		float lat = Float.parseFloat( latLongRadius[0] );
		float lon = Float.parseFloat( latLongRadius[1] ); 
		mGeoLocation =  new GeoPoint( (int )(lat * 1E6),   (int )(lon * 1E6));

		return mGeoLocation;
	}

}
