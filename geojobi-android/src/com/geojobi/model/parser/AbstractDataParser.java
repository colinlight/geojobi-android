package com.geojobi.model.parser;

import com.geojobi.base.IDataParser;

public class AbstractDataParser implements IDataParser {

	public static final String ERROR = "error";
	
	protected boolean mHasError = false;
	
	protected String mErrorMsg = "";
	
	@Override
	public void parseData(String data) {
		
	}

	@Override
	public void destroy() {
	
	}

	@Override
	public boolean hasError() {
		
		return this.mHasError;
	}

	@Override
	public String getError() {
		return this.mErrorMsg;
	}

}
