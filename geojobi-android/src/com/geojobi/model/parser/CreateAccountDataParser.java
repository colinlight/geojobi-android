package com.geojobi.model.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.geojobi.base.IDataParser;
import com.geojobi.base.ISettingsDelegate;


public class CreateAccountDataParser extends AbstractDataParser{

	public static final String TAG = "CreateAccountDataParser";
	
	private ISettingsDelegate mSettingsDelegate;
	
	public CreateAccountDataParser( ISettingsDelegate settingDelegate)
	{
		this.mSettingsDelegate = settingDelegate;
	}
	
	@Override
	public void parseData(String data) {
		
		try
		{
			JSONObject jsonData = new JSONObject(data);
			JSONArray list = jsonData.getJSONArray("results");
			
			JSONObject item = list.getJSONObject(0);
			JSONArray nameArray = item.names();
			String name = nameArray.getString(0).toString();
			
			//{"results":[{"guid":"8a22959b-a58a-4b98-899b-7d4faf4dacbf"},
			//{"success":"C00B: Candidate record successfully created in database"}]}
			
			if ( name.equals(AbstractDataParser.ERROR) )
			{
				this.mHasError = true;
				this.mErrorMsg = item.getString(name);
			}
			
			
			
		}
		catch(JSONException e)
		{
			Log.v(TAG, e.getMessage());
		}
	}

	@Override
	public void destroy() {
		
	}

}
