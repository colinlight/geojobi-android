package com.geojobi.model.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.geojobi.model.db.AppDatabaseHelper;
import com.geojobi.model.db.OptionColumns;
import com.geojobi.model.db.DatabaseTableConstants;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class OptionsDataParser extends AbstractDataParser {
	
	public static final String TAG = "OptionsDataParser";
	
	private AppDatabaseHelper mDbHelper;
	
	public OptionsDataParser( AppDatabaseHelper dbHelper )
	{
		this.mDbHelper = dbHelper;
	}
	
	@Override
	public void parseData(String data) {
		
		try
		{
			JSONObject jsonData = new JSONObject(data);
			JSONArray list = jsonData.getJSONArray("results");
			
			this.mDbHelper.updateOptions();
			
			SQLiteDatabase db = mDbHelper.getWritableDatabase();
			db.beginTransaction();
			
			for( int i = 0; i < list.length(); i++ )
			{
				JSONObject item = list.getJSONObject(i);
				JSONArray nameArray = item.names();
				String name = nameArray.getString(0);
				
				JSONArray options = item.getJSONArray(name);
				
				Log.v(TAG, name + ": " + options );
				
				for( int j = 0; j < options.length(); j++ )
				{
					JSONObject option = options.getJSONObject(j);
					int id = option.getInt("id");
					String description = option.getString(OptionColumns.DESCRIPTION);
					
					ContentValues optionValues = new ContentValues();
					optionValues.put(OptionColumns.TYPE, name);
					optionValues.put(OptionColumns.TYPE_ID, id);
					optionValues.put(OptionColumns.DESCRIPTION, description);
					db.insert(DatabaseTableConstants.OPTIONS, null, optionValues);
				}
			}
			db.setTransactionSuccessful();
			db.endTransaction();
			db.close();
		}
		catch(JSONException e)
		{
			Log.v(TAG, e.getMessage());
		}
	}
	
	@Override
	public void destroy()
	{
		mDbHelper = null;
	}
		
	
	

}
