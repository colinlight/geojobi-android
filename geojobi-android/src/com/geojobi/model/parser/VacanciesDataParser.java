package com.geojobi.model.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.geojobi.R;
import com.geojobi.activity.HomeActivity;
import com.geojobi.activity.JobListActivity;
import com.geojobi.activity.JobsTabActivity;
import com.geojobi.base.ISettingsDelegate;
import com.geojobi.model.db.AppDatabaseHelper;
import com.geojobi.model.db.OptionColumns;
import com.geojobi.model.db.DatabaseTableConstants;
import com.geojobi.model.db.VacancyColumns;

public class VacanciesDataParser extends AbstractDataParser {
	
	public static final String TAG = "VacanciesDataParser";
	
	private AppDatabaseHelper mDbHelper;
	
	private Context mContext;
	
	public VacanciesDataParser( AppDatabaseHelper dbHelper, Context context )
	{
		this.mDbHelper = dbHelper;
		
		this.mContext = context;
	}
	
	@Override
	public void parseData(String data) {
		
		try
		{
			JSONObject jsonData = new JSONObject(data);
			JSONArray list = jsonData.getJSONArray("results");
	
			SQLiteDatabase db = mDbHelper.getWritableDatabase();
			
			for( int i = 0; i < list.length(); i++ )
			{
				JSONObject item = list.getJSONObject(i);
				
				this.addVacanyToDatabase(db, DatabaseTableConstants.VANCANCY, 
						item.getString(VacancyColumns.GUID), 
						item.getString(VacancyColumns.JOB_TITLE), 
						item.getString(VacancyColumns.MAP_POINT), 
						item.getString(VacancyColumns.JOB_TYPE_IDS), 
						item.getInt(VacancyColumns.MAXIMUMSALARYPERMANENTID),
						item.getInt(VacancyColumns.MAXIMUMSALARYCONTRACTID),
						item.getString(VacancyColumns.DEADLINEDATE),
						item.getInt( "candidatedecisionid" ),
						item.getString( "datecreated" ), 
						item.getString( "lastupdated" )
						);
			}
			
			db.close();
			
	
			String noteService = Context.NOTIFICATION_SERVICE;
			NotificationManager noteManager = (NotificationManager) mContext.getSystemService(noteService);
			Notification note = new Notification( R.drawable.geojobi_icon, "New Jobs", System.currentTimeMillis() );
			note.flags |= Notification.FLAG_AUTO_CANCEL;
			
			String noteTitle = mContext.getString(R.string.newJobNoteTitle);
			String noteContent = mContext.getString(R.string.newJobNoteContent);
			
			Intent noteIntent = new Intent( mContext, JobsTabActivity.class);
			PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, noteIntent, 0);
			note.setLatestEventInfo(mContext, noteTitle, noteContent, contentIntent);
			
			noteManager.notify(0, note);
			
			
		}
		catch(JSONException e)
		{
			Log.v(TAG, e.getMessage());
		}

	}
	
	@Override
	public void destroy()
	{
		mDbHelper = null;
	}
		
	private void addVacanyToDatabase(SQLiteDatabase db, String table, String guid, String title, 
			String point, String typeIds, int maxSalaryPerm, int maxSalaryContract, 
			String deadlineDate, int status, String created, String modified)
	{
		ContentValues vacancyValues = new ContentValues();
		vacancyValues.put(VacancyColumns.GUID, guid);
		vacancyValues.put(VacancyColumns.JOB_TITLE, title);
		vacancyValues.put(VacancyColumns.MAP_POINT, point);
		vacancyValues.put(VacancyColumns.JOB_TYPE_IDS, typeIds);		
		vacancyValues.put(VacancyColumns.MAXIMUMSALARYPERMANENTID, maxSalaryPerm );
		vacancyValues.put(VacancyColumns.MAXIMUMSALARYCONTRACTID, maxSalaryContract );
		vacancyValues.put(VacancyColumns.DEADLINEDATE, deadlineDate );
		vacancyValues.put(VacancyColumns.STATUS, status );
		vacancyValues.put(VacancyColumns.CREATED_DATE, created);
		vacancyValues.put(VacancyColumns.MODIFIED_DATE, modified);
		
		//TODO: need to know if we are inserting or updating the value
		db.insertOrThrow(table, null, vacancyValues);
	}

}
