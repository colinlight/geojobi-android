package com.geojobi.model.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.geojobi.base.IJobDataDelegate;


public class JobStatusDataParser extends AbstractDataParser {
	
	public static final String TAG = "JobStatusDataParser";
	
	private IJobDataDelegate mJobDataDelegate;
	
	private String mGuid;
	
	private int mStatus;
	
	public JobStatusDataParser( IJobDataDelegate jobDataDelegate, String guid, int status )
	{
		this.mJobDataDelegate = jobDataDelegate;
		this.mGuid = guid;
		this.mStatus = status;
	}
	
	@Override
	public void parseData(String data) {
		
		try
		{
			JSONObject jsonData = new JSONObject(data);
			JSONArray list = jsonData.getJSONArray("results");
			
			JSONObject item = list.getJSONObject(0);
			JSONArray nameArray = item.names();
			String name = nameArray.getString(0).toString();
			
			if ( name.equals(AbstractDataParser.ERROR) )
			{
				this.mHasError = true;
				this.mErrorMsg = item.getString(name);
				return;
			}
			
			mJobDataDelegate.setJobStatus(mGuid, mStatus);
		}
		catch(JSONException e)
		{
			Log.v(TAG, e.getMessage());
		}
	}
	
	@Override
	public void destroy()
	{
		mJobDataDelegate = null;
	}

}
