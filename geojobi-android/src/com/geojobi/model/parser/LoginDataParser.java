package com.geojobi.model.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.provider.ContactsContract.CommonDataKinds.Email;
import android.util.Log;

import com.geojobi.base.ISettingsDelegate;
import com.geojobi.model.Settings;

public class LoginDataParser extends AbstractDataParser {
	
public static final String TAG = "CreateAccountDataParser";
	
	private ISettingsDelegate mSettingsDelegate;
	
	private String mEmail;
	
	public LoginDataParser( ISettingsDelegate settingDelegate, String email )
	{
		this.mSettingsDelegate = settingDelegate;
		this.mEmail = email;
	}
	
	@Override
	public void parseData(String data) {
		
		try
		{
			JSONObject jsonData = new JSONObject(data);
			JSONArray list = jsonData.getJSONArray("results");
			
			JSONObject item = list.getJSONObject(0);
			JSONArray nameArray = item.names();
			String name = nameArray.getString(0).toString();
			
			if ( name.equals(AbstractDataParser.ERROR) )
			{
				this.mHasError = true;
				this.mErrorMsg = item.getString(name);
			}
			else
			{
				String guid = item.getString(name);
				mSettingsDelegate.setString(Settings.CANDIDATE_GUID, guid);
				mSettingsDelegate.setString(Settings.USER_DETAILS_EMAIL, this.mEmail);
			}
			
			
		}
		catch(JSONException e)
		{
			Log.v(TAG, e.getMessage());
		}
	}

	@Override
	public void destroy() {
		
	}

}
