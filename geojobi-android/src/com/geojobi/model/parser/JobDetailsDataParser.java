package com.geojobi.model.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.geojobi.model.db.AppDatabaseHelper;
import com.geojobi.model.db.DatabaseTableConstants;
import com.geojobi.model.db.VacancyColumns;

public class JobDetailsDataParser extends AbstractDataParser {
	
public static final String TAG = "JobDetailsDataParser";
	
	private AppDatabaseHelper mDbHelper;
	
	private String mGuid;
	
	public JobDetailsDataParser( AppDatabaseHelper dbHelper, String guid )
	{
		this.mDbHelper = dbHelper;
		this.mGuid = guid;
	}
	
	@Override
	public void parseData(String data) {
		
		try
		{
			JSONObject jsonData = new JSONObject(data);
			JSONArray list = jsonData.getJSONArray("results");
			JSONObject jobData = list.getJSONObject(0);
	
			SQLiteDatabase db = mDbHelper.getWritableDatabase();
			
			ContentValues vacancyValues = new ContentValues();
			vacancyValues.put( VacancyColumns.DEADLINEDATE,  
					jobData.getString( VacancyColumns.DEADLINEDATE ) );
			
			vacancyValues.put( VacancyColumns.JOBDESCRIPTION,  
					jobData.getString( VacancyColumns.JOBDESCRIPTION ) );
			
			vacancyValues.put( VacancyColumns.SOLID_WORKS_EXPERIENCE_ID,  
					jobData.getString( VacancyColumns.SOLID_WORKS_EXPERIENCE_ID ) );
			
			vacancyValues.put( VacancyColumns.CAREEREXPERIENCEIDS,  
					jobData.getString( VacancyColumns.CAREEREXPERIENCEIDS ) );
			
			vacancyValues.put( VacancyColumns.SKILLSIDS,  
					jobData.getString( VacancyColumns.SKILLSIDS ) );
			
			vacancyValues.put( VacancyColumns.INDUSTRYEXPERIENCEIDS,  
					jobData.getString( VacancyColumns.INDUSTRYEXPERIENCEIDS ) );
			
			vacancyValues.put( VacancyColumns.RATINGID,  
					jobData.getString( VacancyColumns.RATINGID ) );
			
			vacancyValues.put( VacancyColumns.PERCENTAGESOLIDWORKSID,  
					jobData.getString( VacancyColumns.PERCENTAGESOLIDWORKSID ) );
			
			vacancyValues.put( VacancyColumns.MAXIMUMSALARYCONTRACTID,  
					jobData.getString( VacancyColumns.MAXIMUMSALARYCONTRACTID ) );
			
			vacancyValues.put( VacancyColumns.MAXIMUMSALARYPERMANENTID,  
					jobData.getString( VacancyColumns.MAXIMUMSALARYPERMANENTID ) );
			
			//TODO: need to know if we are inserting or updating the value
			db.update(DatabaseTableConstants.VANCANCY, vacancyValues, "guid = '" + mGuid + "'" , null );
			
			db.close();
		}
		catch(JSONException e)
		{
			Log.v(TAG, e.getMessage());
		}

	}
	
	@Override
	public void destroy()
	{
		mDbHelper = null;
	}
	

}
