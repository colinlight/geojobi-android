package com.geojobi.model;

public class IndustryFilter {
	
	public IndustryFilter( String industry, int industryId, boolean selected, String years, int yearsID )
	{
		this.mIndustry = industry;
		this.mIndustryId = industryId;
		this.mSelected = selected;
		this.mYears = years;
		this.mYearsID = yearsID;
	}
	
	
	private String mIndustry;
	
	public void setIndustry( String industry )
	{
		this.mIndustry = industry;
	}
	public String getIndustry()
	{
		return this.mIndustry;
	}
	
	
	private int mIndustryId;
	
	public void setIndustryId( int industryId )
	{
		this.mIndustryId = industryId;
	}
	public int getIndustryId()
	{
		return this.mIndustryId;
	}
	
	
	
	private boolean mSelected;
	
	public void setSelected( boolean selected )
	{
		this.mSelected = selected;
	}
	public boolean getSelected()
	{
		return this.mSelected;
	}
	
	
	private String mYears;
	
	public void setYears( String years )
	{
		this.mYears = years;
	}
	public String getYears()
	{
		return this.mYears;
	}
	
	
	private int mYearsID;
	
	public void setYearsID( int yearsID )
	{
		this.mYearsID = yearsID;
	}
	public int getYearsID()
	{
		return this.mYearsID;
	}

}
