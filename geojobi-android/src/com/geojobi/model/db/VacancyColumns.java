package com.geojobi.model.db;

import android.provider.BaseColumns;

public class VacancyColumns implements BaseColumns {
	
	public static final String CREATED_DATE = "created";
    public static final String MODIFIED_DATE = "modified";
   
    public static final String GUID = "guid";
    public static final String STATUS = "status";
    public static final String JOB_TITLE = "jobtitle";
    public static final String MAP_POINT = "mappoint";
    public static final String JOB_TYPE_IDS = "jobtypeids";
   
    public static final String DEADLINEDATE = "deadlinedate";
    public static final String JOBDESCRIPTION = "jobdescription";
    public static final String SOLID_WORKS_EXPERIENCE_ID = "solidworksexperienceid";
    public static final String CAREEREXPERIENCEIDS = "careerexperienceids";
    public static final String SKILLSIDS = "skillsids";
    public static final String INDUSTRYEXPERIENCEIDS = "industryexperienceids";
    public static final String RATINGID = "ratingid";
    public static final String PERCENTAGESOLIDWORKSID = "percentagesolidworksid";
    public static final String MAXIMUMSALARYPERMANENTID = "maximumsalarypermanentid";
    public static final String MAXIMUMSALARYCONTRACTID = "maximumsalarycontractid"; 
}
