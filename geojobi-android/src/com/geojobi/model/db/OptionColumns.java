package com.geojobi.model.db;

import android.provider.BaseColumns;

public class OptionColumns implements BaseColumns {

	public static final String CREATED_DATE = "created";
    public static final String MODIFIED_DATE = "modified";
    
    public static final String TYPE = "type";
    public static final String TYPE_ID = "typeId";
    public static final String DESCRIPTION = "description";
}
