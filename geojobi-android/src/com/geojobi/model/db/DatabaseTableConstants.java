package com.geojobi.model.db;

public class DatabaseTableConstants {

	/**
	 * options table
	 */
	public static final String OPTIONS = "options";
	
	/**
	 * A list of options database fields names e.g. in the options table there are types 
	 * and a type can map to one of these field names
	 */
	public static final String CANDIDATE_DECISION = "candidatedecision";
	public static final String COUNTRIES = "countries";
	public static final String CURRENCY = "currency";
	public static final String DISTANCE = "distance";
	public static final String EDUCATION = "education";
	public static final String ENJOYALTERPOINTS = "enjoyalterpoints";
	public static final String INDUSTRY = "industry";
	public static final String JOBTYPE = "jobtype";
	public static final String LOCATIONLABELS = "locationlabels";
	public static final String MATCHPREFERENCES = "matchpreferences";
	public static final String NOTICEPERIOD = "noticeperiod";
	public static final String PERMIT = "permit";
	public static final String RATINGS = "ratings";
	public static final String REJECTIONREASONS = "rejectionreasons";
	public static final String SALARYCONTRACT = "salarycontract";
	public static final String SALARYPERMANENT = "salarypermanent";
	public static final String SOLIDWORKSCANDIDATELICENSE = "solidworkscandidatelicense";
	public static final String SOLIDWORKSCLIENTLICENSE = "solidworksclientlicense";
	public static final String SOLIDWORKSSKILLS = "solidworksskills";
	public static final String SOLIDWORKSUSAGE = "solidworksusage";
	public static final String UNITS = "units";
	public static final String YEARSEXPERIENCE = "yearsexperience";
	
	
	/**
	 * Default id values for options, these relate to the item position index
	 * rather than the option id provide by webserice
	 */
	public static final int NOTICEPERIOD_DEFAULT = 4;
	
	public static final int JOBTYPE_DEFAULT = 1;
	
	public static final int SALARYPERMANENT_DEFAULT = 40;
	
	
	/**
	 * vacancy table
	 */
	public static final String VANCANCY = "vancancy";
	
	/**
	 * vacancy user application status, e.g has the user applied for a position
	 */
	public static final int STATUS_NONE = 0;
	public static final int STATUS_INTERESTED = 1;
	public static final int STATUS_REJECT = 2;
	

}
