package com.geojobi.model.db;


import java.util.ArrayList;
import java.util.List;

import com.geojobi.base.IJobDataDelegate;
import com.geojobi.base.IOptionSelections;
import com.geojobi.base.IUserOptionsDelegate;
import com.geojobi.model.IndustryFilter;
import com.geojobi.model.Job;
import com.geojobi.model.OptionSelections;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AppDatabaseHelper extends SQLiteOpenHelper implements IUserOptionsDelegate, IJobDataDelegate{

	private static final String DATABASE_NAME = "geojobi.db";

	private static final int DATABASE_VERSION = 1;

	private static final String TAG = "AppDatabaseHelper";

	private Context mContext;

	private static final String OPTION_TABLE =
			"CREATE TABLE " + DatabaseTableConstants.OPTIONS + " ("
					+ OptionColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ OptionColumns.TYPE + " TEXT, "
					+ OptionColumns.TYPE_ID + " INTEGER, "
					+ OptionColumns.DESCRIPTION + " TEXT"
					+ ");";

	private static final String VANCANCY_TABLE =
			"CREATE TABLE " + DatabaseTableConstants.VANCANCY + " ("
					+ VacancyColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ VacancyColumns.GUID + " TEXT, "
					+ VacancyColumns.STATUS + " INTEGER, "
					+ VacancyColumns.JOB_TITLE + " TEXT, "
					+ VacancyColumns.JOBDESCRIPTION + " TEXT, "
					+ VacancyColumns.MAP_POINT + " TEXT, "
					+ VacancyColumns.JOB_TYPE_IDS + " TEXT, "
					+ VacancyColumns.DEADLINEDATE + " TEXT, "
					+ VacancyColumns.SOLID_WORKS_EXPERIENCE_ID + " TEXT, "
					+ VacancyColumns.CAREEREXPERIENCEIDS + " TEXT, "
					+ VacancyColumns.SKILLSIDS + " TEXT, "
					+ VacancyColumns.INDUSTRYEXPERIENCEIDS + " INTEGER, "
					+ VacancyColumns.RATINGID + " INTEGER, "
					+ VacancyColumns.PERCENTAGESOLIDWORKSID + " INTEGER, "
					+ VacancyColumns.MAXIMUMSALARYPERMANENTID + " INTEGER, "
					+ VacancyColumns.MAXIMUMSALARYCONTRACTID + " INTEGER, "
					+ VacancyColumns.CREATED_DATE + " TEXT, "
					+ VacancyColumns.MODIFIED_DATE + " TEXT"
					+ ");";


	public AppDatabaseHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION );

		this.mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		this.createOptionsTables(db);

		db.execSQL( VANCANCY_TABLE );

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		Log.w(AppDatabaseHelper.TAG, "Upgrading database from version " + oldVersion + " to "
				+ newVersion + ", which will destroy all old data");

		db.execSQL("DROP TABLE IF EXISTS " + DatabaseTableConstants.OPTIONS );

		db.execSQL("DROP TABLE IF EXISTS " + DatabaseTableConstants.VANCANCY );

		this.onCreate(db);
	}

	/**
	 * Update options table
	 */
	public void updateOptions()
	{
		SQLiteDatabase db = this.getWritableDatabase();

		db.execSQL("DROP TABLE IF EXISTS " + DatabaseTableConstants.OPTIONS );
		this.createOptionsTables(db);

		db.close();
	}

	private void createOptionsTables( SQLiteDatabase db )
	{
		db.execSQL( OPTION_TABLE );
	}



	/**
	 * Implementing Methods from IUserOptionDelegate interface 
	 */
	public Cursor getCursorForOptionsOfType( String type )
	{
		SQLiteDatabase db = this.getReadableDatabase();
		return db.query(DatabaseTableConstants.OPTIONS, QueryConstants.OPTION_COLUMNS, 
				"type = '" + type + "'" ,
				null, null, null, null);
	}


	public IOptionSelections getOptionsOfType( String type, String selections )
	{
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(DatabaseTableConstants.OPTIONS, QueryConstants.OPTION_COLUMNS, 
				"type = '" + type + "'" ,
				null, null, null, null);
		
		
		int totalItems = 0;
		String [] listOfSelections = null;
		if ( selections.length() > 0 )
		{
			listOfSelections = selections.split(",");
			totalItems = listOfSelections.length;			
		}
		
		boolean[] selected = new boolean[cursor.getCount()];
		Integer[] ids = new Integer[cursor.getCount()];
		List<String> selectionNames = new ArrayList<String>();
		
		cursor.moveToFirst();
		while( cursor.isAfterLast() == false)
		{
			int currentPosition = cursor.getPosition();
		
			int index = cursor.getColumnIndex(OptionColumns.DESCRIPTION);
			selectionNames.add( cursor.getString(index));
			
			int idIndex = cursor.getColumnIndex(OptionColumns.TYPE_ID);
			int optionID = cursor.getInt(idIndex);
			ids[ currentPosition ] = optionID;
			
			if ( listOfSelections != null )
			{
				boolean isSelected = false;
				for ( int i=0; i < totalItems; i++)
				{
					if ( optionID == Integer.parseInt(listOfSelections[i] ) )
					{
						isSelected = true;
						break;
					}
				}
				selected[ currentPosition ] = isSelected;
			}
			cursor.moveToNext();
		}
		cursor.close();
		db.close();
		
		IOptionSelections optionSelections = new OptionSelections( 
				selectionNames.toArray(new String[selectionNames.size()]), selected, ids);
		
		return optionSelections;
	}


	public CharSequence[] getNamesForOptionsOfType( String type )
	{
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(DatabaseTableConstants.OPTIONS, QueryConstants.OPTION_COLUMNS, 
				"type = '" + type + "'" ,
				null, null, null, null);


		List<String> selectionNames = new ArrayList<String>();
		cursor.moveToFirst();
		while( cursor.isAfterLast() == false)
		{
			int index = cursor.getColumnIndex(OptionColumns.DESCRIPTION);
			selectionNames.add( cursor.getString(index));
			cursor.moveToNext();
		}

		cursor.close();

		db.close();


		final CharSequence[] names = selectionNames.toArray(new String[selectionNames.size()]);

		return names;
	}

	public ArrayList<IndustryFilter> getIndustryFilters()
	{
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(DatabaseTableConstants.OPTIONS, QueryConstants.OPTION_COLUMNS, 
				"type = '" + DatabaseTableConstants.INDUSTRY + "'" ,
				null, null, null, null);

		ArrayList<IndustryFilter> filterList = new ArrayList<IndustryFilter>();

		cursor.moveToFirst();
		while( cursor.isAfterLast() == false)
		{
			int index = cursor.getColumnIndex(OptionColumns.DESCRIPTION);
			int idIndex = cursor.getColumnIndex(OptionColumns.TYPE_ID);

			filterList.add(new IndustryFilter(cursor.getString(index), 
					cursor.getInt(idIndex), false, "", -1 ));

			cursor.moveToNext();
		}

		cursor.close();
		db.close();

		return filterList;
	}

	/**
	 * Implement methods for IJobDataDelegate
	 */
	public void resetJobData()
	{
		try
		{
			SQLiteDatabase db = this.getWritableDatabase();
			db.execSQL("DROP TABLE IF EXISTS " + DatabaseTableConstants.VANCANCY );
			db.execSQL( VANCANCY_TABLE );
			db.close();
		}catch (Exception e) {
			Log.v(TAG, e.getMessage());
		}
	}


	public Cursor getCursorForAllJobs()
	{
		SQLiteDatabase db = this.getReadableDatabase();

		String query = "SELECT * FROM " + DatabaseTableConstants.VANCANCY 
				+ " where " + VacancyColumns.STATUS + "='" + DatabaseTableConstants.STATUS_NONE + "'";

		Cursor cursor = db.rawQuery(query, null);

		return cursor;
	}


	public ArrayList<Job> getAllJobs()
	{
		ArrayList<Job> newJobs = new ArrayList<Job>();
		Cursor cursor = this.getCursorForAllJobs();

		try
		{
			cursor.moveToFirst();
			while( cursor.isAfterLast() == false)
			{
				Job job = new Job( this.mContext );	
				job.setGUID( cursor.getString( cursor.getColumnIndex(VacancyColumns.GUID) ) );
				job.setTitle( cursor.getString( cursor.getColumnIndex(VacancyColumns.JOB_TITLE) ) );
				job.setLocation( cursor.getString( cursor.getColumnIndex(VacancyColumns.MAP_POINT)) );
				job.setSalaryPerm( 
						this.getOptionDescription(DatabaseTableConstants.SALARYPERMANENT, 
								cursor.getInt( cursor.getColumnIndex(VacancyColumns.MAXIMUMSALARYPERMANENTID) ) )
						);
				job.setDealine( cursor.getString( cursor.getColumnIndex(VacancyColumns.DEADLINEDATE)) );

				newJobs.add(job);
				cursor.moveToNext();
			}

		}
		catch (Exception e) {
			// TODO: handle exception
		}

		cursor.close();

		return newJobs;
	}


	public Job getJobFromGUID( String guid )
	{
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor =  db.query(DatabaseTableConstants.VANCANCY, QueryConstants.VANCANCY_COLUMNS_ALL, 
				VacancyColumns.GUID + " = '" + guid + "'" ,
				null, null, null, null);

		Job job = null;
		try
		{
			cursor.moveToFirst();
			int index = cursor.getColumnIndex( VacancyColumns.JOBDESCRIPTION );
			String description = cursor.getString(index);

			if ( description != null)
			{
				job = new Job( this.mContext, guid, description);	
				job.setTitle( cursor.getString( cursor.getColumnIndex(VacancyColumns.JOB_TITLE) ) );
				
				job.setLocation( cursor.getString( cursor.getColumnIndex(VacancyColumns.MAP_POINT)) );

				job.setType( 
						this.getOptionDescription(DatabaseTableConstants.JOBTYPE, 
								cursor.getInt( cursor.getColumnIndex(VacancyColumns.JOB_TYPE_IDS) ) )
						);

				job.setSalaryPerm( 
						this.getOptionDescription(DatabaseTableConstants.SALARYPERMANENT, 
								cursor.getInt( cursor.getColumnIndex(VacancyColumns.MAXIMUMSALARYPERMANENTID) ) )
						);

				job.setIndustry( 
						this.getOptionDescription(DatabaseTableConstants.INDUSTRY, 
								cursor.getInt( cursor.getColumnIndex(VacancyColumns.INDUSTRYEXPERIENCEIDS) ) )
						);

				job.setPercentSolidWorks( 
						this.getOptionDescription(DatabaseTableConstants.SOLIDWORKSUSAGE, 
								cursor.getInt( cursor.getColumnIndex(VacancyColumns.PERCENTAGESOLIDWORKSID) ) )
						);
			}
		}
		catch (Exception e) {
			job = null;
		}
		cursor.close();
		db.close();

		return job;
	}


	public void setJobStatus( String jobGUID, int status )
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues vacancyValues = new ContentValues();
		vacancyValues.put( VacancyColumns.STATUS, status );
		db.update(DatabaseTableConstants.VANCANCY, vacancyValues, "guid = '" + jobGUID + "'", null);
		db.close();
	}


	public Cursor getCursorForArchivedJobs()
	{
		SQLiteDatabase db = this.getReadableDatabase();

		String query = "SELECT * FROM " + DatabaseTableConstants.VANCANCY 
				+ " where " + VacancyColumns.STATUS + "!='" + DatabaseTableConstants.STATUS_NONE + "'";

		Cursor cursor = db.rawQuery(query, null);

		return cursor;
	}



	public String getOptionDescription( String optionType, int typeId )
	{
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor;
		String description = "";

		try
		{
			String query = "SELECT * FROM " + DatabaseTableConstants.OPTIONS + " where type='" + optionType + "' AND typeId= " + typeId ;
			cursor = db.rawQuery(query, null);

			cursor.moveToFirst();
			description = cursor.getString( cursor.getColumnIndex( OptionColumns.DESCRIPTION ) );
			cursor.close();
		}
		catch (Exception e) {}

		db.close();

		return description;
	}




}
