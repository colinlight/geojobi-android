package com.geojobi.model.db;

public class QueryConstants {

	public static final String[] OPTION_COLUMNS = 
	{
		OptionColumns._ID, OptionColumns.TYPE, OptionColumns.TYPE_ID, OptionColumns.DESCRIPTION
	};
	
	
	public static final String[] VANCANCY_COLUMNS = 
	{
		VacancyColumns._ID, VacancyColumns.GUID, VacancyColumns.JOB_TITLE, VacancyColumns.MAP_POINT, 
		VacancyColumns.JOB_TYPE_IDS, VacancyColumns.CREATED_DATE, VacancyColumns.MODIFIED_DATE
	};
	
	public static final String[] VANCANCY_COLUMNS_ALL = 
		{
			VacancyColumns._ID, VacancyColumns.GUID, VacancyColumns.JOB_TITLE, VacancyColumns.MAP_POINT, 
			VacancyColumns.JOB_TYPE_IDS, VacancyColumns.DEADLINEDATE, VacancyColumns.JOBDESCRIPTION,
			VacancyColumns.SOLID_WORKS_EXPERIENCE_ID, VacancyColumns.CAREEREXPERIENCEIDS, 
			VacancyColumns.SKILLSIDS, VacancyColumns.INDUSTRYEXPERIENCEIDS, VacancyColumns.RATINGID,
			VacancyColumns.PERCENTAGESOLIDWORKSID, VacancyColumns.MAXIMUMSALARYCONTRACTID, 
			VacancyColumns.MAXIMUMSALARYPERMANENTID, VacancyColumns.CREATED_DATE, 
			VacancyColumns.MODIFIED_DATE
		};	
	
}
