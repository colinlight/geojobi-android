package com.geojobi.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


import com.geojobi.base.ISettingsDelegate;
import com.google.android.maps.GeoPoint;

public class SettingsManager implements ISettingsDelegate {

	private Context _context;
	
	
	public SettingsManager( Context context)
	{
		this._context = context;
	}
	
	public void setContext(Context context)
	{
		this._context = context;
	}
	
	
	@Override
	public String getString(String name) 
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		return appPrefs.getString(name, "");
	}
	@Override
	public void setString(String name, String value) 
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		SharedPreferences.Editor editor = appPrefs.edit();
		editor.putString(name, value);
		editor.commit();
	}
	
	
	@Override
	public Boolean getState( String name )
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		return appPrefs.getBoolean(name, false);
	}
	@Override
	public void setState( String name, Boolean value )
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		SharedPreferences.Editor editor = appPrefs.edit();
		editor.putBoolean(name, value);
		editor.commit();
	}
	
	
	@Override
	public int getInt( String name) 
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		return appPrefs.getInt(name, -1 );
	}

	@Override
	public void setInt( String name, int value) 
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		SharedPreferences.Editor editor = appPrefs.edit();
		editor.putInt(name, value);
		editor.commit();
	}
	
	
	@Override
	public Long getLong( String name)
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		return appPrefs.getLong(name, 0);
	}
	@Override
	public void setLong(String name, Long value)
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		SharedPreferences.Editor editor = appPrefs.edit();
		editor.putLong(name, value);
		editor.commit();
	}
	
	
	@Override
	public String parseSettingsListToString( boolean[] items )
	{
		StringBuffer selectedItems = new StringBuffer();
		int totalItems = items.length;
		for( int i=0; i<totalItems; i++)
		{
			selectedItems.append( items[i] );
			selectedItems.append(",");
		}
		return selectedItems.toString();
	}
	
	
	@Override
	public String parseSettingsListToString( long[] items, String delimeter )
	{
		StringBuffer selectedItems = new StringBuffer();
		delimeter = ( delimeter == null )? "," : delimeter;
		int totalItems = items.length;
		for( int i=0; i<totalItems; i++)
		{
			selectedItems.append( items[i] );
			selectedItems.append( delimeter );
		}
		return selectedItems.toString();
	}
	
	
	
	
	@Override
	public String parseLocationToString( GeoPoint point, int radius, int salary, int jobType, 
			int permit, int label, String labelName )
	{
		StringBuffer selectedItems = new StringBuffer();
		selectedItems.append( point.getLatitudeE6() );
		selectedItems.append(",");
		selectedItems.append( point.getLongitudeE6() );
		selectedItems.append(",");
		selectedItems.append( Integer.toString( radius ) );
		selectedItems.append(",");
		selectedItems.append( Integer.toString(salary) );
		selectedItems.append(",");
		selectedItems.append( Integer.toString(jobType) );
		selectedItems.append(",");
		selectedItems.append( Integer.toString(permit) );
		selectedItems.append(",");
		selectedItems.append( Integer.toString(label) );
		selectedItems.append(",");
		selectedItems.append( labelName );
		return selectedItems.toString();
	}
	
	
	@Override
	public boolean isStringSettingsValid( String[] settings )
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		for( String setting : settings)
		{
			String value = appPrefs.getString(setting, "");
			if ( value.length() <= 0 )
				return false;
		}
		return true;
	}
	
	@Override
	public boolean isIntegerSettingsValid( String[] settings )
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		for( String setting : settings)
		{
			int value = appPrefs.getInt(setting, -1);
			if ( value <= 0 )
				return false;
		}
		return true;
	}
	
	@Override
	public boolean isIntegerSettingsValid( String[] settings, boolean allowZero )
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		for( String setting : settings)
		{
			int value = appPrefs.getInt(setting, -1);
			if ( value < 0 )
				return false;
		}
		return true;
	}
	
	
	@Override
	public boolean isBooleanSettingsValid( String[] settings )
	{
		SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(this._context);
		for( String setting : settings)
		{
			boolean value = appPrefs.getBoolean(setting, false);
			if ( !value )
				return false;
		}
		return true;
	}
	
	
	@Override
	public String getListSettingFormat( String value )
	{
		return " * " + value + "\n";
	}
	
	
	
	

}
