package com.geojobi.helper;

import java.util.ArrayList;


import com.geojobi.base.ISettingsDelegate;
import com.geojobi.model.Settings;
import com.google.android.maps.GeoPoint;

public class JobLocationsOverlayHelper {

	public static ArrayList<JobOverlayItem> getPreviousLocations( ISettingsDelegate settingsDelegate ) {

		ArrayList<JobOverlayItem> locations = new ArrayList<JobOverlayItem>();

		for (int i = 0; i < 5; i++) {

			String location = settingsDelegate.getString( Settings.USER_LOCATION_LAT_LONG + i);

			if (location.length() > 0) {
				String[] latLongRadius = location.split(",");
				int lat = Integer.parseInt(latLongRadius[0]);
				int lon = Integer.parseInt(latLongRadius[1]);
				int radius = Integer.parseInt(latLongRadius[2]);
				int salary = Integer.parseInt(latLongRadius[3]);
				int jobType = Integer.parseInt(latLongRadius[4]);
				int permit = Integer.parseInt(latLongRadius[5]);
				int label = Integer.parseInt(latLongRadius[6]);
				String labelName = (latLongRadius[7]);

				GeoPoint point = new GeoPoint(lat, lon);
				JobOverlayItem overlay = new JobOverlayItem(point, label, labelName, radius, salary, 
						jobType, permit, "", "");
				locations.add(overlay);
			}
		}
		return locations;
	}

}
