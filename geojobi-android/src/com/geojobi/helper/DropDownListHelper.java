package com.geojobi.helper;


import com.geojobi.model.IndustryFilter;
import com.geojobi.model.db.OptionColumns;

import android.content.Context;
import android.database.Cursor;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;


public class DropDownListHelper {
	
	/**
	 * Set the data model on a spinner
	 * 
	 * @param cursor
	 * @param spinner
	 * @param context
	 */
	public static void setDataModelForDropDownFromOptions( Cursor cursor, Spinner spinner, Context context )
	{
		
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(context, 
							android.R.layout.simple_spinner_item, 
							cursor, 
							new String[]{OptionColumns.DESCRIPTION},
							new int[]{android.R.id.text1});
		
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
	}
	
	
	/**
	 * 
	 * Sets the selected index for a spinner, this related to the id set in the database
	 * e.g.TypeID
	 * 
	 * @param cursor
	 * @param spinner
	 * @param context
	 * @param selectedID
	 */
	public static void setSelectedDropDownItem( Cursor cursor, Spinner spinner, Context context, int selectedID )
	{
		int selectedIndex = 0;
		
		cursor.moveToFirst();
		while( cursor.isAfterLast() == false)
		{
			int index = cursor.getColumnIndex(OptionColumns.TYPE_ID);
			int id = cursor.getInt(index);
			if ( id == selectedID )
			{
				selectedIndex = selectedID;
				break;
			}
			cursor.moveToNext();
		}
		
		spinner.setSelection(selectedIndex);
	}
	
	
	/**
	 * Sets the selected index for a spinner, this related to the id set in the database and if 
	 * none is selected it sets the default ID
	 * e.g.TypeID
	 * 
	 * @param cursor
	 * @param spinner
	 * @param context
	 * @param selectedID
	 * @param defaultSelectedIndex
	 */
	public static void setSelectedDropDownItem( Cursor cursor, Spinner spinner, Context context, 
			int selectedID, int defaultSelectedIndex )
	{
		int selectedIndex = defaultSelectedIndex;
		
		cursor.moveToFirst();
		while( cursor.isAfterLast() == false)
		{
			int index = cursor.getColumnIndex(OptionColumns.TYPE_ID);
			int id = cursor.getInt(index);
			if ( id == selectedID )
			{
				selectedIndex = selectedID;
				break;
			}
			cursor.moveToNext();
		}
		
		spinner.setSelection(selectedIndex);
	}
	
	/**
	 * Parses a string of selections into a boolean array
	 * @param selections
	 * @param selectedItems
	 */
	public static void setSelectionsForMultichoice( String selections, boolean[] selectedItems )
	{
		String [] listOfSelections = selections.split(",");
		int totalItems = listOfSelections.length;
		for ( int i=0; i < totalItems; i++)
		{
			selectedItems[i] = Boolean.parseBoolean( listOfSelections[i] );
		}
	}
	

}
