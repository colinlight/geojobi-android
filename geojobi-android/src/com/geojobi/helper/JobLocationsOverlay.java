package com.geojobi.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.location.Address;
import android.view.MotionEvent;
import android.widget.Toast;

import com.geojobi.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;


public class JobLocationsOverlay extends ItemizedOverlay<JobOverlayItem> {

	public static final String TAG = "JobLocationsOverlay";

	private ArrayList<JobOverlayItem> mJobOverlays;
	
	private IOverlayAlertDelegate mAlertDelegate;

	private Drawable mMarker;

	private Context mContext;

	private boolean mIsPinch = false;
	
	private Geocoder mGeoCoder;

	public JobLocationsOverlay( IOverlayAlertDelegate delegate, Drawable marker, 
			Context context, ArrayList<JobOverlayItem> overlays ) 
	{
		super( marker );
		this.mAlertDelegate = delegate;
		this.mMarker = marker;
		this.mContext = context;
		this.mJobOverlays = overlays;
		this.mGeoCoder = new Geocoder(context);

		this.populate();
	}

	public int addOverlay( JobOverlayItem overlay )
	{
		mJobOverlays.add(overlay);
		this.populate();
		return mJobOverlays.indexOf(overlay);
	}
	
	public void removeOverlay( JobOverlayItem overlay )
	{
		mJobOverlays.remove(overlay);
		this.populate();
		this.setLastFocusedIndex(-1);	
	}
	
	public int getIndex( JobOverlayItem overlay )
	{
		return mJobOverlays.indexOf(overlay);
	}
	

	@Override
	public int size()
	{
		return mJobOverlays.size();
	}

	@Override
	public void draw( Canvas canvas, MapView mapView, boolean shadow )
	{
		super.draw(canvas, mapView, shadow);
		boundCenterBottom( mMarker );
	}

	@Override
	public boolean onTouchEvent(MotionEvent e, MapView mapView )
	{
		int fingers = e.getPointerCount();
		if ( e.getAction() == MotionEvent.ACTION_DOWN )
		{
			this.mIsPinch = false;
		}
		if ( e.getAction() == MotionEvent.ACTION_MOVE && fingers == 2 )
		{
			this.mIsPinch = true;
		}

		return super.onTouchEvent(e, mapView);

	}


	@Override
	public boolean onTap( GeoPoint point, MapView mapView )
	{
		if ( super.onTap(point, mapView ) )
			return true;

		if ( mIsPinch )
			return false;

		if ( this.size() >= 5 )
		{
			Toast toast = Toast.makeText(mContext, 
					R.string.userLocationsMaxToastTxt, Toast.LENGTH_SHORT);
			
			toast.show();
			return true;
		}
		
		try {
			
			List<Address> addressList = mGeoCoder.getFromLocation( point.getLatitudeE6()/1E6, point.getLongitudeE6()/1E6, 1);
			
			if ( addressList.size() <= 0 )
			{
				Toast toastInValid = Toast.makeText(mContext, 
						R.string.userLocationsInvalidLocationToastTxt, Toast.LENGTH_SHORT);
				
				toastInValid.show();
				return true;
			}
			
			this.mAlertDelegate.displayDialog( point, addressList );

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return false;
	}


	@Override 
	protected boolean onTap( int index )
	{

		if (this.size() <= 0 )
			return true;
		
		try{
			
			JobOverlayItem overlay = this.mJobOverlays.get(index);
			GeoPoint point = overlay.getPoint();
			
			List<Address> addressList = mGeoCoder.getFromLocation( point.getLatitudeE6()/1E6, point.getLongitudeE6()/1E6, 1);
			
			
			this.mAlertDelegate.displayDialog(overlay, addressList);
			return true;
			
		}
		catch (IOException e) {
			e.printStackTrace();
			return true;
		}
	}

	@Override
	protected JobOverlayItem createItem(int i) {
		return mJobOverlays.get(i);
	}
	
	
}
