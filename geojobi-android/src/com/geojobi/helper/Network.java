package com.geojobi.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;

public class Network {
	
	public static boolean checkNetWorkStatus( Context context )
	{
		 ConnectivityManager conMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        
         if (conMgr.getActiveNetworkInfo() != null
                 && conMgr.getActiveNetworkInfo().isAvailable()
                 && conMgr.getActiveNetworkInfo().isConnected()) 
         {
        	 return true;
         }
         
         return false;
	}
	
	public static boolean checkNetWorkStatus( Context context, int toastStringID )
	{
		boolean isConnected = Network.checkNetWorkStatus(context);
     
		if ( !isConnected )
		{
			 Toast toast = Toast.makeText(context, toastStringID, Toast.LENGTH_LONG);
		     toast.show();	
		}
        return isConnected;
	}
	
	
	public static boolean checkNetWorkStatus( Context context, int alertTitle, String alertContent  )
	{
		boolean isConnected = Network.checkNetWorkStatus(context);
     
		if ( !isConnected )
		{
			 AlertHelper.createAlertDialog(context, alertTitle, alertContent);
		}
        return isConnected;
	}
	
	
	

}
