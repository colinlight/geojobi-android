package com.geojobi.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.view.MotionEvent;
import android.widget.Toast;

import com.geojobi.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public class VacancyLocationsOverlay extends ItemizedOverlay<OverlayItem> {
	
	public static final String TAG = "JobLocationsOverlay";

	private ArrayList<OverlayItem> mJobOverlays;
	
	private IVacancyOverlayDelegate mShowVacancyDelegate;

	private Drawable mMarker;

	private Context mContext;

	private boolean mIsPinch = false;
	
	private Geocoder mGeoCoder;

	public VacancyLocationsOverlay( IVacancyOverlayDelegate delegate, Drawable marker, 
			Context context, ArrayList<OverlayItem> overlays ) 
	{
		super( marker );
		this.mShowVacancyDelegate = delegate;
		this.mMarker = marker;
		this.mContext = context;
		this.mJobOverlays = overlays;
		this.mGeoCoder = new Geocoder(context);

		this.populate();
	}

	public int addOverlay( OverlayItem overlay )
	{
		mJobOverlays.add(overlay);
		this.populate();
		return mJobOverlays.indexOf(overlay);
	}
	
	public void removeOverlay( OverlayItem overlay )
	{
		mJobOverlays.remove(overlay);
		this.populate();
		this.setLastFocusedIndex(-1);	
	}
	
	public int getIndex( OverlayItem overlay )
	{
		return mJobOverlays.indexOf(overlay);
	}
	

	@Override
	public int size()
	{
		return mJobOverlays.size();
	}

	@Override
	public void draw( Canvas canvas, MapView mapView, boolean shadow )
	{
		super.draw(canvas, mapView, shadow);
		boundCenterBottom( mMarker );
	}

	@Override
	public boolean onTouchEvent(MotionEvent e, MapView mapView )
	{
		int fingers = e.getPointerCount();
		if ( e.getAction() == MotionEvent.ACTION_DOWN )
		{
			this.mIsPinch = false;
		}
		if ( e.getAction() == MotionEvent.ACTION_MOVE && fingers == 2 )
		{
			this.mIsPinch = true;
		}

		return super.onTouchEvent(e, mapView);

	}


	@Override
	public boolean onTap( GeoPoint point, MapView mapView )
	{
		if ( super.onTap(point, mapView ) )
			return true;

//		if ( mIsPinch )
//			return false;

		return false;
	}


//	@Override 
	protected boolean onTap( int index )
	{
		OverlayItem overlay = this.mJobOverlays.get(index);
		this.mShowVacancyDelegate.showJobVancancyForGuid(overlay.getSnippet());
		return true;
	}

	@Override
	protected OverlayItem createItem(int i) {
		return mJobOverlays.get(i);
	}

}
