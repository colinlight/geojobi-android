package com.geojobi.helper;

import com.geojobi.R;
import com.geojobi.activity.HomeActivity;
import com.geojobi.activity.LoginActivity;
import com.geojobi.activity.UserSelectionsActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.widget.TextView;


public class AlertHelper {

	/**
	 * Creates a progress alert with a title
	 * 
	 * @param context
	 * @param stringId
	 * @return
	 */
	public static Dialog createProgressAlert( Context context, int stringId )
	{
		ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setTitle(stringId);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		return progressDialog;
	}

	/**
	 * Creates and shows an alert with a message and an OK button
	 * @param context
	 * @param titleId
	 * @param msg
	 */
	public static void createAlertDialog( Context context, int titleId, String msg )
	{
		AlertDialog alert = new AlertDialog.Builder(context)
		.setTitle(titleId)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setMessage(msg)
		.setPositiveButton(R.string.alertDefaultDialogOk, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

			}
		})
		.create();

		alert.show();
	}


	/**
	 * Create and shows a dialog with a title, message and close button
	 * 
	 * @param context
	 * @param titleId
	 * @param msgID
	 * @param closeTxtId
	 */
	public static void createAlertDialog( Context context, int titleId, int msgID, int closeTxtId )
	{
		AlertDialog alert = new AlertDialog.Builder(context)
		.setTitle(titleId)
		.setMessage(msgID)
		.setPositiveButton(closeTxtId, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		})
		.create();
		alert.show();
		
//		final TextView messageView = new TextView( context );
//		messageView.setPadding(2, 2, 2, 2)
//		final SpannableString spanText= new SpannableString(context.getText(msgID));
//		Linkify.addLinks(spanText, Linkify.WEB_URLS);
//		messageView.setText(spanText);
//		messageView.setTextColor(Color.WHITE);
//		messageView.setMovementMethod(LinkMovementMethod.getInstance());
//		
//		AlertDialog alert = new AlertDialog.Builder(context)
//		.setTitle(titleId)
//		.setView(messageView)
//		.setPositiveButton(closeTxtId, new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog, int whichButton) {
//			}
//		})
//		.create();
//		alert.show();
	}

}
