package com.geojobi.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {
	
	public static final String DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";
	
	public static Date convertStringToDate( String dateString )
	{
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat( DateHelper.DATE_FORMAT );
			return dateFormat.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
