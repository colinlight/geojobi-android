package com.geojobi.helper;


import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class JobOverlayItem extends OverlayItem {

	private GeoPoint mGeoPoint; 
	
	public JobOverlayItem( GeoPoint point, int label, String labelName, int radius, int salary, int jobType, int permit, 
			String title, String snippet) 
	{
		super( point, title, snippet );
		
		this.mGeoPoint = point;
		this.mLabel = label;
		this.mLabelName = labelName;
		this.mRadius = radius;
		this.mSalary = salary;
		this.mJobType = jobType;
		this.mPermit = permit;
	}
	
	public GeoPoint getPoint()
	{
		return this.mGeoPoint;
	}
	
	@Override
	public String toString()
	{
		return mLabelName;
	}
	
	
	private String mLabelName = "";

	public void setLabelName( String value )
	{
		this.mLabelName = value;
	}
	public String getLabelName()
	{
		return this.mLabelName;
	}


	private int mLabel = 0;

	public void setLabel( int value )
	{
		this.mLabel = value;
	}
	public int getLabel()
	{
		return this.mLabel;
	}




	private int mRadius = 0;

	public void setRadius( int value )
	{
		this.mRadius = value;
	}
	public int getRadius()
	{
		return this.mRadius;
	}


	private int mSalary = 0;

	public void setSalary( int value )
	{
		this.mSalary = value;
	}
	public int getSalary()
	{
		return this.mSalary;
	}



	private int mJobType = 0;

	public void setJobType( int value )
	{
		this.mJobType = value;
	}
	public int getJobType()
	{
		return this.mJobType;
	}



	private int mPermit = 0;

	public void setPermit( int value )
	{
		this.mPermit = value;
	}
	public int getPermit()
	{
		return this.mPermit;
	}
	

}
