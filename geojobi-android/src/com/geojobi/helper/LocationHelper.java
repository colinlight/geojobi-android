package com.geojobi.helper;

import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.widget.TextView;

import com.geojobi.R;
import com.google.android.maps.GeoPoint;

public class LocationHelper {

	public static GeoPoint getGeoPointForLocation( LocationManager locationManagaer, String provider)
	{
		Location location = locationManagaer.getLastKnownLocation(provider);
		return new GeoPoint( (int)( location.getLatitude() * 1E6 ), 
				(int)( location.getLongitude() * 1E6 ) );
	}

	
	public static String getLocationString( Address address ) 
	{
		if (  address.getPostalCode() != null)
			return address.getPostalCode();

		if (  address.getAdminArea() != null)
			return address.getAdminArea();

		if ( address.getCountryName() != null)
			return address.getCountryName();
		
		return "";
	}

}
