package com.geojobi.helper;

import java.util.List;

import android.location.Address;
import com.google.android.maps.GeoPoint;

public interface IOverlayAlertDelegate {
	
	public void displayDialog( GeoPoint point, List<Address> addressList  );
	
	public void displayDialog( JobOverlayItem overlay, List<Address> addressList );
	
	public void overlayUpdated();

}
